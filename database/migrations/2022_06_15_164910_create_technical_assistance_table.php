<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_assistance', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('degree')->nullable();
            $table->string('speciality')->nullable();
            $table->boolean('typeSpecialist')->nullable();
            $table->string('convencional')->nullable();
            $table->string('conservacion')->nullable();
            $table->string('organica')->nullable();
            $table->string('otra')->nullable();
            $table->string('specialties')->nullable();
            $table->string('typeCultivo')->nullable();
            $table->float('cost')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_assistance');
    }
};
