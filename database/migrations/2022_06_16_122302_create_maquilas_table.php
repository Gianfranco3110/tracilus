<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maquilas', function (Blueprint $table) {
            $table->id();
            $table->string('activity')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->float('cost')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maquilas');
    }
};
