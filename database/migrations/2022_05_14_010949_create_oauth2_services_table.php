<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth2_services', function (Blueprint $table) {
            $table->id();
            $table->string('service');
            $table->timestamps();
        });

        $date = date('Y-m-d H:i:s');

        DB::table('oauth2_services')->insert([
            ['service' => 'Google', 'created_at' => $date, 'updated_at' => $date],
            ['service' => 'Facebook', 'created_at' => $date, 'updated_at' => $date]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth2_services');
    }
};
