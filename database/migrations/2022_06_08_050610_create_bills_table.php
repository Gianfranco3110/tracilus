<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('business_name');
            $table->string('rfc');
            $table->unsignedBigInteger('fiscal_domicile_country');
            $table->unsignedBigInteger('fiscal_domicile_state');
            $table->string('fiscal_domicile_municipality');
            $table->string('fiscal_domicile_postal_code');
            $table->string('fiscal_domicile_cologne');
            $table->string('fiscal_domicile_street');
            $table->string('fiscal_domicile_outdoor_number');
            $table->string('fiscal_domicile_location');
            $table->string('fiscal_domicile_location_reference');
            $table->unsignedBigInteger('business_address_country')->nullable();
            $table->unsignedBigInteger('business_address_state')->nullable();
            $table->string('business_address_municipality')->nullable();
            $table->string('business_address_postal_code')->nullable();
            $table->string('business_address_cologne')->nullable();
            $table->string('business_address_street')->nullable();
            $table->string('business_address_outdoor_number')->nullable();
            $table->string('business_address_location')->nullable();
            $table->string('business_address_location_reference')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('fiscal_domicile_country')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('fiscal_domicile_state')->references('id')->on('states')->onDelete('cascade');
            $table->foreign('business_address_country')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('business_address_state')->references('id')->on('states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
};
