<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_rents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('maquila_id');
            $table->foreign('maquila_id')->references('id')->on('maquilas')->onDelete('cascade');
            $table->timestamp('started_at');
            $table->timestamp('expired_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_rents');
    }
};
