<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('second_last_name')->nullable();
            $table->string('slug')->nullable();
            $table->string('email')->unique()->nullable();
            $table->date('birthdate')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('verification_code')->unique()->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->unsignedBigInteger('profile_id')->comment('1 - Administrador Master, 2 - Administrador, 3 - Cliente, 4 - Proveedor')->nullable();
            $table->unsignedBigInteger('gender_id')->comment('1 - Femenino, 2 - Masculino')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->foreign('gender_id')->references('id')->on('genders')->onDelete('cascade');
        });

        $date = date('Y-m-d H:i:s');

        DB::table('users')->insert([
            ['name' => 'admin', 
            'last_name' => 'master', 
            'slug' => 'admin-master', 
            'email' => 'admin@gmail.com', 
            'password' => Hash::make('tractilus'), 
            'profile_id' => 1, 
            'email_verified_at' => $date, 
            'phone_verified_at' => $date, 
            'created_at' => $date, 
            'updated_at' => $date]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
