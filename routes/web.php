<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\VerificationPhoneController;
use App\Http\Controllers\Auth\SocialAuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index');

Route::get('/iniciar-sesion', function () {
    return view('index');
})->name('iniciar-sesion');

Route::get('/registrate', function () {
    return view('index');
});

Route::get('/registrate/{user?}', function () {
    return view('index');
});

Route::get('/productor/formulario/parte-1', function () {
    return view('index');
});

Route::get('/productor/formulario/parte-2', function () {
    return view('index');
});

Route::get('/productor/formulario/parte-3', function () {
    return view('index');
});

Route::get('/proveedor/formulario/sin-factura/parte-1', function () {
    return view('index');
});

Route::get('/proveedor/formulario/sin-factura/parte-2', function () { 
    return view('index');
});

Route::get('/proveedor/formulario/tipo-de-proveedor', function () {
    return view('index');
});

Route::get('/proveedor/formulario/puede-facturar', function () {
    return view('index');
});

Route::get('/proveedor/formulario/con-factura/parte-1', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/implementos/formulario', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/implementos/inventario', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/insumos/formulario/parte-1', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/insumos/formulario/parte-2', function () {
    return view('index');
});

Route::get('/proveedor/inicio/edit-productos/insumos/formulario/parte-2', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/insumos/inventario', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/asistencia-tecnica/formulario', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/asistencia-tecnica/inventario', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/maquila/formulario', function () {
    return view('index');
});

Route::get('/proveedor/inicio/agrega-productos/maquila/inventario', function () {
    return view('index');
});

Route::middleware(['auth:sanctum','verifiedphone'])->group(function () {
    Route::get('/inicio/usuario', function () {
        return view('index');
    })->name('home');
});

Route::get('phone/verify', [VerificationPhoneController::class, 'show'])->name('phoneverification.notice');
Route::post('phone/verify', [VerificationPhoneController::class, 'verify'])->name('phoneverification.verify');

Route::get('/verificacion/telefono', function () {
    return view('index');
})->name('verificacion');

Route::get('auth/{provider}', [SocialAuthController::class, 'redirectToProvider'])->name('social.auth');
Route::get('auth/{provider}/callback', [SocialAuthController::class, 'handleProviderCallback']);