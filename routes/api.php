<?php

use App\Http\Controllers\API\AreaInfluenceController;
use App\Http\Controllers\API\BillController;
use App\Http\Controllers\API\CityController;
use App\Http\Controllers\API\CalendarRentController;
use App\Http\Controllers\API\CountryController;
use App\Http\Controllers\API\ImplementController;
use App\Http\Controllers\API\LocationController;
use App\Http\Controllers\API\MaquilaController;
use App\Http\Controllers\API\ParcelaController;
use App\Http\Controllers\API\StateController;
use App\Http\Controllers\API\SupplieController;
use App\Http\Controllers\API\CategoryController;
use Illuminate\Http\Request;
use App\Http\Controllers\API\TechnicalAssistanceController;
use App\Http\Controllers\API\TransferTimeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Auth API
Route::post('/register/first', [RegisterController::class, 'registerFirst']);
Route::post('/login', [LoginController::class, 'login']);

// All API Routes
// Route::middleware('auth:sanctum')->group(function () {
Route::put('/register/second', [RegisterController::class, 'registerSecond']);
Route::post('/logout', [LogoutController::class, 'logout']);
Route::resource('location', LocationController::class);
Route::resource('parcela', ParcelaController::class);
Route::resource('country', CountryController::class);
Route::resource('state', StateController::class);
Route::resource('city', CityController::class);
Route::resource('bill', BillController::class);
Route::resource('implement', ImplementController::class);
Route::resource('technical_assistance', TechnicalAssistanceController::class);
Route::resource('maquila', MaquilaController::class);
Route::resource('supplie', SupplieController::class);
Route::get('/show_by_country/{id}', [StateController::class, 'showByCountry']);
Route::get('/show_by_state/{id}', [CityController::class, 'showByState']);
Route::get('/show_by_user_implements/{id}', [ImplementController::class, 'showByUser']);
Route::get('/show_by_user_maquilas/{id}', [MaquilaController::class, 'showByUser']);
Route::get('/show_by_user_technical_assistance/{id}', [TechnicalAssistanceController::class, 'showByUser']);
Route::get('/show_by_user_supplies/{id}', [SupplieController::class, 'showByUser']);
Route::resource('area_influence', AreaInfluenceController::class);
Route::resource('transfer_time', TransferTimeController::class);
Route::controller(TransferTimeController::class)->group(function () {
    Route::get('show_by_area_influence/{area_influence}', 'showByAreaInfluence');
});
Route::resource('calendar_rent', CalendarRentController::class);
Route::controller(CalendarRentController::class)->group(function () {
    Route::get('show_by_maquila/{maquila}', 'showByMaquila');
});

// });