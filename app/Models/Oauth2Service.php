<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oauth2Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'service'
    ];
}
