<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Implement extends Model
{
    use HasFactory;

    protected $fillable = [
        'activity',
        'brand',
        'model',
        'type',
        'cost',
        'image1',
        'image2',
        'image3',
        'image4',
        'user_id'
    ];
}
