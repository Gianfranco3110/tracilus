<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 
        'business_name', 
        'rfc', 
        'fiscal_domicile_country',
        'fiscal_domicile_state',
        'fiscal_domicile_municipality',
        'fiscal_domicile_postal_code',
        'fiscal_domicile_cologne',
        'fiscal_domicile_street',
        'fiscal_domicile_outdoor_number',
        'fiscal_domicile_location',
        'fiscal_domicile_location_reference',
        'business_address_country',
        'business_address_state',
        'business_address_municipality',
        'business_address_postal_code',
        'business_address_cologne',
        'business_address_street',
        'business_address_outdoor_number',
        'business_address_location',
        'business_address_location_reference'
    ];
}
