<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 
        'country_id', 
        'state_id', 
        'city_id',
        'suburb',
        'postal_Code',
        'street',
        'number',
        'reference'
    ];
}
