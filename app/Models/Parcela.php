<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parcela extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 
        'type',
        'name', 
        'hectares', 
        'latitude',
        'longitude',
        'accuracy',
        'irrigation_system'
    ];
}
