<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplie extends Model
{
    use HasFactory;

    protected $fillable = [
        'typeSupplie',
        'otherSupplie',
        'name',
        'typeContainer',
        'otherContainer',
        'inventory',
        'unitPrice',
        'image1',
        'image2',
        'image3',
        'image4',
        'user_id'
    ];
}
