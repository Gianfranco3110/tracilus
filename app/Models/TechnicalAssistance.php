<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TechnicalAssistance extends Model
{
    use HasFactory;

    protected $table = 'technical_assistance';

    protected $fillable = [
        'name',
        'degree',
        'speciality',
        'typeSpecialist',
        'convencional',
        'conservacion',
        'organica',
        'otra',
        'specialties',
        'typeCultivo',
        'cost',
        'image1',
        'image2',
        'image3',
        'image4',
        'user_id'
    ];
}
