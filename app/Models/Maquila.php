<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maquila extends Model
{
    use HasFactory;

    protected $fillable = [
        'activity',
        'brand',
        'model',
        'name',
        'type',
        'cost',
        'image1',
        'image2',
        'image3',
        'image4',
        'user_id',
        'status'
    ];
}
