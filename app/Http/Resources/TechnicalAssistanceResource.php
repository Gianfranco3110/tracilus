<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TechnicalAssistanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'degree' => $this->degree,
            'speciality' => $this->speciality,
            'typeSpecialist' => $this->typeSpecialist,
            'convencional' => $this->convencional,
            'conservacion' => $this->conservacion,
            'organica' => $this->organica,
            'otra' => $this->otra,
            'specialties' => $this->specialties,
            'typeCultivo' => $this->typeCultivo,
            'cost' => $this->cost,
            'image1' => $this->image1,
            'image2' => $this->image2,
            'image3' => $this->image3,
            'image4' => $this->image4,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at->format('d/m/Y'),
            'updated_at' => $this->updated_at->format('d/m/Y')
        ];
    }
}
