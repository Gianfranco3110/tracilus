<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'business_name' => $this->business_name,
            'rfc' => $this->rfc,
            'fiscal_domicile_country' => $this->fiscal_domicile_country,
            'fiscal_domicile_state' => $this->fiscal_domicile_state,
            'fiscal_domicile_municipality' => $this->fiscal_domicile_municipality,
            'fiscal_domicile_postal_code' => $this->fiscal_domicile_postal_code,
            'fiscal_domicile_cologne' => $this->fiscal_domicile_cologne,
            'fiscal_domicile_street' => $this->fiscal_domicile_street,
            'fiscal_domicile_outdoor_number' => $this->fiscal_domicile_outdoor_number,
            'fiscal_domicile_location' => $this->fiscal_domicile_location,
            'fiscal_domicile_location_reference' => $this->fiscal_domicile_location_reference,
            'business_address_country' => $this->business_address_country,
            'business_address_state' => $this->business_address_state,
            'business_address_municipality' => $this->business_address_municipality,
            'business_address_postal_code' => $this->business_address_postal_code,
            'business_address_cologne' => $this->business_address_cologne,
            'business_address_street' => $this->business_address_street,
            'business_address_outdoor_number' => $this->business_address_outdoor_number,
            'business_address_location' => $this->business_address_location,
            'business_address_location_reference' => $this->business_address_location_reference,
            'created_at' => $this->created_at->format('d/m/Y'),
            'updated_at' => $this->updated_at->format('d/m/Y')
        ];
    }
}
