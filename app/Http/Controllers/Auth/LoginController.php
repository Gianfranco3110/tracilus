<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{
    public function login(Request $request)
    {
        
        $userData = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);

        if($userData->fails()){
            return $this->sendErrorResponse('Validation Error.', $userData->errors());
        }

        if(!Auth::attempt($request->only('email', 'password')))
        {
            return $this->sendErrorResponse('Credenciales Incorrectas', ['error'=>'Unauthorised'], 401);
        }

        $user = Auth::user();

        $token = $user->createToken('Tractilus')->plainTextToken;

        return $this->sendSuccessResponse(['token' => $token, 'id' => $user->id, 'user_type' => $user->profile_id], 'Login correcto');
    }
}