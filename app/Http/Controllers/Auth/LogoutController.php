<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogoutController extends BaseController
{
    public function logout()
    {
        Auth::user()->tokens()->delete();
        auth('web')->logout();

        return $this->sendSuccessResponse([], 'Logged out');
    }
}
