<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\API\BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;

class VerificationPhoneController extends BaseController
{
    public function show(Request $request)
    {
        return $request->user()->hasVerifiedPhone()
                        ? redirect()->route('home')
                        : redirect()->route('verificacion');
    }

    public function verify(Request $request)
    {
        $user = User::find($request->id);
        
        if ($user->hasVerifiedPhone()) {
            return redirect()->route('home');
        }
 
        $request->validate(['code' => ['required', 'string', 'min:6', 'max:6', 'regex:/([0-9]{6,6})/'],]);
         
        if ($user->verification_code !== $request->code)
        {
            return $this->sendErrorResponse('El codigo que ingresaste es incorrecto. Por favor intentalo otra vez.', ['message' => 'El codigo que ingresaste es incorrecto. Por favor intentalo otra vez.']);
        }
         
        $user->markPhoneAsVerified();
         
        return $this->sendSuccessResponse(['name' => $user->name], 'Tu telefono ha sido verificado.');
    }
}
