<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends BaseController
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Register step first api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerFirst(Request $request)
    {
        $userData = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'phone' => 'required'
        ]);

        if($userData->fails()){
            return $this->sendErrorResponse('Validation Error.', $userData->errors());
        }

        $user = $this->userService->store($request->all());

        $token = $user->createToken('Tractilus')->plainTextToken;

        $user->sendPhoneVerificationNotification();
        
        return $this->sendSuccessResponse(['token' => $token, 'id' => $user->id], 'Usuario Registrado');
    }

     /**
     * Register step second api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerSecond(Request $request)
    {
        $userData = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'second_last_name' => 'required',
            'gender_id' => 'required',
            'birthdate' => 'required'
        ]);

        if($userData->fails()){
            return $this->sendErrorResponse('Validation Error.', $userData->errors());
        }

        $user = $this->userService->update($request->all(), $request->input('id'));
        
        return $this->sendSuccessResponse(['name' => $user->name], 'Usuario Registrado');
    }
}