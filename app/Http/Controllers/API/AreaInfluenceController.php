<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\AreaInfluenceResource;
use App\Models\AreaInfluence;
use App\Models\TransferTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AreaInfluenceController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $areaInfluences = AreaInfluence::all();

        return $this->sendSuccessResponse(AreaInfluenceResource::collection($areaInfluences), 'area of incluences retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_id' => 'required|exists:users,id',
            'country_id' => 'required|exists:countries,id',
            'state_id' => 'required|exists:states,id',
            'city_id' => 'required|exists:cities,id',
            'time' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $areaInfluence = AreaInfluence::create($input);
        if ($areaInfluence) {
            $transferTime = TransferTime::create(['area_influence_id' => $areaInfluence->id, 'time' => $input['time']]);
            return $this->sendSuccessResponse(new AreaInfluenceResource($areaInfluence), 'Area of incluence created successfully.');
        } else {
            return $this->sendErrorResponse('Ocurrió un error no se pudo guardar.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\AreaInfluence $areaInfluence
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(AreaInfluence $areaInfluence)
    {
        if (is_null($areaInfluence)) {
            return $this->sendErrorResponse('Area of influence not found');
        }

        return $this->sendSuccessResponse(new AreaInfluenceResource($areaInfluence), 'Area of influence retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $
     * @param \App\Models\AreaInfluence $areaInfluence
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, AreaInfluence $areaInfluence)
    {
        if (is_null($areaInfluence)) {
            return $this->sendErrorResponse('Area of influence not found');
        }

        $input = $request->all();
        $areaInfluence->fill($input);
        $areaInfluence->save();

        return $this->sendSuccessResponse(new AreaInfluenceResource($areaInfluence), 'Area of influence updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\AreaInfluence $areaInfluence
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(AreaInfluence $areaInfluence)
    {
        if (is_null($areaInfluence)) {
            return $this->sendErrorResponse('Area of influence not found');
        }

        $areaInfluence->delete();

        return $this->sendSuccessResponse([], 'Area of influence deleted successfully.');
    }
}
