<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\BillResource;
use App\Models\Bill;
use Illuminate\Support\Facades\Validator;

class BillController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $bills = Bill::all();

        return $this->sendSuccessResponse(BillResource::collection($bills), 'Bills retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'business_name' => 'required',
            'rfc' => 'required',
            'fiscal_domicile_country' => 'required',
            'fiscal_domicile_state' => 'required',
            'fiscal_domicile_municipality' => 'required',
            'fiscal_domicile_postal_code' => 'required',
            'fiscal_domicile_cologne' => 'required',
            'fiscal_domicile_street' => 'required',
            'fiscal_domicile_outdoor_number' => 'required',
            'fiscal_domicile_location' => 'required',
            'fiscal_domicile_location_reference' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $bill = Bill::create($input);

        return $this->sendSuccessResponse(new BillResource($bill), 'Bill created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\Bill $bill
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Bill $bill)
    {
        if(is_null($bill)){
            return $this->sendErrorResponse('Bill not found');
        }

        return $this->sendSuccessResponse(new BillResource($bill), 'Bill retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request 
     * @param \App\Models\Bill $bill
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Bill $bill)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'business_name' => 'required',
            'rfc' => 'required',
            'fiscal_domicile_country' => 'required',
            'fiscal_domicile_state' => 'required',
            'fiscal_domicile_municipality' => 'required',
            'fiscal_domicile_postal_code' => 'required',
            'fiscal_domicile_cologne' => 'required',
            'fiscal_domicile_street' => 'required',
            'fiscal_domicile_outdoor_number' => 'required',
            'fiscal_domicile_location' => 'required',
            'fiscal_domicile_location_reference' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validation Error.', $validator->errors());
        }

        $bill->fill($input);
        $bill->save();

        return $this->sendSuccessResponse(new BillResource($bill), 'Bill updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Bill $bill
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Bill $bill)
    {
        if(is_null($bill)){
            return $this->sendErrorResponse('Bill not found');
        }

        $bill->delete();

        return $this->sendSuccessResponse([], 'Bill deleted successfully.');
    }
}
