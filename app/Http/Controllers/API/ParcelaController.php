<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\ParcelaResource;
use App\Models\Parcela;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ParcelaController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $parcelas = Parcela::all();

        return $this->sendSuccessResponse(ParcelaResource::collection($parcelas), 'Parcelas retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'type' => 'required',
            'name' => 'required',
            'hectares' => 'required',
            'irrigation_system' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $parcela = Parcela::create($input);

        return $this->sendSuccessResponse(new ParcelaResource($parcela), 'Parcela created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\Parcela $parcela
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Parcela $parcela)
    {
        if(is_null($parcela)){
            return $this->sendErrorResponse('Parcela not found');
        }

        return $this->sendSuccessResponse(new ParcelaResource($parcela), 'Parcela retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request $
     * @param \App\Models\Parcela $parcela
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Parcela $parcela)
    {
        if(is_null($parcela)){
            return $this->sendErrorResponse('Parcela not found');
        }
        
        $input = $request->all();
        $parcela->fill($input);
        $parcela->save();

        return $this->sendSuccessResponse(new ParcelaResource($parcela), 'Parcela updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Parcela $parcela
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Parcela $parcela)
    {
        if(is_null($parcela)){
            return $this->sendErrorResponse('Parcela not found');
        }

        $parcela->delete();

        return $this->sendSuccessResponse([], 'Parcela deleted successfully.');
    }
}
