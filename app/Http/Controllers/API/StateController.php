<?php

namespace App\Http\Controllers\API;

use App\Models\State;
use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Http\Resources\StateResource;
use App\Models\Country;
use Illuminate\Support\Facades\Validator;

class StateController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $states = State::orderBy('state', 'ASC')->get();

        return $this->sendSuccessResponse(StateResource::collection($states), 'States retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'country_id' => 'required',
            'state' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $state = State::create($input);

        return $this->sendSuccessResponse(new StateResource($state), 'State created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\State $state
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(State $state)
    {
        if(is_null($state)){
            return $this->sendErrorResponse('State not found');
        }

        return $this->sendSuccessResponse(new StateResource($state), 'State retrieved successfully');
    }

    public function showByCountry($country_id)
    {
        $states = State::where('country_id', $country_id)->get();

        return $this->sendSuccessResponse(StateResource::collection($states), 'States retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request $
     * @param \App\Models\State $state
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, State $state)
    {
        if(is_null($state)){
            return $this->sendErrorResponse('State not found');
        }
        
        $input = $request->all();
        $state->fill($input);
        $state->save();

        return $this->sendSuccessResponse(new StateResource($state), 'State updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\State $state
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(State $state)
    {
        if(is_null($state)){
            return $this->sendErrorResponse('State not found');
        }

        $state->delete();

        return $this->sendSuccessResponse([], 'State deleted successfully.');
    }
}
