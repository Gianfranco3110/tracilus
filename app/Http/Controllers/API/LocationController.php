<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Models\Location;
use App\Http\Resources\LocationResource;
use Illuminate\Support\Facades\Validator;

class LocationController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $locations = Location::all();

        return $this->sendSuccessResponse(LocationResource::collection($locations), 'Locations retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'suburb' => 'required',
            'postal_Code' => 'required',
            'street' => 'required',
            'number' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $location = Location::create($input);

        return $this->sendSuccessResponse(new LocationResource($location), 'Location created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\Location $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Location $location)
    {
        if(is_null($location)){
            return $this->sendErrorResponse('Location not found');
        }

        return $this->sendSuccessResponse(new LocationResource($location), 'Location retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request $
     * @param \App\Models\Location $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Location $location)
    {
        if(is_null($location)){
            return $this->sendErrorResponse('Location not found');
        }
        
        $input = $request->all();
        $location->fill($input);
        $location->save();

        return $this->sendSuccessResponse(new LocationResource($location), 'Location updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Location $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Location $location)
    {
        if(is_null($location)){
            return $this->sendErrorResponse('Location not found');
        }

        $location->delete();

        return $this->sendSuccessResponse([], 'Location deleted successfully.');
    }
}
