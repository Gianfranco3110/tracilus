<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Support\Facades\Validator;

class CityController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $cities = City::orderBy('city', 'ASC')->get();

        return $this->sendSuccessResponse(CityResource::collection($cities), 'Cities retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'country_id' => 'required',
            'state_id' => 'required',
            'city' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $city = City::create($input);

        return $this->sendSuccessResponse(new CityResource($city), 'City created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\City $city
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(City $city)
    {
        if(is_null($city)){
            return $this->sendErrorResponse('City not found');
        }

        return $this->sendSuccessResponse(new CityResource($city), 'City retrieved successfully');
    }

    public function showByState($state_id)
    {
        $cities = City::where('state_id', $state_id)->get();

        return $this->sendSuccessResponse(CityResource::collection($cities), 'Cities retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request $
     * @param \App\Models\City $city
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, City $city)
    {
        if(is_null($city)){
            return $this->sendErrorResponse('City not found');
        }
        
        $input = $request->all();
        $city->fill($input);
        $city->save();

        return $this->sendSuccessResponse(new CityResource($city), 'City updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\City $city
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(City $city)
    {
        if(is_null($city)){
            return $this->sendErrorResponse('City not found');
        }

        $city->delete();

        return $this->sendSuccessResponse([], 'City deleted successfully.');
    }
}
