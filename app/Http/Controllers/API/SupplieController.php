<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\SupplieResource;
use App\Models\Supplie;
use Illuminate\Support\Facades\Validator;

class SupplieController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $supplies = Supplie::all();

        return $this->sendSuccessResponse(SupplieResource::collection($supplies), 'Supplies retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $implement = Supplie::create($input);

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $implement->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/supplies', $name);
            $implement->image1 = $name;
            $implement->save();
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $implement->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/supplies', $name);
            $implement->image2 = $name;
            $implement->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $implement->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/supplies', $name);
            $implement->image3 = $name;
            $implement->save();
        }

        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $implement->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/supplies', $name);
            $implement->image4 = $name;
            $implement->save();
        }

        return $this->sendSuccessResponse(new SupplieResource($implement), 'Supplie created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\Supplie $supplie
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Supplie $supplie)
    {
        if(is_null($supplie)){
            return $this->sendErrorResponse('Supplie not found');
        }

        return $this->sendSuccessResponse(new SupplieResource($supplie), 'Supplie retrieved successfully');
    }

    public function showByUser($user_id)
    {
        $supplies = Supplie::where('user_id', $user_id)->get();

        return $this->sendSuccessResponse(SupplieResource::collection($supplies), 'Supplies retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request 
     * @param \App\Models\Supplie $supplie
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Supplie $supplie)
    {
        $input = $request->all();
        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $technical_assistance->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image1 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $technical_assistance->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image2 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $technical_assistance->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image3 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $technical_assistance->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image4 = $name;
            $technical_assistance->save();
        }

        $supplie->fill($input);
        $supplie->save();

        return $this->sendSuccessResponse(new SupplieResource($supplie), 'Supplie updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Supplie $bill
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Supplie $supplie)
    {
        if(is_null($supplie)){
            return $this->sendErrorResponse('Supplie not found');
        }

        if(!empty($supplie->image1) && file_exists(public_path() . '/uploads/images/supplies/'.$supplie->image1)){ 
            unlink(public_path() . '/uploads/images/supplies/'.$supplie->image1);
        } 
        if(!empty($supplie->image2) && file_exists(public_path() . '/uploads/images/supplies/'.$supplie->image2)){ 
            unlink(public_path() . '/uploads/images/supplies/'.$supplie->image2);
        } 
        if(!empty($supplie->image3) && file_exists(public_path() . '/uploads/images/supplies/'.$supplie->image3)){ 
            unlink(public_path() . '/uploads/images/supplies/'.$supplie->image3);
        } 
        if(!empty($supplie->image4) && file_exists(public_path() . '/uploads/images/supplies/'.$supplie->image4)){ 
            unlink(public_path() . '/uploads/images/supplies/'.$supplie->image4);
        } 

        $supplie->delete();

        return $this->sendSuccessResponse([], 'Supplie deleted successfully.');
    }
} 
