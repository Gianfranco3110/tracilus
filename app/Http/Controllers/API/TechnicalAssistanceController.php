<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\TechnicalAssistanceResource;
use App\Models\TechnicalAssistance;
use Illuminate\Support\Facades\Validator;

class TechnicalAssistanceController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $technical_assistance = TechnicalAssistance::all();

        return $this->sendSuccessResponse(TechnicalAssistanceResource::collection($technical_assistance), 'Technical Assistance retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        // $validator = Validator::make($input, [
        //     'cost' => 'required',
        //     'image1' => 'required',
        //     'image2' => 'required',
        //     'image3' => 'required',
        //     'image4' => 'required'
        // ]);

        // if($validator->fails()){
        //     return $this->sendErrorResponse('Validator Error.', $validator->errors());
        // }

        $technical_assistance = TechnicalAssistance::create($input);

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $technical_assistance->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image1 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $technical_assistance->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image2 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $technical_assistance->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image3 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $technical_assistance->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image4 = $name;
            $technical_assistance->save();
        }

        return $this->sendSuccessResponse(new TechnicalAssistanceResource($technical_assistance), 'Technical Assistance created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\TechnicalAssistance $technical_assistance
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(TechnicalAssistance $technical_assistance)
    {
        if(is_null($technical_assistance)){
            return $this->sendErrorResponse('Technical Assistance not found');
        }

        return $this->sendSuccessResponse(new TechnicalAssistanceResource($technical_assistance), 'Technical Assistance retrieved successfully');
    }

    public function showByUser($user_id)
    {
        $technical_assistances = TechnicalAssistance::where('user_id', $user_id)->get();

        return $this->sendSuccessResponse(TechnicalAssistanceResource::collection($technical_assistances), 'Technical Assistance retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request 
     * @param \App\Models\TechnicalAssistance $technical_assistance
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, TechnicalAssistance $technical_assistance)
    {
        $input = $request->all();

        // $validator = Validator::make($input, [
        //     'user_id' => 'required',
        // ]);

        // if($validator->fails()){
        //     return $this->sendErrorResponse('Validation Error.', $validator->errors());
        // }
        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $technical_assistance->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image1 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $technical_assistance->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image2 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $technical_assistance->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image3 = $name;
            $technical_assistance->save();
        }

        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $technical_assistance->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/technical_assistance', $name);
            $technical_assistance->image4 = $name;
            $technical_assistance->save();
        }

        $technical_assistance->fill($input);
        $technical_assistance->save();

        return $this->sendSuccessResponse(new TechnicalAssistanceResource($technical_assistance), 'Technical Assistance updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Implement $technical_assistance
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TechnicalAssistance $technical_assistance)
    {
        if(is_null($technical_assistance)){
            return $this->sendErrorResponse('Technical Assistance not found');
        }

        if(!empty($technical_assistance->image1) && file_exists(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image1)){ 
            unlink(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image1);
        } 
        if(!empty($technical_assistance->image2) && file_exists(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image2)){ 
            unlink(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image2);
        } 
        if(!empty($technical_assistance->image3) && file_exists(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image3)){ 
            unlink(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image3);
        } 
        if(!empty($technical_assistance->image4) && file_exists(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image4)){ 
            unlink(public_path() . '/uploads/images/technical_assistance/'.$technical_assistance->image4);
        } 

        $technical_assistance->delete();

        return $this->sendSuccessResponse([], 'Technical Assistance deleted successfully.');
    }
}
