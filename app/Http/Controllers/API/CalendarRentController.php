<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\CalendarRentResource;
use App\Models\CalendarRent;
use App\Models\Maquila;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CalendarRentController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $calendarRents = CalendarRent::all();

        return $this->sendSuccessResponse(CalendarRentResource::collection($calendarRents), 'Calendar rents retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'maquila_id' => 'required|exists:maquilas,id',
            'started_at' => 'required|date',
            'expired_at' => 'required|date',
        ]);
        if ($validator->fails()) {
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $calendarRent = CalendarRent::create($input);
        if ($calendarRent) {
            Maquila::where('id', $input['maquila_id'])
                ->update(['status' => false]);

            return $this->sendSuccessResponse(new CalendarRentResource($calendarRent), 'Calendar rents created successfully.');
        } else {
            return $this->sendErrorResponse('Ocurrió un error no se pudo guardar.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CalendarRent $calendarRent
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(CalendarRent $calendarRent)
    {
        if (is_null($calendarRent)) {
            return $this->sendErrorResponse('Calendar rents not found');
        }

        return $this->sendSuccessResponse(new CalendarRentResource($calendarRent), 'Calendar rents retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $
     * @param \App\Models\CalendarRent $calendarRent
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, CalendarRent $calendarRent)
    {
        if (is_null($calendarRent)) {
            return $this->sendErrorResponse('Calendar rents not found');
        }

        $input = $request->all();
        $calendarRent->fill($input);
        $calendarRent->save();

        return $this->sendSuccessResponse(new CalendarRentResource($calendarRent), 'Calendar rents updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\CalendarRent $calendarRent
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CalendarRent $calendarRent)
    {
        if (is_null($calendarRent)) {
            return $this->sendErrorResponse('Calendar rents not found');
        }

        $calendarRent->delete();

        return $this->sendSuccessResponse([], 'Calendar rents deleted successfully.');
    }

    /**
     * Display the specified resource by maquila.
     *
     * @param \App\Models\Maquila $maquila
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByMaquila(Maquila $maquila)
    {
        if (is_null($maquila)) {
            return $this->sendErrorResponse('Area of influence time not found');
        }
        $calendarRent = CalendarRent::where('area_influence_id', $maquila->id)->first();

        return $this->sendSuccessResponse(new CalendarRentResource($calendarRent), 'Calendar rents retrieved successfully');
    }
}
