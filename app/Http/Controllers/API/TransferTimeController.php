<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\TransferTimeResource;
use App\Models\AreaInfluence;
use App\Models\TransferTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransferTimeController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $transferTimes = TransferTime::all();

        return $this->sendSuccessResponse(TransferTimeResource::collection($transferTimes), 'Transfer time retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'area_influence_id' => 'required|exists:area_influences,id',
            'time' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $transferTime = TransferTime::create($input);
        if ($transferTime) {
            return $this->sendSuccessResponse(new TransferTimeResource($transferTime), 'Transfer time created successfully.');
        } else {
            return $this->sendErrorResponse('Ocurrió un error no se pudo guardar.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\TransferTime $transferTime
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(TransferTime $transferTime)
    {
        if (is_null($transferTime)) {
            return $this->sendErrorResponse('Transfer time not found');
        }

        return $this->sendSuccessResponse(new TransferTimeResource($transferTime), 'Transfer time retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $
     * @param \App\Models\TransferTime $transferTime
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, TransferTime $transferTime)
    {
        if (is_null($transferTime)) {
            return $this->sendErrorResponse('Transfer time not found');
        }

        $input = $request->all();
        $transferTime->fill($input);
        $transferTime->save();

        return $this->sendSuccessResponse(new TransferTimeResource($transferTime), 'Transfer time updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\TransferTime $transferTime
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TransferTime $transferTime)
    {
        if (is_null($transferTime)) {
            return $this->sendErrorResponse('Transfer time not found');
        }

        $transferTime->delete();

        return $this->sendSuccessResponse([], 'Transfer time deleted successfully.');
    }

    /**
     * Display the specified resource by area of influence.
     *
     * @param \App\Models\AreaInfluence $transferTime
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByAreaInfluence(AreaInfluence $areaInfluence)
    {
        if (is_null($areaInfluence)) {
            return $this->sendErrorResponse('Area of influence time not found');
        }
        $transferTime=TransferTime::where('area_influence_id', $areaInfluence->id)->first();

        return $this->sendSuccessResponse(new TransferTimeResource($transferTime), 'Transfer time retrieved successfully');
    }
}
