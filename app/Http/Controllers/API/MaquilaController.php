<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\MaquilaResource;
use App\Models\Maquila;
use Illuminate\Support\Facades\Validator;

class MaquilaController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $maquilas = Maquila::all();

        return $this->sendSuccessResponse(MaquilaResource::collection($maquilas), 'Maquilas retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $implement = Maquila::create($input);

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $implement->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/maquilas', $name);
            $implement->image1 = $name;
            $implement->save();
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $implement->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/maquilas', $name);
            $implement->image2 = $name;
            $implement->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $implement->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/maquilas', $name);
            $implement->image3 = $name;
            $implement->save();
        }
    
        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $implement->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/maquilas', $name);
            $implement->image4 = $name;
            $implement->save();
        }

        return $this->sendSuccessResponse(new MaquilaResource($implement), 'Maquila created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\Maquila $maquila
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Maquila $maquila)
    {
        if(is_null($maquila)){
            return $this->sendErrorResponse('Maquila not found');
        }

        return $this->sendSuccessResponse(new MaquilaResource($maquila), 'Maquila retrieved successfully');
    }

    public function showByUser($user_id)
    {
        $maquilas = Maquila::where('user_id', $user_id)->get();

        return $this->sendSuccessResponse(MaquilaResource::collection($maquilas), 'Maquilas retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request 
     * @param \App\Models\Maquila $maquila
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Maquila $maquila)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_id'   => 'required',
            'activity'  => 'required',
            'brand'     => 'required',
            'model'     => 'required',
            'type'      => 'required',
            'cost'      => 'required'
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validation Error.', $validator->errors());
        }
        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $implement->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/maquilas', $name);
            $implement->image1 = $name;
            $implement->save();
        }
        
        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $implement->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image2 = $name;
            $implement->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $implement->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image3 = $name;
            $implement->save();
        }
    
        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $implement->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image4 = $name;
            $implement->save();
        }

        $maquila->fill($input);
        $maquila->save();

        return $this->sendSuccessResponse(new MaquilaResource($maquila), 'Maquila updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Maquila $bill
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Maquila $maquila)
    {
        if(is_null($maquila)){
            return $this->sendErrorResponse('Maquila not found');
        }

        if(!empty($maquila->image1) && file_exists(public_path() . '/uploads/images/maquilas/'.$maquila->image1)){ 
            unlink(public_path() . '/uploads/images/maquilas/'.$maquila->image1);
        } 
        if(!empty($maquila->image2) && file_exists(public_path() . '/uploads/images/maquilas/'.$maquila->image2)){ 
            unlink(public_path() . '/uploads/images/maquilas/'.$maquila->image2);
        } 
        if(!empty($maquila->image3) && file_exists(public_path() . '/uploads/images/maquilas/'.$maquila->image3)){ 
            unlink(public_path() . '/uploads/images/maquilas/'.$maquila->image3);
        } 
        if(!empty($maquila->image4) && file_exists(public_path() . '/uploads/images/maquilas/'.$maquila->image4)){ 
            unlink(public_path() . '/uploads/images/maquilas/'.$maquila->image4);
        } 

        $maquila->delete();

        return $this->sendSuccessResponse([], 'Maquila deleted successfully.');
    }
}