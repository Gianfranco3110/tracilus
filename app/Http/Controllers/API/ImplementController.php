<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\ImplementResource;
use App\Models\Implement;
use Illuminate\Support\Facades\Validator;

class ImplementController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $implements = Implement::all();

        return $this->sendSuccessResponse(ImplementResource::collection($implements), 'Implements retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $implement = Implement::create($input);
        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $implement->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image1 = $name;
            $implement->save();
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $implement->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image2 = $name;
            $implement->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $implement->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image3 = $name;
            $implement->save();
        }
    
        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $implement->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image4 = $name;
            $implement->save();
        }
        return $this->sendSuccessResponse(new ImplementResource($implement), 'Implement created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\Implement $implement
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Implement $implement)
    {
        if(is_null($implement)){
            return $this->sendErrorResponse('Implement not found');
        }

        return $this->sendSuccessResponse(new ImplementResource($implement), 'Implement retrieved successfully');
    }

    public function showByUser($user_id)
    {
        $implements = Implement::where('user_id', $user_id)->get();

        return $this->sendSuccessResponse(ImplementResource::collection($implements), 'Implements retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request 
     * @param \App\Models\Implement $implement
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Implement $implement)
    {
        
        $input = $request->all();
        if (!$request->hasFile('image1')) {
            $file = $request->file('image1');
            $name = $implement->id . "image-1." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image1 = $name;
            $implement->save();
        }
        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $name = $implement->id . "image-2." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image2 = $name;
            $implement->save();
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $name = $implement->id . "image-3." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image3 = $name;
            $implement->save();
        }
    
        if ($request->hasFile('image4')) {
            $file = $request->file('image4');
            $name = $implement->id . "image-4." . $file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/images/implements', $name);
            $implement->image4 = $name;
            $implement->save();
        }

        $implement->fill($input);
        $implement->save();
        return $this->sendSuccessResponse(new ImplementResource($implement), 'Implement updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Implement $bill
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Implement $implement)
    {
        if(is_null($implement)){
            return $this->sendErrorResponse('Implement not found');
        }

        if(!empty($implement->image1) && file_exists(public_path() . '/uploads/images/implements/'.$implement->image1)){ 
            unlink(public_path() . '/uploads/images/implements/'.$implement->image1);
        } 
        if(!empty($implement->image2) && file_exists(public_path() . '/uploads/images/implements/'.$implement->image2)){ 
            unlink(public_path() . '/uploads/images/implements/'.$implement->image2);
        } 
        if(!empty($implement->image3) && file_exists(public_path() . '/uploads/images/implements/'.$implement->image3)){ 
            unlink(public_path() . '/uploads/images/implements/'.$implement->image3);
        } 
        if(!empty($implement->image4) && file_exists(public_path() . '/uploads/images/implements/'.$implement->image4)){ 
            unlink(public_path() . '/uploads/images/implements/'.$implement->image4);
        } 

        $implement->delete();

        return $this->sendSuccessResponse([], 'Implement deleted successfully.');
    }
}