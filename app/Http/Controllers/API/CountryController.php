<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\CountryResource;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CountryController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $countries = Country::orderBy('country', 'ASC')->get();

        return $this->sendSuccessResponse(CountryResource::collection($countries), 'Countries retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'country' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendErrorResponse('Validator Error.', $validator->errors());
        }

        $country = Country::create($input);

        return $this->sendSuccessResponse(new CountryResource($country), 'Country created successfully.');
    }

    /**
     * Display the specified resource.
     * 
     * @param \App\Models\Country $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Country $country)
    {
        if(is_null($country)){
            return $this->sendErrorResponse('Country not found');
        }

        return $this->sendSuccessResponse(new CountryResource($country), 'Country retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request $
     * @param \App\Models\Country $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Country $country)
    {
        if(is_null($country)){
            return $this->sendErrorResponse('Country not found');
        }
        
        $input = $request->all();
        $country->fill($input);
        $country->save();

        return $this->sendSuccessResponse(new CountryResource($country), 'Country updated successfully');
    }

    /**
     * Remove the specified resource from storage. 
     * 
     * @param \App\Models\Country $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Country $country)
    {
        if(is_null($country)){
            return $this->sendErrorResponse('Country not found');
        }

        $country->delete();

        return $this->sendSuccessResponse([], 'Country deleted successfully.');
    }
}
