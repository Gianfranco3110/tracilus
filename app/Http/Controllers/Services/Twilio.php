<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class Twilio extends BaseController
{
    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param String $recipients string or array of phone number of recepient
     */
    public static function sendMessage($message, $recipients)
    {
        $account_sid = config('services.twilio')['account_sid'];
        $auth_token = config('services.twilio')['auth_token'];
        $phone = config('services.twilio')['phone'];
        $twilio = new Client($account_sid, $auth_token);
        $twilio->messages->create($recipients, [
            'from' => $phone,
            'body' => $message
        ] );
    }
}
