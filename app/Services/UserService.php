<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Str as Str;

class UserService 
{
    protected $modelClass = User::class;
    public $model;

    public function __construct()
    {
        $this->model = new $this->modelClass;
    }

    public function store($attributes)
    {
        $attributes['password'] = empty($attributes['password']) ? : bcrypt($attributes['password']);

        $user = $this->model->newInstance($attributes);

        $user->save();

        return $user;
    }

    public function update($attributes, $id)
    {
        $user = User::find($id);

        $user->fill($attributes);
        $user->slug = Str::slug($attributes['name'] . "-" . $attributes['last_name']);

        $user->save();

        return $user;
    }
}