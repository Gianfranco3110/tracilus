export const tokenName = "user_tractilus_token";
export const baseUrl = 'https://tractilus.tk';
// export const baseUrl = "http://127.0.0.1:8000";

// Auth Repository
export const login = "/api/login";
export const register = "/api/register/first";
export const registerSecond = "/api/register/second";
export const logout = "/api/logout";
