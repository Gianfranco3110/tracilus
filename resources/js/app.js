import { BrowserRouter, Routes, Route } from 'react-router-dom'

import Home from './components/home/home'
import Nav from './components/layout/nav/nav'
import Footer from './components/layout/footer/footer'
import SignIn from './components/signIn/signIn'
import SignUp from './components/signUp/signUp'
import SignUpUser from './components/signUpUser/signUpUser'
import VerifyPhone from './components/client/verifyphone'

import ProducerFormPart1 from './components/producer/form/part1/part1'
import ProducerFormPart2 from './components/producer/form/part2/part2'
import ProducerFormPart3 from './components/producer/form/part3/part3'

import ProviderFormNoTaxPart1 from './components/provider/form/noTax/part1/part1'
import ProviderFormNoTaxPart2 from './components/provider/form/noTax/part2/part2'
import ProviderFormTypeOfCompany from './components/provider/form/typeOfCompany/typeOfCompany'
import ProviderFormCanTax from './components/provider/form/canTax/canTax'
import ProviderFormTaxPart1 from './components/provider/form/tax/part1/part1'
import ProviderHomeAddToCatalog from './components/provider/addToCatalog/home/home'
import ProviderImplementosForm from './components/provider/addToCatalog/implementos/form/form'
import ProviderImplementosInventory from './components/provider/addToCatalog/implementos/inventory/inventory'
import ProviderInsumosFormPart1 from './components/provider/addToCatalog/insumos/form/part1/part1'
import ProviderInsumosFormPart2 from './components/provider/addToCatalog/insumos/form/part2/part2'

import ProviderInsumosFormEditPart2 from './components/provider/addToCatalog/insumos/form/part2/partEdit2'
import ProviderInsumosInventory from './components/provider/addToCatalog/insumos/inventory/inventory'
import ProviderAsistenciaTecnicaForm from './components/provider/addToCatalog/asistenciaTecnica/form/form'
import ProviderAsistenciaTecnicaInventory from './components/provider/addToCatalog/asistenciaTecnica/inventory/inventory'
import ProviderMaquilaForm from './components/provider/addToCatalog/maquila/form/form'
import ProviderMaquilaInventory from './components/provider/addToCatalog/maquila/inventory/inventory'


import ClientHome from './components/client/home'

function App() {
    return (
        <BrowserRouter>
            <Nav />
            <Routes>
                <Route exact path = '/' element={<Home/>} />
                <Route exact path = '/iniciar-sesion' element={<SignIn/>} />
                <Route exact path = '/registrate' element={<SignUp />} />
                <Route exact path = '/registrate/:user' element={<SignUpUser />} />
                <Route exact path = '/inicio/usuario' element={<ClientHome/>} />
                <Route exact path = '/verificacion/telefono' element={<VerifyPhone/>} />
                <Route exact path = '/productor/formulario/parte-1' element={<ProducerFormPart1 />} />
                <Route exact path = '/productor/formulario/parte-2' element={<ProducerFormPart2 />} />
                <Route exact path = '/productor/formulario/parte-3' element={<ProducerFormPart3 />} />
                <Route exact path = '/proveedor/formulario/sin-factura/parte-1' element={<ProviderFormNoTaxPart1 />} />
                <Route exact path = '/proveedor/formulario/sin-factura/parte-2' element={<ProviderFormNoTaxPart2 />} />
                <Route exact path = '/proveedor/formulario/tipo-de-proveedor' element={<ProviderFormTypeOfCompany />} />
                <Route exact path = '/proveedor/formulario/puede-facturar' element={<ProviderFormCanTax />} />
                <Route exact path = '/proveedor/formulario/con-factura/parte-1' element={<ProviderFormTaxPart1 />} />
                <Route exact path = '/proveedor/inicio/agrega-productos' element={<ProviderHomeAddToCatalog/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/implementos/formulario' element={<ProviderImplementosForm/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/implementos/inventario' element={<ProviderImplementosInventory/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/insumos/formulario/parte-1' element={<ProviderInsumosFormPart1/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/insumos/formulario/parte-2' element={<ProviderInsumosFormPart2/>} />
                <Route exact path = '/proveedor/inicio/edit-productos/insumos/formulario/parte-2' element={<ProviderInsumosFormEditPart2/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/insumos/inventario' element={<ProviderInsumosInventory/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/asistencia-tecnica/formulario' element={<ProviderAsistenciaTecnicaForm/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/asistencia-tecnica/inventario' element={<ProviderAsistenciaTecnicaInventory/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/maquila/formulario' element={<ProviderMaquilaForm/>} />
                <Route exact path = '/proveedor/inicio/agrega-productos/maquila/inventario' element={<ProviderMaquilaInventory/>} />
            </Routes>
            <Footer />
        </BrowserRouter>
    );
}

export default App;