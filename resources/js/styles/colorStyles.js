export const  whiteTractilus = 'rgba(255, 255, 255, 1)'

export const yellowTractilus = 'rgba(248, 223, 123, 1)'

export const greenLightTractilus = 'rgba(80, 165, 140, 1)'

export const blueDarkTractilus = 'rgba(10, 50, 60, 1)'

export const orangeTractilus = 'rgba(255, 150, 60, 1)'

export const greenTractilus = 'rgba(98, 144, 18, 1)'

export const greyTractilus = 'rgba(0, 0, 0, 0.16)'

export const greyLigthTractilus = 'rgba(226, 226, 226, 1)'

export const greyDarkTractilus = 'rgba(129, 129, 129, 1)'

export const greenDarkTractilus = 'rgba(28, 130, 141, 1)'

export const implementoRed = 'rgba(154, 30, 25, 1)'

export const insumosBlue = 'rgba(28, 67, 141, 1)'

export const asistenciaTecnicaGray = 'rgba(48, 61, 62, 1)'

export const maquilaPurple = 'rgba(134, 18, 144, 1)'
