const specilities = [
  'Seleccionar...',
  'Diagnóstico de parcela',
  'Análisis de suelo',
  'Interpretación para acondicionamiento de parcela',
  'Fertilización integral',
  'Control de maleza',
  'Control de plagas',
  'Control de enfermedades',
  'Aplicación de foriales',
  'Arreglo topológico',
  'Calibración de equipos agricolas',
  'Cosecha',
  'Postcosecha',
  'Otra',
]


export default specilities