import styled from 'styled-components'

import { greenTractilus, whiteTractilus } from '../../../styles/colorStyles'

export const ConfirmationCodeContainer = styled.main `
  position: fixed;
  display: ${props => props.showModalErrorSignUpData ? 'flex' : 'none'};
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0,0,0,0.2);
`


export const Message = styled.div `
  position: relative;
  background-color: ${greenTractilus};
  width: 30%;
  padding-top: 5%;
  padding-bottom: 5%;
  border-radius: 20px;

  > figure {
    position: absolute;
    top: 20px;
    right: 20px;
    width: 25px;
    height: 25px;
    cursor: pointer;

    > img  {
      width: 100%;
      height: 100%;
    }
  }

  > h3 {
    color: ${whiteTractilus};
    font-family: 'Montserrat';
    font-size: 2vw;
    font-weight: 400;
    text-align: center;
    margin-bottom: 2.5%;
  }
`


