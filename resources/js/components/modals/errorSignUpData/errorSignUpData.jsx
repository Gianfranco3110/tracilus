import React, {useState} from 'react'
import '../modal.css';

import {
  ConfirmationCodeContainer,
  Message
} from './errorSignUpData.styles'

const ModalErrorSignUpData = ({ handleClose, show, children }) => {
  const [ userData, setUserData ] = useState()

  const showHideClassName = show ? "display-block" : "display-none";

  return (
    <ConfirmationCodeContainer className={showHideClassName}>
      <Message>
        <figure onClick={handleClose}>
          <img src="/" alt="tache" />
        </figure>
        <h3>¡Error!</h3>
        <p>Verifica que tus datos sean correctos.</p>
      </Message>
    </ConfirmationCodeContainer>
  )
};

export default ModalErrorSignUpData;