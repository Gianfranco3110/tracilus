import React, { useEffect, useState } from 'react'

import {
  ErrorLackOfDataContainer,
  Message
} from './errorLackOfData.styles'


export default function ErrorLackOfData (props) {
  const [ showModal, setShowModal ] = useState(props.showModalErrorLackOfData)

  useEffect( ()=> {
    setShowModal(props.showModalErrorLackOfData)
  },[props.showModalErrorLackOfData])

  const closeModal = () => {
    setShowModal(false)
    setShowModal(props.setShowModalErrorLackOfData)
  }

  return (
    <ErrorLackOfDataContainer showModal={showModal}>
      <Message>
        <figure onClick={closeModal}>
          <img src="/" alt="tache" />
        </figure>
        <h3>
          ¡Error!
        </h3>
        <p>Verifica que hayas llenado todos los campos.</p>
      </Message>
    </ErrorLackOfDataContainer>
  )
}