import React, {useState} from 'react'
import '../modal.css';
import AuthApi from '../../api/authRepository';

import {
  ConfirmationCodeContainer,
  Code
} from './confirmationCode.styles'

const ConfirmationCode = ({ handleClose, show, children }) => {
  const [ userData, setUserData ] = useState({})
  const [ message, setMessage ] = useState()
  const showHideClassName = show ? "display-block" : "display-none";

  const handleData = e => {
    userData[e.target.name] = e.target.value
  }

  const sendDataToServer = () => {
    userData["id"] = localStorage.getItem("id");
    AuthApi.verifiedPhone(userData)
          .then(r => {
            let type = localStorage.getItem("user_type");
            if(type == 'productor')
              window.location = '/productor/formulario/parte-1'
            else if(type == 'proveedor')
              window.location = '/proveedor/formulario/sin-factura/parte-1'
          }).catch(e => {
            setMessage(e.data.message)  
          });
  }

  return (
    <ConfirmationCodeContainer className={showHideClassName}>
      <Code>
        <figure onClick={handleClose}>
          <img src="/" alt="tache" />
        </figure>
        <h3>
          Ingrese el código de 
          confirmación
        </h3>
        <input onChange={handleData} type="text" name='code'/>
        <p>{message}</p>
        <button onClick={sendDataToServer}>Enviar</button>
      </Code>
    </ConfirmationCodeContainer>
  )
}

export default ConfirmationCode;