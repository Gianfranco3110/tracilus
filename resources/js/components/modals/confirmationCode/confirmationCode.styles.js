import styled from "styled-components";

import { greenTractilus, whiteTractilus } from "../../../styles/colorStyles";

export const ConfirmationCodeContainer = styled.main`
    position: fixed;
    display: ${(props) => (props.showModalConfirmationCode ? "flex" : "none")};
    justify-content: center;
    align-items: center;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    background-color: rgba(0, 0, 0, 0.5);

    @media (max-width: 600px) {
    }
`;

export const Code = styled.div`
  position: relative;
  background-color: ${greenTractilus};
  width: 30%;
  padding-top: 5%;
  padding-bottom: 5%;
  border-radius: 20px;

  > div {
    position: absolute;
    top: 20px;
    right: 25px;
    display: flex;
    flex-direction: column;
    width: 30px;
    height: 30px;
    cursor: pointer;

    > div:nth-child(1) {
      background-color: ${whiteTractilus};
      width: 80%;
      height: 2px;
      transform: rotate(45deg) translateX(12px);
    }

    > div:nth-child(2) {
      background-color: ${whiteTractilus};
      width: 80%;
      height: 2px;
      transform: rotate(-45deg) translateX(1.2px) translateY(10.5px);
    }
  }

  > h3 {
    color: ${whiteTractilus};
    font-family: 'Montserrat';
    font-size: 2vw;
    font-weight: 400;
    text-align: center;
    margin-bottom: 2.5%;
  }

  > input {
    display: block;
    font-size: 1.5vw;
    font-family: 'Montserrat';
    text-align: center;
    letter-spacing: 20px;
    color: ${whiteTractilus};
    background-color: transparent;
    border: none;
    border-bottom: 1px solid ${whiteTractilus};
    width: 60%;
    height: 35px;
    margin: 5% auto;
    padding-left: 2.5%;
    outline: none;
  }

  > button {
    display: block;
    color: ${whiteTractilus};
    font-size: 1vw;
    font-family: 'Montserrat';
    font-weight: bold;
    width: 60%;
    margin: auto;
    background-color: transparent;
    border: 1px solid ${whiteTractilus};
    border-radius: 50px;
    padding: 2%;
    cursor: pointer;
  }

  @media(max-width: 600px) {
    width: 90%;

    > div {

    }

    > h3 {
      font-size: 5vw;
      text-align: center;
      width: 80%;
      margin: auto;
      margin-top: 25%;
      margin-bottom: 2.5%;
    }

    > input {
      border: none;
      border-bottom: 1px solid ${whiteTractilus};
      width: 80%;
      height: 35px;
      margin: 10% auto;
    }

    > button {
      font-size: 4vw;
      width: 80%;
  }
`;
