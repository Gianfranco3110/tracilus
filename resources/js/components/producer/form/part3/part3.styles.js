import styled from 'styled-components'

import { blueDarkTractilus, greenLightTractilus, greenTractilus, greyTractilus, whiteTractilus } from '../../../../styles/colorStyles'

export const Part3Container = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
`

export const Part3Title = styled.div `
  
  > h2 {
    color: ${greenTractilus};
    text-align: center;
    font-size: 2.5vw;
    font-family: 'Montserrat';
    font-weight: 600;
    margin-bottom: 1%;
  }

  > p {
    font-size: 1.2vw;
    font-family: 'Montserrat';
    color: ${blueDarkTractilus};
    text-align: center;
    margin-bottom: 2.5%;
  }

  @media (max-width: 600px) {

    > h2 {
      font-size: 9vw;
      margin-bottom: 2.5%;
    }

    > p {
      font-size: 4vw;
      width: 80%;
      margin: auto;
      margin-bottom: 5%;
    }
  }

`

export const Status = styled.div `
  display: flex;
  justify-content: space-between;
  width: 25%;
  margin: 2.5% auto;

  > p {
    color: ${greyTractilus};
    font-size: 2vw;
    font-family: 'Montserrat';
    border: 1px solid ${greyTractilus};
    border-radius: 50%;
    padding: 2.5% 5.5%;
  }

  > p:nth-child(1) {
    padding: 2.5% 6.5%;
  }

  > p:nth-child(3) {
    color: ${whiteTractilus};
    border: 1px solid ${greenTractilus};
    background-color: ${greenTractilus};
    padding: 2.5% 5.5%;
  }

  @media (max-width: 600px) {
    width: 50%;
    margin: 5% auto;

    > p {
      font-size: 6vw;
    }

    > p:nth-child(1) {
    }
  }
`


export const Part3Form = styled.div `
`


export const Part3FormManual = styled.div `
  display: flex;
  flex-direction: column;
  width: 35%;
  margin: auto;


  @media (max-width: 600px) {
    width: 85%;
  }
`


export const CardParcela = styled.div `
  margin-top: 5%;

  > p {
    color: ${greenTractilus};
    text-align: center;
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.2vw;
  }

  > div:nth-child(2) {
    display: flex;
    width: 80%;
    margin: 5% auto;

    > div {
      display: flex;
      align-items: center;
      width: 50%;

      > input {
        font-size: 1.2vw;
        width: 25px;
        height: 25px;
        border: 1px solid ${greenTractilus};
      }

      > p {
        font-family: 'Montserrat';
        font-size: 1.2vw;
        color: ${greenTractilus};
        margin-left: 5%;
      }
    }
  }

  > div {

    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${greenTractilus};
      margin-bottom: 1%;
    }

    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greenTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greenTractilus};
      padding-left: 2%;
      outline: none;
    }

    > select {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greenTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greenTractilus};
      padding-left: 2%;
      outline: none;

      > option {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${greenTractilus};
      }
    }
  }

  @media(max-width: 600px) {

    > p {
      font-size: 5vw;
    }

    > div:nth-child(2) {

      > div {
        width: 50%;

        > input {
          font-size: 5vw;
          width: 25px;
          height: 25px;
        }

        > p {
          font-size: 5vw;
        }
      }
    }


    > div {

      > p {
        font-size: 5vw;
        margin-bottom: 2.5%;
      }
  
      > input {
        font-size: 5vw;
        margin-bottom: 5%;
      }
  
      > select {
        font-size: 5vw;
        width: 100%;
        margin-bottom: 5%;
  
        > option {
          font-size: 5vw;
        }
      }
    }
  }
`


export const FormButtons = styled.div `
  display: flex;
  flex-direction: column;
  justify-content: center;

  > div {
    display: flex;

    > button:nth-child(1),
    > button:nth-child(2) {
      display: flex;
      justify-content: space-between;
      align-items: center;
      font-family: 'Montserrat';
      font-weight: 300;
      font-size: 1.3vw;
      color: ${greenTractilus};
      border: none;
      background-color: transparent;
      width: 55%;
      margin: 5% auto auto auto;
      padding: 2% 6%;
      border-radius: 20px;
      cursor: pointer;

      > figure {
        width: 12%;
    
        > img {
          width: 100%;
          height: 100%;
        }
      }
    }
  }

  button {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1vw;
    color: ${whiteTractilus};
    background-color: ${greenTractilus};
    border: none;
    width: 35%;
    margin: 5% auto 10% auto;
    padding: 2% 6%;
    border-radius: 20px;
    cursor: pointer;
    transition: all 0.25s linear;

    &:hover {
      background-color: ${greenLightTractilus};
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }

  @media (max-width: 600px) {

    > div {
      
      > button:nth-child(1) {
        font-size: 5vw;
        width: 50%;
        padding: 2% 1%;

        > figure {
          width: 27%;
      
          > img {
            width: 100%;
            height: 100%;
          }
        }
      }

      > button:nth-child(2) {
        font-size: 5vw;
        width: 50%;
        padding: 2% 1%;

        > figure {
          width: 23%;
      
          > img {
            width: 100%;
            height: 100%;
          }
        }
      }
    }


    > button {
      font-size: 5vw;
      width: 100%;
      padding: 4% 6%;
    }
  }


`

