import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ParcelaAPI from '../../../api/parcelaRepository';
 
import {
  Part3Container,
  Part3Title,
  Status,
  Part3Form,
  Part3FormManual,
  CardParcela,
  FormButtons
} from './part3.styles'

export default function Part3 () {
  const [ userData, setUserData ] = useState({})
  const [ onlyNumbers, setOnlyNumbers ] = useState()
  const [ numberOfParcela, setNumberOfParcela ] = useState(0)
  let navigate = useNavigate()

  const handleData = e => userData[e.target.name] = e.target.value

  const handleOnlyNumbers = e => {    
    const re = /^[+\d](?:.*\d)?$/;

    if (e.target.value === '' || re.test(e.target.value)) {
       setOnlyNumbers(e.target.value)
       userData[e.target.name] = e.target.value
    }
  }

  const getGeolocation = () => {
    let options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    }

    function success(pos) {
      let crd = pos.coords
    
      userData['latitude'] = crd.latitude
      userData['longitude'] = crd.longitude
      userData['accuracy'] = crd.accuracy
    }
    
    function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    navigator.geolocation.getCurrentPosition(success, error, options);
  }

  const addCardParcela = () => numberOfParcela >= 1 ? setNumberOfParcela(1+numberOfParcela) : setNumberOfParcela(1)

  const removeCardParcela = () => {
    if(numberOfParcela === 0) {
      setNumberOfParcela(0)
    } else if (numberOfParcela >=4) {
      setNumberOfParcela(3)
    } else {
      setNumberOfParcela(numberOfParcela-1)
    }
  }

  const sendDataToServer = async () => {
    for (var i = 0; i <= numberOfParcela; i++) {
        let parcela = { 
          'user_id' : localStorage.getItem("id"),
          'type' : userData['type_'+i],
          'name' : userData['name_'+i],
          'hectares' : userData['hectares_'+i],
          'irrigation_system' : userData['irrigation_system_'+i],
          'latitude' : userData['latitude'],
          'longitude' : userData['longitude'],
          'accuracy' : userData['accuracy']
        };

        try {
            const response = await ParcelaAPI.newParcela(parcela);
            if (!response.success) {
                throw Error(response.message);
            }
            else if(i === numberOfParcela)
              window.location = '/inicio/usuario'
        } catch (error) {
            alert("No se pudo guardar, revise sus datos ingresados e intentelo de nuevo")
        }
    }      
  }

  return (
    <Part3Container>
      <Part3Title>
        <h2>Productor</h2>
        <p>
          Deseamos conocerlo un poco más para ofrecerle
          las mejores opciones.
        </p>
      </Part3Title>
      <Status>
        <p>1</p>
        <p>2</p>
        <p>3</p>
      </Status>
      <Part3Form>
        <Part3FormManual>
          {numberOfParcela >= 0 &&
          <CardParcela>
            <p>Parcela 1</p>
            <div>
              <div>
                <input onChange={handleData} type="radio" name="type_0" value='Propia'/>
                <p>Propia:</p>
              </div>
              <div>
                <input onChange={handleData} type="radio" name="type_0" value='Rentada'/>
                <p>Rentada:</p>
              </div>
            </div>
            <div>
              <p>Nombre de parcela:</p>
              <input onChange={handleData} type="text" name='name_0' />
            </div>
            <div>
              <p>Superficie en hectáreas</p>
              <input onChange={handleOnlyNumbers} type="text" name='hectares_0' />
            </div>
            <div>
              <p>Georefencia:</p>
              <input onClick={getGeolocation} type="button" name='georeference_0' value='Ubíquenme'/>
            </div>
            <div>
              <p>Sistema de riego:</p>
              <select onChange={handleData} name="irrigation_system_0">
                <option value={false}>Seleccionar...</option>
                <option value="Temporal">Temporal</option>
                <option value="Riego">Riego</option>
                <option value="Goteo">Goteo</option>
              </select>
            </div>
          </CardParcela>
          }
          {numberOfParcela >= 1 &&
          <CardParcela>
            <p>Parcela 2</p>
            <div>
              <div>
                <input onChange={handleData} type="radio" name="type_1" value='Propia'/>
                <p>Propia:</p>
              </div>
              <div>
                <input onChange={handleData} type="radio" name="type_1" value='Rentada'/>
                <p>Rentada:</p>
              </div>
            </div>
            <div>
              <p>Nombre de parcela:</p>
              <input onChange={handleData} type="text" name='name_1' />
            </div>
            <div>
              <p>Superficie en hectáreas</p>
              <input onChange={handleOnlyNumbers} type="text" name='hectares_1' />
            </div>
            <div>
              <p>Georefencia:</p>
              <input onClick={getGeolocation} type="button" name='georeference_1' value='Ubíquenme'/>
            </div>
            <div>
              <p>Sistema de riego:</p>
              <select onChange={handleData} name="irrigation_system_1">
                <option value={false}>Seleccionar...</option>
                <option value="Temporal">Temporal</option>
                <option value="Riego">Riego</option>
                <option value="Goteo">Goteo</option>
              </select>
            </div>
          </CardParcela>
          }          
          {numberOfParcela >= 2 &&
            <CardParcela>
              <p>Parcela 3</p>
              <div>
                <div>
                  <input onChange={handleData} type="radio" name="type_2" value='Propia'/>
                  <p>Propia:</p>
                </div>
                <div>
                  <input onChange={handleData} type="radio" name="type_2" value='Rentada'/>
                  <p>Rentada:</p>
                </div>
              </div>
              <div>
                <p>Nombre de parcela:</p>
                <input onChange={handleData} type="text" name='name_2' />
              </div>
              <div>
                <p>Superficie en hectáreas</p>
                <input onChange={handleOnlyNumbers} type="text" name='hectares_2' />
              </div>
              <div>
                <p>Georefencia:</p>
                <input onClick={getGeolocation} type="button" name='georeference_2' value='Ubíquenme'/>
              </div>
              <div>
                <p>Sistema de riego:</p>
                <select onChange={handleData} name="irrigation_system_2">
                  <option value={false}>Seleccionar...</option>
                  <option value="Temporal">Temporal</option>
                  <option value="Riego">Riego</option>
                  <option value="Goteo">Goteo</option>
                </select>
              </div>
            </CardParcela>
            }
            {numberOfParcela >= 3 &&
            <CardParcela>
              <p>Parcela 4</p>
              <div>
                <div>
                  <input onChange={handleData} type="radio" name="type_3" value='Propia'/>
                  <p>Propia:</p>
                </div>
                <div>
                  <input onChange={handleData} type="radio" name="type_3" value='Rentada'/>
                  <p>Rentada:</p>
                </div>
              </div>
              <div>
                <p>Nombre de parcela:</p>
                <input onChange={handleData} type="text" name='name_3' />
              </div>
              <div>
                <p>Superficie en hectáreas</p>
                <input onChange={handleOnlyNumbers} type="text" name='hectares_3' />
              </div>
              <div>
                <p>Georefencia:</p>
                <input onClick={getGeolocation} type="button" name='georeference_3' value='Ubíquenme'/>
              </div>
              <div>
                <p>Sistema de riego:</p>
                <select onChange={handleData} name="irrigation_system_3">
                  <option value={false}>Seleccionar...</option>
                  <option value="Temporal">Temporal</option>
                  <option value="Riego">Riego</option>
                  <option value="Goteo">Goteo</option>
                </select>
              </div>
            </CardParcela>
            }
            {numberOfParcela >= 4 &&
            <CardParcela>
              <p>Parcela 5</p>
              <div>
                <div>
                  <input onChange={handleData} type="radio" name="type_4" value='Propia'/>
                  <p>Propia:</p>
                </div>
                <div>
                  <input onChange={handleData} type="radio" name="type_4" value='Rentada'/>
                  <p>Rentada:</p>
                </div>
              </div>
              <div>
                <p>Nombre de parcela:</p>
                <input onChange={handleData} type="text" name='name_4' />
              </div>
              <div>
                <p>Superficie en hectáreas</p>
                <input onChange={handleOnlyNumbers} type="text" name='hectares_4' />
              </div>
              <div>
                <p>Georefencia:</p>
                <input onClick={getGeolocation} type="button" name='georeference_4' value='Ubíquenme'/>
              </div>
              <div>
                <p>Sistema de riego:</p>
                <select onChange={handleData} name="irrigation_system_4">
                  <option value={false}>Seleccionar...</option>
                  <option value="Temporal">Temporal</option>
                  <option value="Riego">Riego</option>
                  <option value="Goteo">Goteo</option>
                </select>
              </div>
            </CardParcela>
            }

          <FormButtons>
            <div>
              {numberOfParcela <= 3 &&
                <button onClick={addCardParcela}>
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/agregarMas.svg" alt="boton de agregar" />
                  </figure>
                  Agregar parcela
                </button>
              }
              {numberOfParcela != 0 &&
                <button onClick={removeCardParcela}>
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/agregarMas.svg" alt="boton de agregar" />
                  </figure>
                  Quitar parcela
                </button>
              }
            </div>
            <button onClick={sendDataToServer}>Finalizar</button>
          </FormButtons>
        </Part3FormManual>
      </Part3Form>
    </Part3Container>
  )
}