import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ImplementApi from '../../../../api/implementRepository';

import {
  ImplementosContainer,
  FormImplementos,
  FormImplementosTitle,
  FormImplementosCards,
  TypeTrigger,
  Photos,
  PhotosTitle,
  PhotoUpload,
  ImplementosButtons
} from './form.styles'


export default function Implementos ({producto}) {
  const [datos, setDatos] = useState({
    activity: '',
    brand: '',
    model: '',
    type:'',
    cost: '',
})
const [datosFile, setDatosFile] = useState({
  image1: '',
  image2: '',
  image3: '',
  image4: '',
})
const handleInputChangeFile = (event) => {
  setDatosFile({
      ...datosFile,
      [event.target.name] : event.target.files[0]
  })
}
const handleInputChange = (event) => {
  setDatos({
      ...datos,
      [event.target.name] : event.target.value
  })
}
  const endAndSendDataToServer = () => {
    console.log('enviando datos.. aosdasd.',datosFile.image1)
    const data = {
      id:producto.id,
      activity: datos.activity === '' ? producto.activity : datos.activity,
      image1:datosFile.image1  === '' ? producto.image1 : datosFile.image1,
      image2:datosFile.image2  === '' ? producto.image1 : datosFile.image2,
      image3:datosFile.image3  === '' ? producto.image1 : datosFile.image3,
      image4:datosFile.image4  === '' ? producto.image1 : datosFile.image4,
      brand:datos.brand === '' ? producto.brand : datos.brand ,
      model:datos.model === '' ? producto.model : datos.model,
      type:datos.type === '' ? producto.type : datos.type,
      cost:datos.cost === '' ? producto.cost : datos.cost,
      user_id:localStorage.getItem("id"),
    }
    ImplementApi.updateImplement(data)
      .then(r => {
          location.reload()
          alert("Producto actualizado con exito");
      }).catch(e => {
        console.log('error',e)
          alert("No se pudo registrar el implemento, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  return (
    <ImplementosContainer>
      <FormImplementos>
        <FormImplementosTitle>
          <h2>Editar Implementos</h2>
        </FormImplementosTitle>
        <FormImplementosCards>
          <div>
            <p>Actividad que realiza el implemento:</p>
            <input onChange={handleInputChange} type="text" name='activity' defaultValue={producto.activity} />
          </div>
          <div>
            <p>Marca:</p>
            <input onChange={handleInputChange} type="text" name='brand' defaultValue={producto.brand} />
          </div>
          <div>
            <p>Modelo:</p>
            <input onChange={handleInputChange} type="text" name='model' defaultValue={producto.model} />
          </div>
          <TypeTrigger>
            <p>Tipo de accionamiento:</p>
            <div>
              <div>
                <p>Tractor 4 ruedas</p>
                <input onChange={handleInputChange} type="radio" name= "type" value='fourWheels'/>
              </div>
              <div>
                <p>Tractor 2 ruedas</p>
                <input onChange={handleInputChange} type="radio" name= "type" value='twoWheels'/>
              </div>
              <div>
                <p>animal</p>
                <input onChange={handleInputChange} type="radio" name= "type" value='animal'/>
              </div>
            </div>
          </TypeTrigger>
          <div>
            <p>Costo de renta:</p>
            <input onChange={handleInputChange} type="number" name='cost' defaultValue={producto.cost} />
          </div>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <ImplementosButtons>
            <div>
              <button onClick={endAndSendDataToServer}>Finalizar</button>
            </div>
          </ImplementosButtons>
        </FormImplementosCards>
      </FormImplementos>
    </ImplementosContainer>
  )
}