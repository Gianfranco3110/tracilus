import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ImplementApi from '../../../../api/implementRepository';

import ProviderNav from '../../../../layout/provider/nav/nav'

import {
  ImplementosContainer,
  FormImplementos,
  FormImplementosTitle,
  FormImplementosCards,
  TypeTrigger,
  Photos,
  PhotosTitle,
  PhotoUpload,
  ImplementosButtons
} from './form.styles'


export default function Implementos () {
  const [ implementosData, setImplementosData ] = useState({})
  let navigate = useNavigate()

  const handleData = e => implementosData[e.target.name] = e.target.value

  const handleFile = e => implementosData[e.target.name] = e.target.files[0]

  const sendDataToServer = () => {
    const data = new FormData();
    data.append("activity", implementosData.activity);
    data.append("brand", implementosData.brand);
    data.append("model", implementosData.model);
    data.append("type", implementosData.type);
    data.append("cost", implementosData.cost);
    data.append("image1", implementosData.image1);
    data.append("image2", implementosData.image2);
    data.append("image3", implementosData.image3);
    data.append("image4", implementosData.image4);
    data.append("user_id", localStorage.getItem("id"));
    console.log('data',data)
    ImplementApi.newImplement(data)
      .then(r => {
          navigate('/proveedor/inicio/agrega-productos/implementos/inventario')
      }).catch(e => {
          alert("No se pudo registrar el implemento, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  const endAndSendDataToServer = () => {
    const data = new FormData();
    data.append("activity", implementosData.activity);
    data.append("brand", implementosData.brand);
    data.append("model", implementosData.model);
    data.append("type", implementosData.type);
    data.append("cost", implementosData.cost);
    data.append("image1", implementosData.image1);
    data.append("image2", implementosData.image2);
    data.append("image3", implementosData.image3);
    data.append("image4", implementosData.image4);
    data.append("user_id", localStorage.getItem("id"));
    
    ImplementApi.newImplement(data)
      .then(r => {
          location.reload()
      }).catch(e => {
          alert("No se pudo registrar el implemento, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  return (
    <ImplementosContainer>
      <ProviderNav />
      <FormImplementos>
        <FormImplementosTitle>
          <h2>Implementos</h2>
        </FormImplementosTitle>
        <FormImplementosCards>
          <div>
            <p>Actividad que realiza el implemento:</p>
            <input onChange={handleData} type="text" name='activity' />
          </div>
          <div>
            <p>Marca:</p>
            <input onChange={handleData} type="text" name='brand' />
          </div>
          <div>
            <p>Modelo:</p>
            <input onChange={handleData} type="text" name='model' />
          </div>
          <TypeTrigger>
            <p>Tipo de accionamiento:</p>
            <div>
              <div>
                <p>Tractor 4 ruedas</p>
                <input onChange={handleData} type="radio" name= "type" value='fourWheels'/>
              </div>
              <div>
                <p>Tractor 2 ruedas</p>
                <input onChange={handleData} type="radio" name= "type" value='twoWheels'/>
              </div>
              <div>
                <p>animal</p>
                <input onChange={handleData} type="radio" name= "type" value='animal'/>
              </div>
            </div>
          </TypeTrigger>
          <div>
            <p>Costo de renta:</p>
            <input onChange={handleData} type="number" name='cost' />
          </div>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <ImplementosButtons>
            <div>
              <button onClick={endAndSendDataToServer}>Finalizar y cargar otro</button>
              <button onClick={sendDataToServer}>Finalizar</button>
            </div>
            <a href='/proveedor/inicio/agrega-productos/implementos/inventario'>Regresar</a>
          </ImplementosButtons>
        </FormImplementosCards>
      </FormImplementos>
    </ImplementosContainer>
  )
}