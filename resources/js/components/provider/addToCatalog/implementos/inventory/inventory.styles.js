import styled from 'styled-components'

import { implementoRed, whiteTractilus, greenLightTractilus } from '../../../../../styles/colorStyles'

export const ImplementosContainer = styled.main `
`

export const FormImplementos = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
  width: 80%;
  margin: auto;
`

export const FormImplementosTitle = styled.div `
  margin-bottom: 8%;

  > h2 {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 2.2vw;
    color: rgba(154, 30, 25, 1);
  }
`


export const FormImplementosCards = styled.div `
  width: 80%;
  margin: auto;

  > a:nth-child(1) {
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 2.5%;
    width: 90%;

    > figure {
      width: 20px;
      height: 30px;
     
      border-radius: 50%;

      > img {
        width: 100%;
        height: 100%;
      }
    }

    text-align: center;
    text-decoration: none;
    color: ${implementoRed};
    font-size: 1.5vw;
    font-family: 'Montserrat';
    font-weight: 600;
  }
`

export const ImplementoCard = styled.div `
  display: flex;
  gap: 5%;
  margin-top: 2.5%;
  margin-bottom: 7.5%;

`

export const ImplementoCardInfo = styled.div `
    display: flex;
    justify-content: space-around;
    border: 1px solid rgba(154, 30, 25, 1);
    width: 90%;
    padding-top: 5%;
    padding-bottom: 5%;
    border-radius: 20px;

    > div:nth-child(1) {
      border: 1px solid rgba(154, 30, 25, 1);
      border-radius: 20px;
      width: 22%;
    }

    > div:nth-child(2) {
      border: 1px solid rgba(154, 30, 25, 1);
      border-radius: 20px;
      width: 22%;
    }

    > div:nth-child(3) {
      border-left: 1px solid rgba(154, 30, 25, 1);
      border-right: 1px solid rgba(154, 30, 25, 1);
      width: 22%;

      > div {
        border: 1px solid rgba(154, 30, 25, 1);
        border-radius: 10px;
        margin: 5%;

        > p {
          padding: 4%;
          text-align: center;
        }
      }

    }

    > div:nth-child(4) {
      border: 1px solid rgba(154, 30, 25, 1);
      border-radius: 20px;
      width: 22%;
    }


`


export const ImplementoCardButtons = styled.div `
  display: flex;
  flex-direction: column;
  align-items: center;

  > figure {
    border: 1px solid red;
    width: 30px;
    height: 30px;
    cursor: pointer;

    > img {
      width: 100%;
      height: 100%;
    }
  }
`



export const ImplementosButtons = styled.div `
  display: flex;
  justify-content: space-around;
  align-items: center;

  > a {
    display: block;
    text-align: center;
    text-decoration: none;
    color: ${whiteTractilus};
    font-size: 1vw;
    font-family: 'Montserrat';
    font-weight: 600;
    background-color: rgba(154, 30, 25, 1);
    width: 30%;
    padding: 1.3% 1.3%;
    border-radius: 20px;
    transition: all 0.25s linear;

    &:hover {
      background-color: ${greenLightTractilus};
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }



`
