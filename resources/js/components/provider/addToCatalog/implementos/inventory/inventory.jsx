import React, { useState } from 'react'

import {
  ImplementosContainer,
  FormImplementos,
  FormImplementosTitle,
  FormImplementosCards,
  ImplementosButtons,
} from './inventory.styles'

import ImplementoCard from '../../../../layout/implementoCard/implementoCard'
import ProviderNav from '../../../../layout/provider/nav/nav'

export default function Implementos () {
  const [ implementosData, setImplementosData ] = useState([1,2])

  return (
    <ImplementosContainer>
      <ProviderNav />
      <FormImplementos>
        <FormImplementosTitle>
          <h2>Implementos</h2>
        </FormImplementosTitle>
        <FormImplementosCards>
          <a href='/proveedor/inicio/agrega-productos/implementos/formulario'>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/add/addRed.svg" alt="Icomo agregar" />
            </figure>
            <span>Agregar nuevo implemento</span>
          </a>
          <ImplementoCard/>
          <ImplementosButtons>
            <a href='/proveedor/inicio/agrega-productos/'>Regresar</a>
          </ImplementosButtons>
        </FormImplementosCards>
      </FormImplementos>
    </ImplementosContainer>
  )
}