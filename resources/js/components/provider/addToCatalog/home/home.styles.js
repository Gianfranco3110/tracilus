import styled from 'styled-components'

import { 
  blueDarkTractilus, 
  greyDarkTractilus, 
  orangeTractilus, 
  whiteTractilus,
  greenLightTractilus } from '../../../../styles/colorStyles'

export const HomeContainer = styled.main `
  background-image: url('/');
`

export const UserNav = styled.div `
  display: flex;
  justify-content: flex-end;
  align-items: center;
  gap: 5%;
  width: 80%;
  margin: auto;
  padding-top: 2.5%;
  padding-bottom: 5%;
  border-top: 1px solid ${greyDarkTractilus};

  > a {
    font-family: 'Montserrat';
    font-weight: 600;
    text-decoration: none;
    color: ${blueDarkTractilus};
  }

  > div:nth-child(2) {
    display: flex;
    flex-direction: column;
    cursor: pointer;

    > div {
      margin-bottom: 16%;
      width: 30px;
      border: 1.3px solid rgba(80, 165, 140, 1);
      background-color: rgba(80, 165, 140, 1);
      border-radius: 5px;
    }
  }
  
  > div:nth-child(3) {
    display: none;
    width: 25px;
    height: 2px;
    position: relative;
    border-radius: 2px;

    > div:before,
      div:after {
      content: '';
      position: absolute;
      width: 24px;
      height: 3.5px;
      background-color: rgba(80, 165, 140, 1);
      border-radius: 2px;
    }

    > div:before {
      -webkit-transform: rotate(50deg);
      -moz-transform: rotate(50deg);
      transform: rotate(50deg);
      transform: rotate(50deg);
    }

    > div:after {
      -webkit-transform: rotate(-50deg);
      -moz-transform: rotate(-50deg);
      transform: rotate(-50deg);
      right: 2px;
    }
  }

`

export const ChooseUser = styled.section `
  background-color: rgba(245,245,245,1);
  padding-top: 5%;
  padding-bottom: 5%;
  background-size: cover;
  background-position-x: 85%;
`

export const ChooseUserTitle = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 2%;

  > h2 {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 2.2vw;
    color: ${blueDarkTractilus};
  }
`


export const ChooseUserCards = styled.div `
  display: flex;
  justify-content: space-around;
  gap: 3%;
  width: 80%;
  margin: 5% auto 0 auto;

  > div:nth-child(1) {
    > div {
      background-color: rgba(154, 30, 25, 1);
      margin-bottom: 40%;
    }
  }

  > div:nth-child(2) {
    > div {
      background-color: rgba(28, 67, 141, 1);
    }
  }

  > div:nth-child(3) {
    > div {
      background-color: rgba(134, 18, 144, 1);
    }
  }

  > div:nth-child(4) {
    > div {
      background-color: rgba(48, 61, 62, 1);
    }
  }

`


export const UserCard = styled.div `
  border-radius: 15px;
  width: 25%;

  > figure {
    width: 100%;
    height: 280px;
    

    > img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }

  > div {
    padding-top:10%;
    padding-bottom: 10%;
    border-bottom-right-radius: 15px;
    border-bottom-left-radius: 15px;

    > h2 {
      color: ${whiteTractilus};
      text-align: center;
      font-family: 'Montserrat';
      font-weight: 600;
      margin-bottom: 10%;
    }

    > a {
      display: block;
      text-align: center;
      text-decoration: none;
      color: ${whiteTractilus};
      font-size: 1.2vw;
      font-family: 'Montserrat';
      font-weight: 500;
      background-color: ${orangeTractilus};
      width: 50%;
      margin: auto auto 5% auto;
      padding: 2% 6%;
      border-radius: 20px;
      transition: all 0.25s linear;

      &:hover {
        background-color: ${greenLightTractilus};
        transform: translateY(-3px);
        box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
      }
    }
  }
`
