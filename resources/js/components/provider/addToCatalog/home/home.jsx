import React from 'react'

import {
  HomeContainer,
  UserNav,
  ChooseUser,
  ChooseUserTitle,
  ChooseUserCards,
  UserCard
} from './home.styles'


export default function Home () {

  return (
    <HomeContainer>
      <UserNav>
        <a href="/">Inicio</a>
        <div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
        {/* <figure>
          <img src="https://tractilus.s3.amazonaws.com/Icon+ionic-ios-menu.svg" alt="menu de hamburguesa" />
        </figure>  */}
      </UserNav>
      <ChooseUser>
        <ChooseUserTitle>
          <h2>Elija el tipo de producto a cargar</h2>
        </ChooseUserTitle>
        <ChooseUserCards>
          <UserCard>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/01-implementos-mecanizacion-de-tierra-arado.png" alt="productor de Tractilus" />
            </figure>
            <div>
              <h2>Implementos</h2>
              <a href="/proveedor/inicio/agrega-productos/implementos/inventario">Cargar</a>
            </div>
          </UserCard>
          <UserCard>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/iStock-1282580234%402x.png" alt="productor de Tractilus" />
            </figure>
            <div>
              <h2>Insumos</h2>
              <a href="/proveedor/inicio/agrega-productos/insumos/inventario">Cargar</a>
            </div>
          </UserCard>
          <UserCard>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/iStock-507279814%402x.png" alt="productor de Tractilus" />
            </figure>
            <div>
              <h2>Maquilas</h2>
              <a href="/proveedor/inicio/agrega-productos/maquila/inventario">Cargar</a>
            </div>
          </UserCard>
          <UserCard>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/consulting-service%402x.png" alt="productor de Tractilus" />
            </figure>
            <div>
              <h2>Asistencia técnica</h2>
              <a href="/proveedor/inicio/agrega-productos/asistencia-tecnica/inventario">Cargar</a>
            </div>
          </UserCard>
        </ChooseUserCards>
      </ChooseUser>
    </HomeContainer>
  )
}