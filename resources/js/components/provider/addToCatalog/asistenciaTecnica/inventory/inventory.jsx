import React, { useState } from 'react'

import {
  AsistenciaTecnicaContainer,
  FormAsistenciaTecnica,
  FormAsistenciaTecnicaTitle,
  FormAsistenciaTecnicaCards,
  AsistenciaTecnicaButtons,
} from './inventory.styles'

import AsistenciaTecnicaCard from '../../../../layout/asistenciaTecnicaCard/asistenciaTecnicaCard'
import ProviderNav from '../../../../layout/provider/nav/nav'

export default function AsistenciaTecnica () {
  const [ asistenciaTecnicaData, setAsistenciaTecnicaData ] = useState([1,2])

  return (
    <AsistenciaTecnicaContainer>
      <ProviderNav />
      <FormAsistenciaTecnica>
        <FormAsistenciaTecnicaTitle>
          <h2>Asistencia Tecnica </h2>
        </FormAsistenciaTecnicaTitle>
        <FormAsistenciaTecnicaCards>
          <a href='/proveedor/inicio/agrega-productos/asistencia-tecnica/formulario'>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/add/addBlack.svg" alt="Icomo agregar" />
            </figure>
            <span>Agregar nuevo(a) asistente técnico(a)</span>
          </a>
          <AsistenciaTecnicaCard/>
          <AsistenciaTecnicaButtons>
            <a href='/proveedor/inicio/agrega-productos/'>Regresar</a>
          </AsistenciaTecnicaButtons>
        </FormAsistenciaTecnicaCards>
      </FormAsistenciaTecnica>
    </AsistenciaTecnicaContainer>
  )
}