import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import TechnicalAssistanceApi from '../../../../api/technicalAssistanceRepository';

import {
  AsistenciaTecnicaContainer,
  FormAsistenciaTecnica,
  FormAsistenciaTecnicaTitle,
  FormAsistenciaTecnicaCards,
  TypeSpecialist,
  TypeSystemInput,
  TypeCultivo,
  Photos,
  AsistenciaTecnicaButtons,
  PhotosTitle,
  PhotoUpload
} from './form.styles'

import ProviderNav from '../../../../layout/provider/nav/nav'

import Specialities from '../../../../../constants/specialities'


export default function AsistenciaTecnica (producto) {
    const [datos, setDatos] = useState({
        name: '',
        degree: '',
        speciality: '',
        typeSpecialist:'',
        convencional: '',
        conservacion:'',
        organica:'',
        otra: '',
        specialties:'',
        typeCultivo: '',
        cost:''
      })
    const [datosFile, setDatosFile] = useState({
      image1: '',
      image2: '',
      image3: '',
      image4: ''
      })
    const handleInputChangeFile = (event) => {
      setDatosFile({
          ...datosFile,
          [event.target.name] : event.target.files[0]
      })
    }
    const handleInputChange = (event) => {
      setDatos({
          ...datos,
          [event.target.name] : event.target.value
      })
    }

  const sendDataToServer = () => {
    const data = {
        id:producto.id,
        name: datos.name === '' ? producto.name : datos.name,
        degree:datos.degree === '' ? producto.degree : datos.degree ,
        speciality:datos.speciality === '' ? producto.speciality : datos.speciality,
        typeSpecialist:datos.typeSpecialist === '' ? producto.typeSpecialist : datos.typeSpecialist,
        convencional:datos.convencional === '' ? producto.convencional : datos.convencional,
        conservacion:datos.conservacion === '' ? producto.conservacion : datos.conservacion,
        organica:datos.organica === '' ? producto.organica : datos.organica,
        otra:datos.otra === '' ? producto.otra : datos.otra,
        specialties:datos.specialties === '' ? producto.specialties : datos.specialties,
        typeCultivo:datos.typeCultivo === '' ? producto.typeCultivo : datos.typeCultivo,
        cost:datos.cost === '' ? producto.cost : datos.cost,
        image1:datosFile.image1  === '' ? producto.image1 : datosFile.image1,
        image2:datosFile.image2  === '' ? producto.image1 : datosFile.image2,
        image3:datosFile.image3  === '' ? producto.image1 : datosFile.image3,
        image4:datosFile.image4  === '' ? producto.image1 : datosFile.image4,
        user_id:localStorage.getItem("id"),
      }

    TechnicalAssistanceApi.updateTechnicalAssistance(data)
      .then(r => {
          navigate('/proveedor/inicio/agrega-productos/asistencia-tecnica/inventario')
      }).catch(e => {
          alert("No se pudo registrar la asistencia tecnica, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  return (
    <AsistenciaTecnicaContainer>
      <ProviderNav />
      <FormAsistenciaTecnica>
        <FormAsistenciaTecnicaTitle>
          <h2>Asistencia Técnica</h2>
        </FormAsistenciaTecnicaTitle>
        <FormAsistenciaTecnicaCards>
          <div>
            <p>Nombre del especialista:</p>
            <input onChange={handleInputChange} type="text" name='name' defaultValue={producto.name}/>
          </div>
          <div>
            <p>Grado de estudios:</p>
            <input onChange={handleInputChange} type="text" name='degree' defaultValue={producto.degree}/>
          </div>
          <div>
            <p>Especialidad:</p>
            <input onChange={handleInputChange} type="text" name='speciality' defaultValue={producto.speciality}/>
          </div>
          <TypeSpecialist>
            <h3>Técnico especializado:</h3>
            <div>
              <div>
                <p>Si</p>
                <input onChange={handleInputChange} type="radio" name= "typeSpecialist" value={1} defaultValue={producto.typeSpecialist}/>
              </div>
              <div>
                <p>No</p>
                <input onChange={handleInputChange} type="radio" name= "typeSpecialist" value={0} defaultValue={producto.typeSpecialist}/>
              </div>
            </div>
          </TypeSpecialist>
          <TypeSystemInput>
            <h3>Ha trabajado en sistemas de agricultura</h3>
            <div>
              <div>
                <p>Convencional</p>
                <input onChange={handleInputChange} type="text" name= "convencional" defaultValue={producto.convencional}/>
              </div>
              <div>
                <p>Conservación</p>
                <input onChange={handleInputChange} type="text" name= "conservacion" defaultValue={producto.conservacion}/>
              </div>
              <div>
                <p>Orgánica</p>
                <input onChange={handleInputChange} type="text" name= "organica" defaultValue={producto.organica}/>
              </div>
              <div>
                <p>Otra</p>
                <input onChange={handleInputChange} type="text" name= "otra" defaultValue={producto.otra}/>
              </div>
            </div>
          </TypeSystemInput>
          <div>
            <p>Especialidades:</p>
            <select onChange={handleInputChange} name="specialties" defaultValue={producto.specialties}>
              {Specialities.map( s => <option value={s}>{s}</option>)}
            </select>
          </div>
          <TypeCultivo>
            <h3>Cultivo de especialidad (tipos de cultivo):</h3>
            <div>
              <div>
                <p>Maiz</p>
                <input onChange={handleInputChange} type="radio" name= "typeCultivo" value='maiz'/>
              </div>
              <div>
                <p>Sorgo</p>
                <input onChange={handleInputChange} type="radio" name= "typeCultivo" value='sorgo'/>
              </div>
              <div>
                <p>Trigo</p>
                <input onChange={handleInputChange} type="radio" name= "typeCultivo" value='trigo'/>
              </div>
              <div>
                <p>Frijol</p>
                <input onChange={handleInputChange} type="radio" name= "typeCultivo" value='frijol'/>
              </div>
              <div>
                <p>Calabaza</p>
                <input onChange={handleInputChange} type="radio" name= "typeCultivo" value='calabaza'/>
              </div>
              <div>
                <p>Otro</p>
                <input onChange={handleInputChange} type="text" name= "typeCultivo" />
              </div>
            </div>
          </TypeCultivo>
          <div>
            <p>Costo por asesoría:</p>
            <input onChange={handleInputChange} type="number" name='cost' defaultValue={producto.cost}/>
          </div>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <AsistenciaTecnicaButtons>
            <div>
              <button onClick={sendDataToServer}>Finalizar</button>
            </div>
          </AsistenciaTecnicaButtons>
        </FormAsistenciaTecnicaCards>
      </FormAsistenciaTecnica>
    </AsistenciaTecnicaContainer>
  )
}