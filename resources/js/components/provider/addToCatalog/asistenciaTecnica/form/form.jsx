import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import TechnicalAssistanceApi from '../../../../api/technicalAssistanceRepository';

import {
  AsistenciaTecnicaContainer,
  FormAsistenciaTecnica,
  FormAsistenciaTecnicaTitle,
  FormAsistenciaTecnicaCards,
  TypeSpecialist,
  TypeSystemInput,
  TypeCultivo,
  Photos,
  AsistenciaTecnicaButtons,
  PhotosTitle,
  PhotoUpload
} from './form.styles'

import ProviderNav from '../../../../layout/provider/nav/nav'

import Specialities from '../../../../../constants/specialities'


export default function AsistenciaTecnica () {
  const [ asistenciaTecnicaData, setAsistenciaTecnicaData ] = useState({})
  let navigate = useNavigate()

  const handleData = e => asistenciaTecnicaData[e.target.name] = e.target.value

  const handleFile = e => asistenciaTecnicaData[e.target.name] = e.target.files[0]

  const sendDataToServer = () => {
    const data = new FormData();
    data.append("name", asistenciaTecnicaData.name);
    data.append("degree", asistenciaTecnicaData.degree);
    data.append("speciality", asistenciaTecnicaData.speciality);
    data.append("typeSpecialist", asistenciaTecnicaData.typeSpecialist);
    data.append("convencional", asistenciaTecnicaData.convencional);
    data.append("conservacion", asistenciaTecnicaData.conservacion);
    data.append("organica", asistenciaTecnicaData.organica);
    data.append("otra", asistenciaTecnicaData.otra);
    data.append("specialties", asistenciaTecnicaData.specialties);
    data.append("typeCultivo", asistenciaTecnicaData.typeCultivo);
    data.append("cost", asistenciaTecnicaData.cost);
    data.append("image1", asistenciaTecnicaData.image1);
    data.append("image2", asistenciaTecnicaData.image2);
    data.append("image3", asistenciaTecnicaData.image3);
    data.append("image4", asistenciaTecnicaData.image4);
    data.append("user_id", localStorage.getItem("id"));

    TechnicalAssistanceApi.newTechnicalAssistance(data)
      .then(r => {
          navigate('/proveedor/inicio/agrega-productos/asistencia-tecnica/inventario')
      }).catch(e => {
          alert("No se pudo registrar la asistencia tecnica, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  const endAndSendDataToServer = () => {
    const data = new FormData();
    data.append("name", asistenciaTecnicaData.name);
    data.append("degree", asistenciaTecnicaData.degree);
    data.append("speciality", asistenciaTecnicaData.speciality);
    data.append("typeSpecialist", asistenciaTecnicaData.typeSpecialist);
    data.append("convencional", asistenciaTecnicaData.convencional);
    data.append("conservacion", asistenciaTecnicaData.conservacion);
    data.append("organica", asistenciaTecnicaData.organica);
    data.append("otra", asistenciaTecnicaData.otra);
    data.append("specialties", asistenciaTecnicaData.specialties);
    data.append("typeCultivo", asistenciaTecnicaData.typeCultivo);
    data.append("cost", asistenciaTecnicaData.cost);
    data.append("image1", asistenciaTecnicaData.image1);
    data.append("image2", asistenciaTecnicaData.image2);
    data.append("image3", asistenciaTecnicaData.image3);
    data.append("image4", asistenciaTecnicaData.image4);
    data.append("user_id", localStorage.getItem("id"));

    TechnicalAssistanceApi.newTechnicalAssistance(data)
      .then(r => {
          location.reload()
      }).catch(e => {
          alert("No se pudo registrar la asistencia tecnica, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  return (
    <AsistenciaTecnicaContainer>
      <ProviderNav />
      <FormAsistenciaTecnica>
        <FormAsistenciaTecnicaTitle>
          <h2>Asistencia Técnica</h2>
        </FormAsistenciaTecnicaTitle>
        <FormAsistenciaTecnicaCards>
          <div>
            <p>Nombre del especialista:</p>
            <input onChange={handleData} type="text" name='name' />
          </div>
          <div>
            <p>Grado de estudios:</p>
            <input onChange={handleData} type="text" name='degree' />
          </div>
          <div>
            <p>Especialidad:</p>
            <input onChange={handleData} type="text" name='speciality' />
          </div>
          <TypeSpecialist>
            <h3>Técnico especializado:</h3>
            <div>
              <div>
                <p>Si</p>
                <input onChange={handleData} type="radio" name= "typeSpecialist" value={1}/>
              </div>
              <div>
                <p>No</p>
                <input onChange={handleData} type="radio" name= "typeSpecialist" value={0}/>
              </div>
            </div>
          </TypeSpecialist>
          <TypeSystemInput>
            <h3>Ha trabajado en sistemas de agricultura</h3>
            <div>
              <div>
                <p>Convencional</p>
                <input onChange={handleData} type="text" name= "convencional" />
              </div>
              <div>
                <p>Conservación</p>
                <input onChange={handleData} type="text" name= "conservacion" />
              </div>
              <div>
                <p>Orgánica</p>
                <input onChange={handleData} type="text" name= "organica" />
              </div>
              <div>
                <p>Otra</p>
                <input onChange={handleData} type="text" name= "otra" />
              </div>
            </div>
          </TypeSystemInput>
          <div>
            <p>Especialidades:</p>
            <select onChange={handleData} name="specialties">
              {Specialities.map( s => <option value={s}>{s}</option>)}
            </select>
          </div>
          <TypeCultivo>
            <h3>Cultivo de especialidad (tipos de cultivo):</h3>
            <div>
              <div>
                <p>Maiz</p>
                <input onChange={handleData} type="radio" name= "typeCultivo" value='maiz'/>
              </div>
              <div>
                <p>Sorgo</p>
                <input onChange={handleData} type="radio" name= "typeCultivo" value='sorgo'/>
              </div>
              <div>
                <p>Trigo</p>
                <input onChange={handleData} type="radio" name= "typeCultivo" value='trigo'/>
              </div>
              <div>
                <p>Frijol</p>
                <input onChange={handleData} type="radio" name= "typeCultivo" value='frijol'/>
              </div>
              <div>
                <p>Calabaza</p>
                <input onChange={handleData} type="radio" name= "typeCultivo" value='calabaza'/>
              </div>
              <div>
                <p>Otro</p>
                <input onChange={handleData} type="text" name= "typeCultivo" />
              </div>
            </div>
          </TypeCultivo>
          <div>
            <p>Costo por asesoría:</p>
            <input onChange={handleData} type="number" name='cost' />
          </div>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <AsistenciaTecnicaButtons>
            <div>
              <button onClick={endAndSendDataToServer}>Finalizar y cargar otro</button>
              <button onClick={sendDataToServer}>Finalizar</button>
            </div>
            <a href='/proveedor/inicio/agrega-productos/'>Regresar</a>
          </AsistenciaTecnicaButtons>
        </FormAsistenciaTecnicaCards>
      </FormAsistenciaTecnica>
    </AsistenciaTecnicaContainer>
  )
}