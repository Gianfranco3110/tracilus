import styled from 'styled-components'

import { asistenciaTecnicaGray, whiteTractilus, greenLightTractilus } from '../../../../../styles/colorStyles'

export const AsistenciaTecnicaContainer = styled.main `
`

export const FormAsistenciaTecnica = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
  width: 80%;
  margin: auto;
`

export const FormAsistenciaTecnicaTitle = styled.div `
  margin-bottom: 8%;

  > h2 {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 2.2vw;
    color: ${asistenciaTecnicaGray};
  }
`


export const FormAsistenciaTecnicaCards = styled.div `
  width: 60%;
  margin: auto;

  > div {
    margin-top: 2.5%;
    margin-bottom: 2.5%;

    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${asistenciaTecnicaGray};
      margin-bottom: 2%;
    }

    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${asistenciaTecnicaGray};
      margin-bottom: 2.5%;
      width: 90%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${asistenciaTecnicaGray};
      padding-left: 2%;
      outline: none;
    }

    > select {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${asistenciaTecnicaGray};
      margin-bottom: 2.5%;
      width: 90%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${asistenciaTecnicaGray};
      padding-left: 2%;
      outline: none;

      > option {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${asistenciaTecnicaGray};
      }
    }
  }
`

export const TypeSpecialist = styled.div `
  > h3 {
    font-family: 'Montserrat';
    font-size: 1.2vw;
    color: ${asistenciaTecnicaGray};
    margin-bottom: 2%;
  }

  > div {
    display: flex;
    justify-content: space-between;
    width: 90%;
    margin-top: 2.5%;
    margin-bottom: 5%;
    
    > div {
      display: flex;
      justify-content: flex-end;
      gap: 5%;
      width: 30%;

      > p {
        font-family: 'Montserrat';
        font-size: 1.2vw;
        color: ${asistenciaTecnicaGray};
        margin-left: 5%;
      }

      > input {
        height: 30px;
        width: 30px;
        accent-color: ${asistenciaTecnicaGray};
      }
    }
  }
`


export const TypeSystemInput = styled.div `
  > h3 {
    font-family: 'Montserrat';
    font-size: 1.2vw;
    color: ${asistenciaTecnicaGray};
    margin-bottom: 2%;
  }

  > div {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    width: 90%;
    margin-top: 2.5%;
    margin-bottom: 5%;
    
    > div {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      gap: 2.5%;
      width: 45%;
      margin-bottom: 2.5%;

      > p {
        font-family: 'Montserrat';
        font-size: 1.2vw;
        color: ${asistenciaTecnicaGray};
        margin-left: 5%;
      }

      > input {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${asistenciaTecnicaGray};
        width: 50%;
        height: 45px;
        border-radius: 10px;
        border: 1px solid ${asistenciaTecnicaGray};
        padding-left: 2%;
        outline: none;
      }
    }
  }
`

export const TypeCultivo = styled.div `
  > h3 {
    font-family: 'Montserrat';
    font-size: 1.2vw;
    color: ${asistenciaTecnicaGray};
    margin-bottom: 2%;
  }

  > div {
    display: flex;
    flex-wrap: wrap;
    width: 90%;
    margin-top: 5%;
    margin-bottom: 5%;
    
    > div {
      display: flex;
      justify-content: space-between;
      gap: 5%;
      width: 30%;
      margin-bottom: 5%;
      margin-right: 2.5%;

      > p {
        font-family: 'Montserrat';
        font-size: 1.2vw;
        color: ${asistenciaTecnicaGray};
        margin-left: 5%;
      }

      > input[type='radio'] {
        height: 30px;
        width: 30px;
        accent-color: ${asistenciaTecnicaGray};
      }

      > input[type='text'] {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${asistenciaTecnicaGray};
        width: 100px;
        height: 30px;
        border-radius: 10px;
        border: 1px solid ${asistenciaTecnicaGray};
        padding-left: 5%;
        outline: none;
      }
    }
  }
`

export const Photos = styled.div `
  width: 90%;
`

export const PhotosTitle = styled.div `
    display: flex;
    align-items: center;
    margin-top: 5%;
    margin-bottom: 5%;

    > figure {
      width: 30px;
      height: 30px;
      margin-right: 2.5%;

      > img {
        width: 100%;
        height: 100%;
      }
    }

    > h3 {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${asistenciaTecnicaGray};
    }
`

export const PhotoUpload = styled.div `
  display: flex;
  justify-content: space-between;
  margin-bottom: 10%;

  > div {
    display: flex;
    width: 130px;
    height: 130px;
    border-radius: 20px;
    box-shadow: 0px 3px 6px #00000029;
    cursor: pointer;

    > input {
      visibility: hidden;
      width: 0;
    }

    > label {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 130px;
      height: 130px;
      cursor: pointer;
      

      > figure {
        width: 35px;
        height: 35px;

        > img {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
`


export const AsistenciaTecnicaButtons = styled.div `
  width: 90%;
  
  > div {
    display: flex;
    justify-content: space-around;
    align-items: center;
    gap: 3%;
    margin-bottom: 5%;

    > button {
      font-family: 'Montserrat';
      font-weight: 600;
      font-size: .8vw;
      color: ${whiteTractilus};
      background-color: ${asistenciaTecnicaGray};
      border: none;
      width: 35%;
      padding: 2% 6%;
      border-radius: 20px;
      cursor: pointer;
      transition: all 0.25s linear;
  
      &:hover {
        background-color: ${greenLightTractilus};
        transform: translateY(-3px);
        box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
      }
    }
  }

  > a {
    display: block;
    text-align: center;
    text-decoration: none;
    color: ${whiteTractilus};
    font-size: .8vw;
    font-family: 'Montserrat';
    font-weight: 600;
    background-color: ${asistenciaTecnicaGray};
    width: 33%;
    margin: auto;
    padding: 2% 6%;
    border-radius: 20px;
    transition: all 0.25s linear;

    &:hover {
      background-color: ${greenLightTractilus};
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }



`
