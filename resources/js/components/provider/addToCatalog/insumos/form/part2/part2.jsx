import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import SupplieApi from '../../../../../api/supplieRepository';

import {
  InsumosContainer,
  FormInsumos,
  FormInsumosTitle,
  FormInsumosCards,
  TypeContainer,
  TypeContainerInput,
  Photos,
  PhotosTitle,
  PhotoUpload,
  InsumosButtons
} from './part2.styles'

import ProviderNav from '../../../../../layout/nav/nav'
 

export default function Insumos () {
  const [ insumosData, setInsumosData ] = useState({})
  let navigate = useNavigate()

  const handleData = e => insumosData[e.target.name] = e.target.value

  const handleFile = e => insumosData[e.target.name] = e.target.files[0]

  const sendDataToServer = () => {
    const data = new FormData();
    data.append("typeSupplie", localStorage.getItem("typeSupplie"));
    data.append("otherSupplie", localStorage.getItem("otherSupplie"));
    data.append("name", insumosData.name);
    data.append("typeContainer", insumosData.typeContainer);
    data.append("otherContainer", insumosData.otherContainer);
    data.append("inventory", insumosData.inventory);
    data.append("unitPrice", insumosData.unitPrice);
    data.append("image1", insumosData.image1);
    data.append("image2", insumosData.image2);
    data.append("image3", insumosData.image3);
    data.append("image4", insumosData.image4);
    data.append("user_id", localStorage.getItem("id"));

    SupplieApi.newSupplie(data)
      .then(r => {
          localStorage.removeItem('typeSupplie');
          localStorage.removeItem('otherSupplie');
          navigate('/proveedor/inicio/agrega-productos/insumos/inventario')
      }).catch(e => {
          alert("No se pudo registrar el insumo, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  const endAndSendDataToServer = () => {
    const data = new FormData();
    data.append("typeSupplie", localStorage.getItem("typeSupplie"));
    data.append("otherSupplie", localStorage.getItem("otherSupplie"));
    data.append("name", insumosData.name);
    data.append("typeContainer", insumosData.typeContainer);
    data.append("otherContainer", insumosData.otherContainer);
    data.append("inventory", insumosData.inventory);
    data.append("unitPrice", insumosData.unitPrice);
    data.append("image1", insumosData.image1);
    data.append("image2", insumosData.image2);
    data.append("image3", insumosData.image3);
    data.append("image4", insumosData.image4);
    data.append("user_id", localStorage.getItem("id"));

    SupplieApi.newSupplie(data)
      .then(r => {
          localStorage.removeItem('typeSupplie');
          localStorage.removeItem('otherSupplie');
          navigate('/proveedor/inicio/agrega-productos/insumos/formulario/parte-1')
      }).catch(e => {
          alert("No se pudo registrar el insumo, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  return (
    <InsumosContainer>
      <ProviderNav/>
      <FormInsumos>
        <FormInsumosTitle>
          <h2>Insumos</h2>
        </FormInsumosTitle>
        <FormInsumosCards>
          <div>
            <p>Nombre:</p>
            <input onChange={handleData} type="text" name='name' />
          </div>
          <TypeContainer>
            <h3>Presentación:</h3>
            <div>
              <div>
                <p>Costal</p>
                <input onChange={handleData} type="radio" name= "typeContainer" value='costal'/>
              </div>
              <div>
                <p>Frasco</p>
                <input onChange={handleData} type="radio" name= "typeContainer" value='frasco'/>
              </div>
              <div>
                <p>Granel</p>
                <input onChange={handleData} type="radio" name= "typeContainer" value='granel'/>
              </div>
            </div>
          </TypeContainer>
          <div>
            <p>Otro:</p>
            <input onChange={handleData} type="text" name='otherContainer' />
          </div>
          <TypeContainerInput>
            <div>
              <div>
                <p>Cantidad Existente de producto</p>
                <input onChange={handleData} type="text" name= "inventory" />
              </div>
              <div>
                <p>Precio Unitario</p>
                <input onChange={handleData} type="text" name= "unitPrice" />
              </div>
            </div>
          </TypeContainerInput>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <InsumosButtons>
            <div>
              <button onClick={endAndSendDataToServer}>Finalizar y cargar otro</button>
              <button onClick={sendDataToServer}>Finalizar</button>
            </div>
            <a href='/proveedor/inicio/agrega-productos/'>Regresar</a>
          </InsumosButtons>
        </FormInsumosCards>
      </FormInsumos>
    </InsumosContainer>
  )
}