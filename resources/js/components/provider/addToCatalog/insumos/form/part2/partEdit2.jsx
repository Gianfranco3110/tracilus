import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import SupplieApi from '../../../../../api/supplieRepository';

import {
  InsumosContainer,
  FormInsumos,
  FormInsumosTitle,
  FormInsumosCards,
  TypeContainer,
  TypeContainerInput,
  Photos,
  PhotosTitle,
  PhotoUpload,
  InsumosButtons
} from './part2.styles'

import ProviderNav from '../../../../../layout/nav/nav'
 

export default function Insumos (producto) {
  let navigate = useNavigate()
  const [datos, setDatos] = useState({
    typeSupplie: '',
    otherSupplie: '',
    name: '',
    typeContainer:'',
    otherContainer: '',
    inventory:'',
    unitPrice: '',
})
const [datosFile, setDatosFile] = useState({
  image1: '',
  image2: '',
  image3: '',
  image4: '',
})
const handleInputChangeFile = (event) => {
  setDatosFile({
      ...datosFile,
      [event.target.name] : event.target.files[0]
  })
}
const handleInputChange = (event) => {
  setDatos({
      ...datos,
      [event.target.name] : event.target.value
  })
}
  const sendDataToServer = () => {
    const data = {
        id:producto.id,
        typeSupplie: datos.typeSupplie === '' ? producto.typeSupplie : datos.typeSupplie,
        otherSupplie:datos.otherSupplie === '' ? producto.otherSupplie : datos.otherSupplie ,
        name:datos.name === '' ? producto.name : datos.name,
        typeContainer:datos.typeContainer === '' ? producto.typeContainer : datos.typeContainer,
        otherContainer:datos.otherContainer === '' ? producto.otherContainer : datos.otherContainer,
        inventory:datos.inventory === '' ? producto.inventory : datos.inventory,
        unitPrice:datos.unitPrice === '' ? producto.unitPrice : datos.unitPrice,
        image1:datosFile.image1  === '' ? producto.image1 : datosFile.image1,
        image2:datosFile.image2  === '' ? producto.image1 : datosFile.image2,
        image3:datosFile.image3  === '' ? producto.image1 : datosFile.image3,
        image4:datosFile.image4  === '' ? producto.image1 : datosFile.image4,
        user_id:localStorage.getItem("id"),
    }
    SupplieApi.updateSupplie(data)
      .then(r => {
          navigate('/proveedor/inicio/agrega-productos/insumos/inventario')
      }).catch(e => {
          alert("No se pudo registrar el insumo, revise sus datos ingresados e intentelo de nuevo");
      });
  }
  return (
    <InsumosContainer>
      <ProviderNav/>
      <FormInsumos>
        <FormInsumosTitle>
          <h2>Insumos</h2>
        </FormInsumosTitle>
        <FormInsumosCards>
        <TypeInsumo>
            <h3>Tipo de insumo:</h3>
            <div>
              <div>
                <p>Semilla</p>
                <input onChange={handleInputChange} type="radio" name= "typeSupplie" value='semilla'/>
              </div>
              <div>
                <p>Fertilizante</p>
                <input onChange={handleInputChange} type="radio" name= "typeSupplie" value='fertilizante'/>
              </div>
              <div>
                <p>Herbicida</p>
                <input onChange={handleInputChange} type="radio" name= "typeSupplie" value='herbicida'/>
              </div>
              <div>
                <p>Insecticida</p>
                <input onChange={handleInputChange} type="radio" name= "typeSupplie" value='insecticida'/>
              </div>
              <div>
                <p>Fungicida</p>
                <input onChange={handleInputChange} type="radio" name= "typeSupplie" value='fungicida'/>
              </div>
            </div>
          </TypeInsumo>
          <div>
            <p>Nombre:</p>
            <input onChange={handleInputChange} type="text" name='name' defaultValue={producto.name}/>
          </div>
          <TypeContainer>
            <h3>Presentación:</h3>
            <div>
              <div>
                <p>Costal</p>
                <input onChange={handleInputChange} type="radio" name= "typeContainer" value='costal'/>
              </div>
              <div>
                <p>Frasco</p>
                <input onChange={handleInputChange} type="radio" name= "typeContainer" value='frasco'/>
              </div>
              <div>
                <p>Granel</p>
                <input onChange={handleInputChange} type="radio" name= "typeContainer" value='granel'/>
              </div>
            </div>
          </TypeContainer>
          <div>
            <p>Otro:</p>
            <input onChange={handleInputChange} type="text" name='otherContainer' defaultValue={producto.otherContainer}/>
          </div>
          <TypeContainerInput>
            <div>
              <div>
                <p>Cantidad Existente de producto</p>
                <input onChange={handleInputChange} type="text" name= "inventory" defaultValue={producto.inventory}/>
              </div>
              <div>
                <p>Precio Unitario</p>
                <input onChange={handleInputChange} type="text" name= "unitPrice" defaultValue={producto.unitPrice}/>
              </div>
            </div>
          </TypeContainerInput>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <InsumosButtons>
            <div>
              <button onClick={sendDataToServer}>Finalizar</button>
            </div>
          </InsumosButtons>
        </FormInsumosCards>
      </FormInsumos>
    </InsumosContainer>
  )
}