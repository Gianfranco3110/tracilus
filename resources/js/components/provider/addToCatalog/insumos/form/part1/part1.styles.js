import styled from 'styled-components'

import { blueDarkTractilus, greyDarkTractilus, orangeTractilus, whiteTractilus, greenLightTractilus } from '../../../../../../styles/colorStyles'

export const InsumosContainer = styled.main `
`

export const FormInsumos = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
  width: 80%;
  margin: auto;
`

export const FormInsumosTitle = styled.div `
  margin-bottom: 8%;

  > h2 {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 2.2vw;
    color: rgba(28, 67, 141, 1);
  }
`


export const FormInsumosCards = styled.div `
  width: 60%;
  margin: auto;

  > div {
    margin-top: 2.5%;
    margin-bottom: 2.5%;

    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: rgba(28, 67, 141, 1);
      margin-bottom: 2%;
    }

    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: rgba(28, 67, 141, 1);
      margin-bottom: 2.5%;
      width: 90%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid rgba(28, 67, 141, 1);
      padding-left: 2%;
      outline: none;
    }
  }
`

export const TypeInsumo = styled.div `
  > h3 {
    font-family: 'Montserrat';
    font-size: 1.2vw;
    color: rgba(28, 67, 141, 1);
    margin-bottom: 2%;
  }

  > div {
    display: flex;
    flex-wrap: wrap;
    width: 90%;
    margin-top: 5%;
    margin-bottom: 5%;
    
    > div {
      display: flex;
      justify-content: space-between;
      gap: 5%;
      width: 30%;
      margin-bottom: 5%;
      margin-right: 2.5%;

      > p {
        font-family: 'Montserrat';
        font-size: 1.2vw;
        color: rgba(28, 67, 141, 1);
        margin-left: 5%;
      }

      > input {
        height: 30px;
        width: 30px;
      }
    }
  }
`


export const InsumosButtons = styled.div `
  display: flex;
  justify-content: space-around;
  align-items: center;
  gap: 3%;
  width: 90%;

  > button {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: .8vw;
    color: ${whiteTractilus};
    background-color: rgba(28, 67, 141, 1);
    border: none;
    width: 35%;
    padding: 2% 6%;
    border-radius: 20px;
    cursor: pointer;
    transition: all 0.25s linear;

    &:hover {
      background-color: ${greenLightTractilus};
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }

  > a {
    display: block;
    text-align: center;
    text-decoration: none;
    color: ${whiteTractilus};
    font-size: .8vw;
    font-family: 'Montserrat';
    font-weight: 600;
    background-color: rgba(28, 67, 141, 1);
    width: 33%;
    padding: 2% 6%;
    border-radius: 20px;
    transition: all 0.25s linear;

    &:hover {
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }



`
