import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

import ProviderNav from '../../../../../layout/provider/nav/nav'

import {
  InsumosContainer,
  FormInsumos,
  FormInsumosTitle,
  FormInsumosCards,
  TypeInsumo,
  InsumosButtons
} from './part1.styles'

export default function Insumos () {
  const [ insumosData, setInsumosData ] = useState({})
  let navigate = useNavigate()

  const handleData = e => {
    insumosData[e.target.name] = e.target.value
  }

  const sendDataToServer = () => {

    if(Object.keys(insumosData).length ===1) {
      localStorage.setItem('typeSupplie', insumosData.typeSupplie);
      localStorage.setItem('otherSupplie', insumosData.otherSupplie);
      navigate('/proveedor/inicio/agrega-productos/insumos/formulario/parte-2')
    }
  }

  return (
    <InsumosContainer>
      <ProviderNav/>
      <FormInsumos>
        <FormInsumosTitle>
          <h2>Insumos</h2>
        </FormInsumosTitle>
        <FormInsumosCards>
          <TypeInsumo>
            <h3>Tipo de insumo:</h3>
            <div>
              <div>
                <p>Semilla</p>
                <input onChange={handleData} type="radio" name= "typeSupplie" value='semilla'/>
              </div>
              <div>
                <p>Fertilizante</p>
                <input onChange={handleData} type="radio" name= "typeSupplie" value='fertilizante'/>
              </div>
              <div>
                <p>Herbicida</p>
                <input onChange={handleData} type="radio" name= "typeSupplie" value='herbicida'/>
              </div>
              <div>
                <p>Insecticida</p>
                <input onChange={handleData} type="radio" name= "typeSupplie" value='insecticida'/>
              </div>
              <div>
                <p>Fungicida</p>
                <input onChange={handleData} type="radio" name= "typeSupplie" value='fungicida'/>
              </div>
            </div>
          </TypeInsumo>
          <div>
            <p>Otro:</p>
            <input onChange={handleData} type="text" name='otherSupplie' />
          </div>
          <InsumosButtons>
            <button onClick={sendDataToServer}>Continuar</button>
          </InsumosButtons>
        </FormInsumosCards>
      </FormInsumos>
    </InsumosContainer>
  )
}