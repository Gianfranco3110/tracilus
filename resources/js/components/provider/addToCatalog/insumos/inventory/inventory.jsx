import React, { useState } from 'react'

import {
  InsumosContainer,
  InsumosNav,
  FormInsumos,
  FormInsumosTitle,
  FormInsumosCards,
  InsumosButtons
} from './inventory.styles'

import InsumosCard from '../../../../layout/insumosCard/insumosCard'
import ProviderNav from '../../../../layout/provider/nav/nav'

export default function Insumos () {
  const [ insumosData, setInsumosData ] = useState([1,2,3])

  return (
    <InsumosContainer>
      <ProviderNav />
      <FormInsumos>
        <FormInsumosTitle>
          <h2>Insumos</h2>
        </FormInsumosTitle>
        <FormInsumosCards>
          <a href='/proveedor/inicio/agrega-productos/insumos/formulario/parte-1'>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/add/addBlue.svg" alt="Icomo agregar" />
            </figure>
            <span>Agregar nuevo insumo</span>
          </a>
           <InsumosCard/>
          <InsumosButtons>
            <a href='/proveedor/inicio/agrega-productos/'>Regresar</a>
          </InsumosButtons>
        </FormInsumosCards>
      </FormInsumos>
    </InsumosContainer>
  )
}