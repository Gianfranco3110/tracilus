import React, { useState } from 'react'

import {
  MaquilaContainer,
  FormMaquila,
  FormMaquilaTitle,
  FormMaquilaCards,
  MaquilaButtons,
} from './inventory.styles'

import MaquilaCard from '../../../../layout/maquilaCard/maquilaCard'
import ProviderNav from '../../../../layout/provider/nav/nav'

export default function Maquila () {
  const [ maquilaData, setMaquilaData ] = useState([1,2])

  return (
    <MaquilaContainer>
      <ProviderNav />
      <FormMaquila>
        <FormMaquilaTitle>
          <h2>Maquila</h2>
        </FormMaquilaTitle>
        <FormMaquilaCards>
          <a href='/proveedor/inicio/agrega-productos/maquila/formulario'>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/add/addPurple.svg" alt="Icomo agregar" />
            </figure>
            <span>Agregar nueva maquila</span>
          </a>
          <MaquilaCard/>
          <MaquilaButtons>
            <a href='/proveedor/inicio/agrega-productos/'>Regresar</a>
          </MaquilaButtons>
        </FormMaquilaCards>
      </FormMaquila>
    </MaquilaContainer>
  )
}