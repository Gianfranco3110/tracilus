import React, { useState } from 'react'

import MaquilaApi from '../../../../api/maquilaRepository';

import {
  MaquilaContainer,
  FormMaquila,
  FormMaquilaTitle,
  FormMaquilaCards,
  TypeTrigger,
  Photos,
  PhotosTitle,
  PhotoUpload,
  MaquilaButtons
} from './form.styles'

import ProviderNav from '../../../../layout/provider/nav/nav'

export default function Maquila (producto) {
  const [datos, setDatos] = useState({
    activity: '',
    brand: '',
    model: '',
    type:'',
    cost: '',
    name:''
  })
const [datosFile, setDatosFile] = useState({
  image1: '',
  image2: '',
  image3: '',
  image4: ''
  })
const handleInputChangeFile = (event) => {
  console.log('event',event.target.files[0]);
  setDatosFile({
      ...datosFile,
      [event.target.name] : event.target.files[0]
  })
}
const handleInputChange = (event) => {
  setDatos({
      ...datos,
      [event.target.name] : event.target.value
  })
}
  const sendDataToServer = () => {
    const data = {
        id:producto.id,
        activity: datos.activity === '' ? producto.activity : datos.activity,
        image1:datosFile.image1  === '' ? producto.image1 : datosFile.image1,
        image2:datosFile.image2  === '' ? producto.image1 : datosFile.image2,
        image3:datosFile.image3  === '' ? producto.image1 : datosFile.image3,
        image4:datosFile.image4  === '' ? producto.image1 : datosFile.image4,
        brand:datos.brand === '' ? producto.brand : datos.brand ,
        model:datos.model === '' ? producto.model : datos.model,
        name:datos.name === '' ? producto.name : datos.name,
        type:datos.type === '' ? producto.type : datos.type,
        cost:datos.cost === '' ? producto.cost : datos.cost,
        user_id:localStorage.getItem("id"),
      }
    MaquilaApi.updateMaquila(data)
      .then(r => {
          navigate('/proveedor/inicio/agrega-productos/maquila/inventario')
      }).catch(e => {
          alert("No se pudo registrar la maquila, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  return (
    <MaquilaContainer>
      <ProviderNav/>
      <FormMaquila>
        <FormMaquilaTitle>
          <h2>Editar Maquila</h2>
        </FormMaquilaTitle>
        <FormMaquilaCards>
          <div>
            <p>Actividad que realiza el implemento:</p>
            <input onChange={handleInputChange} type="text" name='activity' defaultValue={producto.activity} />
          </div>
          <div>
            <p>Marca:</p>
            <input onChange={handleInputChange} type="text" name='brand' defaultValue={producto.brand} />
          </div>
          <div>
            <p>Modelo:</p>
            <input onChange={handleInputChange} type="text" name='model' defaultValue={producto.model} />
          </div>
          <div>
            <p>Nombre del operador:</p>
            <input onChange={handleInputChange} type="text" name='name' defaultValue={producto.name}/>
          </div>
          <TypeTrigger>
            <p>Tipo de accionamiento:</p>
            <div>
              <div>
                <p>Tractor 4 ruedas</p>
                <input onChange={handleInputChange} type="radio" name= "type" value='fourWheels'/>
              </div>
              <div>
                <p>Motocultor</p>
                <input onChange={handleInputChange} type="radio" name= "type" value='twoWheels'/>
              </div>
              <div>
                <p>Potencia</p>
                <input onChange={handleInputChange} type="radio" name= "type" value='potencia'/>
              </div>
              <div>
                <p>Ancho de torque</p>
                <input onChange={handleInputChange} type="radio" name= "type" value='animal'/>
              </div>
            </div>
          </TypeTrigger>
          <div>
            <p>Costo de renta:</p>
            <input onChange={handleInputChange} type="number" name='cost' defaultValue={producto.cost}/>
          </div>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleInputChangeFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <MaquilaButtons>
          <div>
              <button onClick={sendDataToServer}>Finalizar</button>
            </div>
          </MaquilaButtons>
        </FormMaquilaCards>
      </FormMaquila>
    </MaquilaContainer>
  )
}