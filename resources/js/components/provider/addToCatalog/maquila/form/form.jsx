import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import MaquilaApi from '../../../../api/maquilaRepository';

import {
  MaquilaContainer,
  FormMaquila,
  FormMaquilaTitle,
  FormMaquilaCards,
  TypeTrigger,
  Photos,
  PhotosTitle,
  PhotoUpload,
  MaquilaButtons
} from './form.styles'

import ProviderNav from '../../../../layout/provider/nav/nav'

export default function Maquila () {
  const [ maquilaData, setMaquilaData ] = useState({})
  let navigate= useNavigate()

  const handleData = e => maquilaData[e.target.name] = e.target.value

  const handleFile = e => maquilaData[e.target.name] = e.target.files[0]

  const sendDataToServer = () => {
    const data = new FormData();
    data.append("activity", maquilaData.activity);
    data.append("brand", maquilaData.brand);
    data.append("model", maquilaData.model);
    data.append("name", maquilaData.name);
    data.append("type", maquilaData.type);
    data.append("cost", maquilaData.cost);
    data.append("image1", maquilaData.image1);
    data.append("image2", maquilaData.image2);
    data.append("image3", maquilaData.image3);
    data.append("image4", maquilaData.image4);
    data.append("user_id", localStorage.getItem("id"));

    MaquilaApi.newMaquila(data)
      .then(r => {
          navigate('/proveedor/inicio/agrega-productos/maquila/inventario')
      }).catch(e => {
          alert("No se pudo registrar la maquila, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  const endAndSendDataToServer = () => {
    const data = new FormData();
    data.append("activity", maquilaData.activity);
    data.append("brand", maquilaData.brand);
    data.append("model", maquilaData.model);
    data.append("name", maquilaData.name);
    data.append("type", maquilaData.type);
    data.append("cost", maquilaData.cost);
    data.append("image1", maquilaData.image1);
    data.append("image2", maquilaData.image2);
    data.append("image3", maquilaData.image3);
    data.append("image4", maquilaData.image4);
    data.append("user_id", localStorage.getItem("id"));

    MaquilaApi.newMaquila(data)
      .then(r => {
        location.reload()
      }).catch(e => {
          alert("No se pudo registrar la maquila, revise sus datos ingresados e intentelo de nuevo");
      });
  }

  return (
    <MaquilaContainer>
      <ProviderNav/>
      <FormMaquila>
        <FormMaquilaTitle>
          <h2>Maquila</h2>
        </FormMaquilaTitle>
        <FormMaquilaCards>
          <div>
            <p>Actividad que realiza el implemento:</p>
            <input onChange={handleData} type="text" name='activity' />
          </div>
          <div>
            <p>Marca:</p>
            <input onChange={handleData} type="text" name='brand' />
          </div>
          <div>
            <p>Modelo:</p>
            <input onChange={handleData} type="text" name='model' />
          </div>
          <div>
            <p>Nombre del operador:</p>
            <input onChange={handleData} type="text" name='name' />
          </div>
          <TypeTrigger>
            <p>Tipo de accionamiento:</p>
            <div>
              <div>
                <p>Tractor 4 ruedas</p>
                <input onChange={handleData} type="radio" name= "type" value='fourWheels'/>
              </div>
              <div>
                <p>Motocultor</p>
                <input onChange={handleData} type="radio" name= "type" value='twoWheels'/>
              </div>
              <div>
                <p>Potencia</p>
                <input onChange={handleData} type="radio" name= "type" value='potencia'/>
              </div>
              <div>
                <p>Ancho de torque</p>
                <input onChange={handleData} type="radio" name= "type" value='animal'/>
              </div>
            </div>
          </TypeTrigger>
          <div>
            <p>Costo de renta:</p>
            <input onChange={handleData} type="number" name='cost' />
          </div>
          <Photos>
            <PhotosTitle>
              <figure>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
              </figure>
              <h3>Subir fotos</h3>
            </PhotosTitle>
            <PhotoUpload>
              <div>
                <input onChange={handleFile} type="file" id='image1' name='image1' accept='image/*'/>
                <label htmlFor="image1">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image2' name='image2' accept='image/*'/>
                <label htmlFor="image2">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image3' name='image3' accept='image/*'/>
                <label htmlFor="image3">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
              <div>
                <input onChange={handleFile} type="file" id='image4' name='image4' accept='image/*'/>
                <label htmlFor="image4">
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="camara fotográfica" />
                  </figure>
                </label>
              </div>
            </PhotoUpload>
          </Photos>
          <MaquilaButtons>
          <div>
              <button onClick={endAndSendDataToServer}>Finalizar y cargar otro</button>
              <button onClick={sendDataToServer}>Finalizar</button>
            </div>
            <a href='/proveedor/inicio/agrega-productos/'>Regresar</a>
          </MaquilaButtons>
        </FormMaquilaCards>
      </FormMaquila>
    </MaquilaContainer>
  )
}