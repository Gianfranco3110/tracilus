import React, { Component, useState } from 'react'
import CountryAPI from '../../../../api/countryRepository';
import StateAPI from '../../../../api/stateRepository';
import BillAPI from '../../../../api/billRepository';

import {
  Part3Container,
  Part3Title,
  Status,
  Part3Form,
  Part3FormManual,
  FiscalAddress,
  CommercialAddress,
  BasicData
} from './part1.styles'

class Bill extends Component {
  constructor(props) {
    super(props);
    this.state = {
        countries: [],
        states: [],
        states2: [],
        fiscalData: false,
        newBill: {
            user_id: localStorage.getItem("id"),
            business_name: '',
            rfc: '',
            fiscal_domicile_country: '',
            fiscal_domicile_state: '',
            fiscal_domicile_municipality: '',
            fiscal_domicile_postal_code: '',
            fiscal_domicile_cologne: '',
            fiscal_domicile_street: '',
            fiscal_domicile_outdoor_number: '',
            fiscal_domicile_location: '',
            fiscal_domicile_location_reference: '',
            business_address_country: '',
            business_address_state: '',
            business_address_municipality: '',
            business_address_postal_code: '',
            business_address_cologne: '',
            business_address_street: '',
            business_address_outdoor_number: '',
            business_address_location: '',
            business_address_location_reference: '',
        }
    }

    this.sendDataToServer = this.sendDataToServer.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  componentDidMount() {
      CountryAPI.getCountry()
          .then(response => {
              return response.data;
          })
          .then(countries => {
              this.setState({ countries });
          }).catch(e => {
              console.log("Error getting countries");
          });
  }

  renderCountries () {
      return this.state.countries.map(country => {
          return (
              <option key={country.id.toString()} value={country.id}>
                  { country.country } 
              </option>    
          );
      })
  }

  renderStates () {
      return this.state.states.map(state => {
          return (
              <option key={state.id.toString()} value={state.id}>
                  { state.state } 
              </option>    
          );
      })
  }

  renderStates2 () {
    return this.state.states2.map(state => {
        return (
            <option key={state.id.toString()} value={state.id}>
                { state.state } 
            </option>    
        );
    })
}

  getStatesByCountry = (e) =>{
      let country =  e.target.value;
      var bill = Object.assign({}, this.state.newBill); 
      bill[e.target.name] = e.target.value;
      this.setState({newBill: bill });

      StateAPI.getStateByCountryId(country)
          .then(response => {
              return response.data;
          })
          .then(states => {
              this.setState({ states });
          }).catch(e => {
              console.log("Error getting states");
          });
  }

  getStatesByCountry2 = (e) =>{
    let country =  e.target.value;
    var bill = Object.assign({}, this.state.newBill); 
    bill[e.target.name] = e.target.value;
    this.setState({newBill: bill });

    StateAPI.getStateByCountryId(country)
        .then(response => {
            return response.data;
        })
        .then(states2 => {
            this.setState({ states2 });
        }).catch(e => {
            console.log("Error getting states");
        });
}

  handleInput = (e) =>{
    var state = Object.assign({}, this.state.newBill); 
    state[e.target.name] = e.target.value;
    this.setState({newBill: state });
  }

  useFiscalData = (e) =>{
    if(e.target.checked) {
        this.setState({fiscalData: true });
    } else {
        this.setState({fiscalData: false });
    }
  }

  sendDataToServer = (e) =>{
    BillAPI.newBill(this.state.newBill)
        .then(r => {
          window.location = '/inicio/usuario'
        }).catch(e => {
              alert("No se pudo guardar, revise sus datos ingresados e intentelo de nuevo");
        });
  }

  render() {
    return (
      <Part3Container>
        <Part3Title>
          <h2>Productor</h2>
          <p>
            Deseamos conocerlo un poco más para ofrecerle
            las mejores opciones.
          </p>
        </Part3Title>
        <Status>
          <p>1</p>
          <p>2</p>
          <p>3</p>
        </Status>
        <Part3Form>
          <Part3FormManual>
            <BasicData>
              <div>
                <p>Razón social o nombre:</p>
                <input onChange={this.handleInput} type="text" name='business_name' />
              </div>
              <div>
                <p>RFC:</p>
                <input onChange={this.handleInput} type="text" name='rfc'/>
              </div>
            </BasicData>
            <FiscalAddress>
              <h3>Domicilio fiscal:</h3>
              <div>
                <div>
                  <p>País:</p>
                  <select onChange={this.getStatesByCountry} name="fiscal_domicile_country" >
                      <option value=''>Seleccione un país</option>
                      {this.renderCountries()}
                  </select>
                </div>
                <div>
                  <p>Estado:</p>
                  <select onChange={this.handleInput} name="fiscal_domicile_state" >
                      <option value=''>Seleccione un estado</option>
                      {this.renderStates()}
                  </select>
                </div>
                <div>
                  <p>Municipio:</p>
                  <input onChange={this.handleInput} type="text" name='fiscal_domicile_municipality' />
                </div>
                <div>
                  <p>Código postal:</p>
                  <input onChange={this.handleInput} type="number" name='fiscal_domicile_postal_code'/>
                </div>
                <div>
                  <p>Colonia:</p>
                  <input onChange={this.handleInput} type="text" name='fiscal_domicile_cologne' />
                </div>
                <div>
                  <p>Calle:</p>
                  <input onChange={this.handleInput} type="text" name='fiscal_domicile_street' />
                </div>
                <div>
                  <p>Número Exterior:</p>
                  <input onChange={this.handleInput} type="text" name='fiscal_domicile_outdoor_number' />
                </div>
                <div>
                  <p>Localidad (opcional):</p>
                  <input onChange={this.handleInput} type="text" name='fiscal_domicile_location'/>
                </div>
                <div>
                  <p>Referencia de ubicación (opcional):</p>
                  <input onChange={this.handleInput} type="text" name='fiscal_domicile_location_reference' />
                </div>
              </div>
            </FiscalAddress>
            <CommercialAddress>
              <h3>Domicilio comercial: (opcional)</h3>
              <div>
                <input onChange={this.useFiscalData} type="checkbox" name="sameAsFiscalAddress" />
                <p>Usar datos fiscales</p>
              </div>
              <div>
                <div>
                  <p>País:</p>
                  {
                    this.fiscalData ?
                    <input type="text" name='business_address_country' value={user.fiscal_domicile_country} readonly/> 
                    : 
                    <select onChange={this.getStatesByCountry2} name="business_address_country" >
                        <option value=''>Seleccione un país</option>
                        {this.renderCountries()}
                    </select>
                  }
                </div>
                <div>
                  <p>Estado:</p>
                  {
                    this.fiscalData ?
                    <input type="text" name='business_address_state' value={user.fiscal_domicile_state} /> 
                    : 
                    <select onChange={this.handleInput} name="business_address_state" >
                        <option value=''>Seleccione un estado</option>
                        {this.renderStates2()}
                    </select>
                  }
                </div>
                <div>
                  <p>Municipio:</p>
                  <input onChange={this.handleInput} type="text" name='business_address_municipality' placeholder={this.fiscalData ? user.fiscal_domicile_municipality  : ''} />
                </div>
                <div>
                  <p>Código postal:</p>
                  <input onChange={this.handleInput} type="number" name='business_address_postal_code' placeholder={this.fiscalData ? user.fiscal_domicile_postal_code  : ''}/>
                </div>
                <div>
                  <p>Colonia:</p>
                  <input onChange={this.handleInput} type="text" name='business_address_cologne' placeholder={this.fiscalData ? user.fiscal_domicile_cologne  : ''} />
                </div>
                <div>
                  <p>Calle:</p>
                  <input onChange={this.handleInput} type="text" name='business_address_street' placeholder={this.fiscalData ? user.fiscal_domicile_street  : ''}/>
                </div>
                <div>
                  <p>Número Exterior:</p>
                  <input onChange={this.handleInput} type="text" name='business_address_outdoor_number' placeholder={this.fiscalData ? user.fiscal_domicile_outdoor_number  : ''}/>
                </div>
                <div>
                  <p>Localidad (opcional):</p>
                  <input onChange={this.handleInput} type="text" name='business_address_location' placeholder={this.fiscalData ? user.fiscal_domicile_location  : ''}/>
                </div>
                <div>
                  <p>Referencia de ubicación (opcional):</p>
                  <input onChange={this.handleInput} type="text" name='business_address_location_reference' placeholder={this.fiscalData ? user.fiscal_domicile_location_reference  : ''}/>
                </div>
              </div>
            </CommercialAddress>
            <button onClick={this.sendDataToServer}>Finalizar</button>
          </Part3FormManual>
        </Part3Form>
      </Part3Container>
    );
  }
}
 
export default Bill;