import styled from 'styled-components'

import { 
  blueDarkTractilus, 
  greenLightTractilus,
  greenDarkTractilus,
  greyDarkTractilus, 
  greyTractilus, 
  orangeTractilus, 
  whiteTractilus } from '../../../../../styles/colorStyles'

export const Part3Container = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
`


export const Part3Title = styled.div `
  
  > h2 {
    color: ${greenLightTractilus};
    text-align: center;
    font-size: 2.5vw;
    font-family: 'Montserrat';
    font-weight: 600;
    margin-bottom: 1%;
  }

  > p {
    font-size: 1.2vw;
    font-family: 'Montserrat';
    color: ${blueDarkTractilus};
    text-align: center;
    margin-bottom: 2.5%;
  }

  @media (max-width: 600px) {

    > h2 {
      font-size: 9vw;
      margin-bottom: 2.5%;
    }

    > p {
      font-size: 4vw;
      width: 80%;
      margin: auto;
      margin-bottom: 5%;
    }
  }

`

export const Status = styled.div `
  display: flex;
  justify-content: space-between;
  width: 25%;
  margin: 2.5% auto;

  > p {
    color: ${greyTractilus};
    font-size: 2vw;
    font-family: 'Montserrat';
    border: 1px solid ${greyTractilus};
    border-radius: 50%;
    padding: 2.5% 5.5%;
  }

  > p:nth-child(1) {
    padding: 2.5% 6.5%;
  }

  > p:nth-child(3) {
    color: ${whiteTractilus};
    border: 1px solid ${greenDarkTractilus};
    background-color: ${greenDarkTractilus};
    padding: 2.5% 5.5%;
  }


  @media (max-width: 600px) {
    width: 50%;
    margin: 5% auto;

    > p {
      font-size: 6vw;
    }

    > p:nth-child(1) {
    }
  }
`


export const Part3Form = styled.div `


`


export const Part3FormManual = styled.div `
  display: flex;
  flex-direction: column;
  width: 35%;
  margin: auto;

  > button {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1vw;
    color: ${whiteTractilus};
    background-color: ${orangeTractilus};
    border: none;
    width: 35%;
    margin: 5% auto auto auto;
    padding: 2% 6%;
    border-radius: 20px;
    cursor: pointer;
    transition: all 0.25s linear;

    &:hover {
      background-color: ${greenLightTractilus};
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }

  @media (max-width: 600px) {
    width: 85%;

    > button {
      font-size: 5vw;
      width: 100%;
      margin: 10% auto;
      padding: 4% 6%;
      border-radius: 25px;
    }
  }
`

export const BasicData = styled.div `

  > div {

    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${greenDarkTractilus};
      margin-bottom: 1%;
    }
  
    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greenDarkTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greenDarkTractilus};
      padding-left: 2%;
      outline: none;
    }

    /*******ERROR RFC ********/
    > input {
      border: ${props => props.errorRFC ? '1px solid red' : `1px solid ${greyDarkTractilus}`}
    } 
    /*******ERROR RFC ********/
  }

  @media (max-width: 600px) {
    
    > div {
  
      > p {
        font-size: 5vw;
      }
      
      > input {
        font-size: 5vw;
      } 

      /*******ERROR RFC ********/
      > input {
      } 
      /*******ERROR RFC ********/
    }
  }
`


export const FiscalAddress = styled.div `

  > h3 {
    font-family: 'Montserrat';
    font-weight: bold;
    font-size: 1.5vw;
    color: ${greenDarkTractilus};
    margin-top: 6%;
    margin-bottom: 2.5%;
  }

  > div {

    > div {
  
      > p {
        font-family: 'Montserrat';
        font-size: 1.2vw;
        color: ${greenDarkTractilus};
        margin-bottom: 1%;
      }
  
      > input {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${greenDarkTractilus};
        margin-bottom: 2.5%;
        width: 100%;
        height: 45px;
        border-radius: 10px;
        border: 1px solid ${greenDarkTractilus};
        padding-left: 2%;
        outline: none;
      }

      > select {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${greenDarkTractilus};
        margin-bottom: 2.5%;
        width: 100%;
        height: 45px;
        border-radius: 10px;
        border: 1px solid ${greenDarkTractilus};
        padding-left: 2%;

        > option {
          font-size: 1.2vw;
          font-family: 'Montserrat';
          color: ${greenDarkTractilus};
        }
      }
    }
  }

  @media (max-width: 600px) {
    > h3 {
      font-size: 5vw;
    }

    > div {
      > div {
  
        > p {
          font-size: 5vw;
        }
  
        > input {
          font-size: 5vw;
        }
  
        > select {
          font-size: 5vw;
          margin-bottom: 5%;
          width: 100%;
  
          > option {
            font-size: 5vw;
          }
        }
      } 
    }

  }
`


export const CommercialAddress = styled.div `
  
  > h3 {
    font-family: 'Montserrat';
    font-weight: bold;
    font-size: 1.5vw;
    color: ${greenDarkTractilus};
    margin-top: 6%;
    margin-bottom: 2.5%;
  }

  > div:nth-child(2) {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 80%;
    margin: 5% auto;

    > input {
      font-size: 1.2vw;
      width: 50px;
      height: 40px;
      border: 1px solid ${greenDarkTractilus};
    }

    > p {
      font-family: 'Montserrat';
      font-size: 1.5vw;
      color: ${greenDarkTractilus};
      margin-bottom: 1%;
      margin-left: 5%;
    }
  }

  > div {

    > div {
  
      > p {
        font-family: 'Montserrat';
        font-size: 1.2vw;
        color: ${greenDarkTractilus};
        margin-bottom: 1%;
      }
  
      > input {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${greenDarkTractilus};
        margin-bottom: 2.5%;
        width: 100%;
        height: 45px;
        border-radius: 10px;
        border: 1px solid ${greenDarkTractilus};
        padding-left: 2%;
        outline: none;
      }

      > select {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${greenDarkTractilus};
        margin-bottom: 2.5%;
        width: 100%;
        height: 45px;
        border-radius: 10px;
        border: 1px solid ${greenDarkTractilus};
        padding-left: 2%;

        > option {
          font-size: 1.2vw;
          font-family: 'Montserrat';
          color: ${greenDarkTractilus};
        }
      }
    }
  }

  @media (max-width: 600px) {
    > h3 {
      font-size: 5vw;
    }

    > div {
      > div {
  
        > p {
          font-size: 5vw;
        }
  
        > input {
          font-size: 5vw;
        }
  
        > select {
          font-size: 5vw;
          margin-bottom: 5%;
          width: 100%;
  
          > option {
            font-size: 5vw;
          }
        }
      } 
    }
  }
`