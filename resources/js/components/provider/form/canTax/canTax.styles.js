import styled from 'styled-components'

import { blueDarkTractilus, greenLightTractilus, greyDarkTractilus, greyTractilus, orangeTractilus, whiteTractilus } from '../../../../styles/colorStyles'

export const Part1Container = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
`

export const Part1Title = styled.div `
  
  > h2 {
    color: ${greenLightTractilus};
    text-align: center;
    font-size: 2.5vw;
    font-family: 'Montserrat';
    font-weight: 600;
    margin-bottom: 1%;
  }

  > p {
    font-size: 1.2vw;
    font-family: 'Montserrat';
    color: ${blueDarkTractilus};
    text-align: center;
    margin-bottom: 2.5%;
  }

  @media (max-width: 600px) {

    > h2 {
      font-size: 9vw;
      margin-bottom: 2.5%;
    }

    > p {
      font-size: 4vw;
      width: 80%;
      margin: auto;
      margin-bottom: 5%;
    }
  }

`

export const Status = styled.div `
  display: flex;
  justify-content: space-between;
  width: 25%;
  margin: 2.5% auto;

  > p {
    color: ${greyTractilus};
    font-size: 2vw;
    font-family: 'Montserrat';
    border: 1px solid ${greyTractilus};
    border-radius: 50%;
    padding: 2.5% 5.5%;
  }

  > p:nth-child(1) {
    color: ${whiteTractilus};
    border: 1px solid ${blueDarkTractilus};
    background-color: ${blueDarkTractilus};
    padding: 2.5% 6.5%;
  }

  @media (max-width: 600px) {
    width: 50%;
    margin: 5% auto;

    > p {
      font-size: 6vw;
    }

    > p:nth-child(1) {
    }
  }

`


export const Part1Form = styled.div `


`


export const Part1FormManual = styled.div `
  width: 35%;
  height: 50vh;
  margin: auto;

  > h2 {
    font-family: 'Montserrat';
    font-size: 1.5vw;
    text-align: center;
  }

  > div {
    display: flex;

      > a {
      text-decoration: none;
      text-align: center;
      font-family: 'Montserrat';
      font-weight: 600;
      font-size: 1vw;
      color: ${whiteTractilus};
      background-color: ${orangeTractilus};
      border: none;
      width: 40%;
      height: 40px;
      margin: 5% auto auto auto;
      padding: 2.2%;
      border-radius: 20px;
      cursor: pointer;
      transition: all 0.25s linear;

      &:hover {
       background-color: ${greenLightTractilus};
       transform: translateY(-3px);
       box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
      }
    }
  }

  @media (max-width: 600px) {
    flex-direction: column;
    width: 85%;
    height: auto;

    > h2 {
      font-size: 5vw;
    }

    > div {
     
      > a {
        font-size: 5vw;
        width: 100%;
        margin: 5% auto;
        border-radius: 25px;
      }
    }
  }
`

