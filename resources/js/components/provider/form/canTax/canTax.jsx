import React from 'react'

import {
  Part1Container,
  Part1Title,
  Status,
  Part1Form,
  Part1FormManual,
} from './canTax.styles'


export default function Part1 (props) {


  return (
    <Part1Container>
      <Part1Title>
        <h2>Proveedor</h2>
        <p>
          Deseamos conocerlo un poco más para ofrecerle
          las mejores opciones.
        </p>
      </Part1Title>
      {/* <Status>
        <p>1</p>
        <p>2</p>
        <p>3</p>
      </Status> */}
      <Part1Form>
        <Part1FormManual>
          <h2>¿Puede emitir factura?</h2>
          <div>
            <a href="/proveedor/formulario/con-factura/parte-1">Si</a>
            <a href="/inicio/usuario">No</a>
          </div>
        </Part1FormManual>
      </Part1Form>
    </Part1Container>
  )
}