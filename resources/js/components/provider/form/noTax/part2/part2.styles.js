import styled from 'styled-components'

import { whiteTractilus, greenDarkTractilus } from '../../../../../styles/colorStyles'

export const Part2Container = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
`


export const Part2Title = styled.div `
  
  > h2 {
    color: ${greenDarkTractilus};
    text-align: center;
    font-size: 2.5vw;
    font-family: 'Montserrat';
    font-weight: 600;
    margin-bottom: 1%;
  }

  > p {
    font-size: 1.2vw;
    font-family: 'Montserrat';
    color: ${greenDarkTractilus};
    text-align: center;
    margin-bottom: 2.5%;
  }

  @media (max-width: 600px) {

    > h2 {
      font-size: 9vw;
      margin-bottom: 2.5%;
    }

    > p {
      font-size: 4vw;
      width: 80%;
      margin: auto;
      margin-bottom: 5%;
    }
  }

`

export const Status = styled.div `
  display: flex;
  justify-content: space-between;
  width: 25%;
  margin: 2.5% auto;

  > p {
    color: ${greenDarkTractilus};
    font-size: 2vw;
    font-family: 'Montserrat';
    border: 1px solid ${greenDarkTractilus};
    border-radius: 50%;
    padding: 2.5% 5.5%;
  }

  > p:nth-child(1) {
    padding: 2.5% 6.5%;
  }

  > p:nth-child(2) {
    color: ${whiteTractilus};
    border: 1px solid ${greenDarkTractilus};
    background-color: ${greenDarkTractilus};
    padding: 2.5% 5.5%;
  }

  @media (max-width: 600px) {
    width: 50%;
    margin: 5% auto;

    > p {
      font-size: 6vw;
    }

    > p:nth-child(1) {
    }
  }

`


export const Part2Form = styled.div `


`


export const Part2FormManual = styled.div `
  display: flex;
  flex-direction: column;
  width: 35%;
  margin: auto;

  > div {
    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${greenDarkTractilus};
      margin-bottom: 1%;
    }

    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greenDarkTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greenDarkTractilus};
      padding-left: 2%;
      outline: none;
    }

    > select {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greenDarkTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greenDarkTractilus};
      padding-left: 2%;

      > option {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${greenDarkTractilus};
      }
    }
  }

  > button {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1vw;
    color: ${whiteTractilus};
    background-color: ${greenDarkTractilus};
    border: none;
    width: 35%;
    margin: 5% auto auto auto;
    padding: 2% 6%;
    border-radius: 20px;
    cursor: pointer;
  }

  @media (max-width: 600px) {
    width: 85%;

    > div {
      > p {
        font-size: 4.8vw;
        margin-bottom: 2.5%;
      }

      > input {
        font-size: 5vw;
        width: 100%;
        margin-bottom: 5%;
      }

      > select {
        font-size: 5vw;
        width: 100%;
        margin-bottom: 5%;

        > option {
          font-size: 5vw;
        }
      }
    }

    > button {
      font-size: 5vw;
      width: 100%;
      margin: 10% auto 10% auto;
      padding: 4% 6%;
      border-radius: 25px;
    }
  }
`

