import React, { Component, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import CountryAPI from '../../../../api/countryRepository';
import StateAPI from '../../../../api/stateRepository';
import CityAPI from '../../../../api/cityRepository';
import LocationAPI from '../../../../api/locationRepository';

import {
  Part2Container,
  Part2Title,
  Status,
  Part2Form,
  Part2FormManual,
} from './part2.styles'
 
class Register3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countries: [],
            states: [],
            cities: [],
            newLocation: {
                user_id: localStorage.getItem("id"),
                country_id: 0,
                state_id: 0,
                city_id: 0,
                suburb: '',
                postal_Code: 0,
                street: '',
                number: 0,
                reference: ''
            }
        }

        this.sendDataToServer = this.sendDataToServer.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    componentDidMount() {
        CountryAPI.getCountry()
            .then(response => {
                return response.data;
            })
            .then(countries => {
                this.setState({ countries });
            }).catch(e => {
                console.log("Error getting countries");
            });
    }

    renderCountries () {
        return this.state.countries.map(country => {
            return (
                <option key={country.id.toString()} value={country.id}>
                    { country.country } 
                </option>    
            );
        })
    }

    renderStates () {
        return this.state.states.map(state => {
            return (
                <option key={state.id.toString()} value={state.id}>
                    { state.state } 
                </option>    
            );
        })
    }

    renderCities () {
        return this.state.cities.map(city => {
            return (
                <option key={city.id.toString()} value={city.id}>
                    { city.city } 
                </option>    
            );
        })
    }

    getStatesByCountry = (e) =>{
        let country =  e.target.value;
        var location = Object.assign({}, this.state.newLocation); 
        location[e.target.name] = e.target.value;
        this.setState({newLocation: location });

        StateAPI.getStateByCountryId(country)
            .then(response => {
                return response.data;
            })
            .then(states => {
                this.setState({ states });
            }).catch(e => {
                console.log("Error getting states");
            });
    }

    getCitiesByState = (e) =>{
        let state =  e.target.value;
        var location = Object.assign({}, this.state.newLocation); 
        location[e.target.name] = e.target.value;
        this.setState({newLocation: location });
        CityAPI.getCityByStateId(state)
            .then(response => {
                return response.data;
            })
            .then(cities => {
                this.setState({ cities });
            }).catch(e => {
                console.log("Error getting cities");
            });
    }

    handleInput = (e) =>{
      var state = Object.assign({}, this.state.newLocation); 
      state[e.target.name] = e.target.value;
      this.setState({newLocation: state });
    }

    sendDataToServer = (e) =>{
        LocationAPI.newLocation(this.state.newLocation)
            .then(r => {
                  window.location = '/proveedor/formulario/tipo-de-proveedor'
            }).catch(e => {
                  alert("No se pudo guardar, revise sus datos ingresados e intentelo de nuevo");
            });
    }

    render() {
        
        return (
            <Part2Container>
                <Part2Title>
                  <h2>Proveedor</h2>
                  <p>
                    Deseamos conocerlo un poco más para ofrecerle
                    las mejores opciones.
                  </p>
                </Part2Title>
                <Status>
                  <p>1</p>
                  <p>2</p>
                  <p>3</p>
                </Status>
                <Part2Form>
                    <Part2FormManual>
                      <div>
                        <p>País:</p>
                        <select onChange={this.getStatesByCountry} name="country_id" >
                            <option value=''>Seleccione un país</option>
                            {this.renderCountries()}
                        </select>
                      </div>
                      <div>
                        <p>Estado:</p>
                        <select onChange={this.getCitiesByState} name="state_id" >
                          <option value=''>Seleccione un estado</option>
                          {this.renderStates()}
                        </select>
                      </div>
                      <div>
                        <p>Municipio:</p>
                        <select name="city_id" onChange={this.handleInput}>
                          <option value=''>Seleccione un municipio</option>
                          {this.renderCities()}
                        </select>
                      </div>
                      <div>
                        <p>Colonia:</p>
                        <input type="text" name='suburb' onChange={this.handleInput}/>
                      </div>
                      <div>
                        <p>Código postal:</p>
                        <input type="number" name='postal_Code' onChange={this.handleInput}/>
                      </div>
                      <div>
                        <p>Calle:</p>
                        <input type="text" name='street' onChange={this.handleInput}/>
                      </div>
                      <div>
                        <p>Número Exterior:</p>
                        <input type="number" name='number' onChange={this.handleInput}/>
                      </div>
                      <div>
                        <p>Referencia de ubicación (opcional):</p>
                        <input type="text" name='reference' onChange={this.handleInput}/>
                      </div>
                      <button onClick={this.sendDataToServer}>Continuar</button>
                    </Part2FormManual>
                </Part2Form>
            </Part2Container>
        );
    }
}
 
export default Register3;