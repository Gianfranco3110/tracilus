import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import AuthApi from '../../../../api/authRepository';

import {
  Part1Container,
  Part1Title,
  Status,
  Part1Form,
  Part1FormManual,
} from './part1.styles'


export default function Part1 (props) {
  const [ userData, setUserData ] = useState({})

  let navigate = useNavigate()

  const handleData = e => userData[e.target.name] = e.target.value


  const sendDataToServer = () => {
    if(Object.keys(userData).length === 5) {
      userData['profile_id'] = 4;
      userData['id'] = localStorage.getItem("id");
      AuthApi.signUpSecond(userData)
          .then(r => {
            window.location = '/proveedor/formulario/sin-factura/parte-2'
          }).catch(e => {
              alert("No se pudo registrar, revise sus datos ingresados e intentelo de nuevo");
          });
    } else {
      console.log('SHOW FORM PRODUCER LACK OF DATA')
    }

  }

  return (
    <Part1Container>
      <Part1Title>
        <h2>Proveedor</h2>
        <p>
          Deseamos conocerlo un poco más para ofrecerle
          las mejores opciones.
        </p>
      </Part1Title>
      <Status>
        <p>1</p>
        <p>2</p>
        <p>3</p>
      </Status>
      <Part1Form>
        <Part1FormManual>
          <div>
            <p>Nombre(s):</p>
            <input onChange={handleData} type="text" name='name' />
          </div>
          <div>
            <p>Apellido Paterno:</p>
            <input onChange={handleData}t type="text" name='last_name' />
          </div>
          <div>
            <p>Apellido Materno:</p>
            <input onChange={handleData} type="text" name='second_last_name' />
          </div>
          <div>
            <p>Género:</p>
            <div>
              <div>
                <input onChange={handleData} type="radio" name='gender_id' value='2'/>
                <span>Masculino</span>
              </div>
              <div>
                <input onChange={handleData} type="radio" name='gender_id' value='1'/>
                <span>Femenino</span>
              </div>
            </div>
          </div>
          <div>
            <p>Fecha de nacimiento:</p>
            <input onChange={handleData} type="date" name='birthdate' />
          </div>
          <button onClick={sendDataToServer}>Continuar</button>
        </Part1FormManual>
      </Part1Form>
    </Part1Container>
  )
}