import styled from 'styled-components'

import {
  greenDarkTractilus,
  whiteTractilus 
} from '../../../../../styles/colorStyles'

export const Part1Container = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
`


export const Part1Title = styled.div `
  
  > h2 {
    color: ${greenDarkTractilus};
    text-align: center;
    font-size: 2.5vw;
    font-family: 'Montserrat';
    font-weight: 600;
    margin-bottom: 1%;
  }

  > p {
    font-size: 1.2vw;
    font-family: 'Montserrat';
    color: ${greenDarkTractilus};
    text-align: center;
    margin-bottom: 2.5%;
  }

  @media (max-width: 600px) {

    > h2 {
      font-size: 9vw;
      margin-bottom: 2.5%;
    }

    > p {
      font-size: 4vw;
      width: 80%;
      margin: auto;
      margin-bottom: 5%;
    }
  }

`

export const Status = styled.div `
  display: flex;
  justify-content: space-between;
  width: 25%;
  margin: 2.5% auto;

  > p {
    color: ${greenDarkTractilus};
    font-size: 2vw;
    font-family: 'Montserrat';
    border: 1px solid ${greenDarkTractilus};
    border-radius: 50%;
    padding: 2.5% 5.5%;
  }

  > p:nth-child(1) {
    color: ${whiteTractilus};
    border: 1px solid ${greenDarkTractilus};
    background-color: ${greenDarkTractilus};
    padding: 2.5% 6.5%;
  }

  @media (max-width: 600px) {
    width: 50%;
    margin: 5% auto;

    > p {
      font-size: 6vw;
    }

    > p:nth-child(1) {
    }
  }

`


export const Part1Form = styled.div `


`


export const Part1FormManual = styled.div `
  display: flex;
  flex-direction: column;
  width: 35%;
  margin: auto;

  > div {
    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${greenDarkTractilus};
      margin-bottom: 1%;
    }

    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greenDarkTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greenDarkTractilus};
      padding-left: 2%;
      outline: none;
    }
  }

  /**********GENDER ************/

    > div:nth-child(4) {
      margin-top: 2.5%;
      margin-bottom: 5%;

      > div {
        display: flex;
  
        > div {
          width: 50%;
          text-align: center;
  
          > input {
            margin-right: 5%;
          }
  
          > span {
            font-size: 1.2vw;
            font-family: 'Montserrat';
            font-weight: 600;
            color: ${greenDarkTractilus};
          }
        }
      }
    }
    
  /**********GENDER ************/

  > button {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1vw;
    color: ${whiteTractilus};
    background-color: ${greenDarkTractilus};
    border: none;
    width: 35%;
    margin: 5% auto auto auto;
    padding: 2% 6%;
    border-radius: 20px;
    cursor: pointer;
  }

  @media (max-width: 600px) {
    width: 85%;

    > div {
      > p {
        font-size: 5vw;
        margin-bottom: 2.5%;
      }

      > input {
        font-size: 5vw;
        margin-bottom: 5%;
      }
    }

    /**********GENDER ************/

      > div:nth-child(4) {
        margin-top: 2.5%;
        margin-bottom: 5%;

        > div {
    
          > div {
            width: 50%;
    
            > input {
            }
    
            > span {
              font-size: 5vw;
            }
          }
        }
      }
    /**********GENDER ************/

    > button {
      font-size: 5vw;
      width: 100%;
      margin: 10% auto 10% auto;
      padding: 4% 6%;
    }
  }
`

