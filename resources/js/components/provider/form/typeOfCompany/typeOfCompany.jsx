import React from 'react'

import {
  Part1Container,
  Part1Title,
  Status,
  Part1Form,
  Part1FormManual,
} from './typeOfCompany.styles'


export default function Part1 (props) {


  return (
    <Part1Container>
      <Part1Title>
        <h2>Proveedor</h2>
        <p>
          Deseamos conocerlo un poco más para ofrecerle
          las mejores opciones.
        </p>
      </Part1Title>
      <Status>
        <p>1</p>
        <p>2</p>
        <p>3</p>
      </Status>
      <Part1Form>
        <Part1FormManual>
          <a href='/proveedor/formulario/puede-facturar'>Soy independiente</a>
          <a href='/proveedor/formulario/puede-facturar'>Soy empresa</a>
        </Part1FormManual>
      </Part1Form>
    </Part1Container>
  )
}