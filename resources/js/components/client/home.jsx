import React from 'react'
import AuthApi from '../api/authRepository';

import {
  HomeContainer,
  UserNav,
  ChooseUser,
  ChooseUserTitle,
  ChooseUserCards,
  UserCard
} from './home.styles'


export default function Home () {

    const logout = () => {
        AuthApi.logOut()
            .then(r => {
                window.location = '/'
            }).catch(e => {
                alert("No se pudo conectar con el servidor");
            });
    }

    return (
      <HomeContainer>
        <UserNav>
          <a href="/">Inicio</a>
          <button onClick={logout}>Cerrar Sesion</button>
          <div>
            <div></div>
            <div></div>
            <div></div>
          </div>
          <div>
            <div></div>
          </div>
        </UserNav>
        <ChooseUser>
          <ChooseUserTitle>
            <h2>Bienvenido a TRACTILUS</h2>
          </ChooseUserTitle>
          <ChooseUserCards>
            
          </ChooseUserCards>
        </ChooseUser>
      </HomeContainer>
    )
}