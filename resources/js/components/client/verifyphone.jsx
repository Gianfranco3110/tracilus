import React from 'react'
import AuthApi from '../api/authRepository';

import {
  HomeContainer,
  UserNav,
  ChooseUser,
  ChooseUserTitle,
  ChooseUserCards,
  UserCard
} from './home.styles'


export default function Home () {

    const logout = () => {
        AuthApi.logOut()
            .then(r => {
                window.location = '/'
            }).catch(e => {
                alert("No se pudo conectar con el servidor");
            });
    }

    return (
      <HomeContainer>
        <UserNav>
          <button onClick={logout}>Cerrar Sesion</button>
          <div>
            <div></div>
            <div></div>
            <div></div>
          </div>
          <div>
            <div></div>
          </div>
        </UserNav>
        <ChooseUser>
          <ChooseUserTitle>
            <h2>Verifica tu telefono</h2>
          </ChooseUserTitle>
          <ChooseUserCards>
            <p>Gracias por registrarte en nuestra plataforma. Antes de continuar, verifique su teléfono.</p>
          </ChooseUserCards>
        </ChooseUser>
      </HomeContainer>
    )
}