import React, { useState } from 'react'
import AuthApi from '../api/authRepository';

import {
  SignUpContainer,
  SignUpTitle,
  SignUpForm,
  SignUpFormManual,
  Forgot,
  SocialNetworks
} from './signUpUser.styles'

import ModalConfirmationCode from '../modals/confirmationCode/confirmationCode'
import ModalErrorSignUpData from '../modals/errorSignUpData/errorSignUpData'

import lada from '../../constants/lada'

export default function SignUpUser () {
  const [ user, setUser ]  = useState({})
  const [ errorEmail, setErrorEmail] = useState(false)
  const [ showPassword, setShowPassword ] = useState(false)
  const [ showConfirmPassword, setShowConfirmPassword ] = useState(false)
  const [ showError, setShowError ] = useState(false)
  const [ show, setShow ] = useState(false)

  const tooglePassword = () => setShowPassword(!showPassword)
  const toogleConfirmPassword = () => setShowConfirmPassword(!showConfirmPassword)

  const handleEmail = e => {
    //let re = /^(?:\d{10}|\w+@\w+\.\w{2,3})$/

    //if(re.test(e.target.value)) {
      user[e.target.name] = e.target.value
      //setErrorEmail(false)
    //} else {
      //setErrorEmail(true)
    //}
  }

  const handleLada = e => {
    user[e.target.name] = e.target.value
  }

  const handlePassword = e => user[e.target.name] = e.target.value
  const handleMobile = e => user[e.target.name] = e.target.value

  const hideModal = () => {
    setShow(false);
  };

  const hideModalError = () => {
    setShowError(false);
  };

  const sendDataToServer = () => {
    let ruta = window.location.pathname;
    let rutaArray = ruta.split('/');
    user['type'] = rutaArray[2]
    localStorage.setItem('user_type', rutaArray[2]);
    if(user.email && user.phone && (user.password === user.c_password)) {
      user.phone = '+'+user.lada+user.phone;
      AuthApi.signUp(user)
          .then(r => {
            setShow(true);
          }).catch(e => {
              console.log(e);
              setShowError(true);
          });
    } else {
      setShowError(true);
    }
  }

  const sendDataToGoogle = () => {
    window.location = '/auth/google'
  }

  const sendDataToFacebook = () => {
    window.location = '/auth/facebook'
  }

  return (
    <SignUpContainer>
      <ModalErrorSignUpData handleClose={hideModalError} show={showError}></ModalErrorSignUpData>
        <ModalConfirmationCode handleClose={hideModal} show={show}></ModalConfirmationCode>
      <SignUpTitle>
        <h2>Registrate</h2>
        <p>
          Por favor, introduzca su correo 
          electrónico y contraseña.
        </p>
      </SignUpTitle>
      <SignUpForm>
        <SignUpFormManual errorEmail={errorEmail}>
          <div>
            <p>Correo electrónico:</p>
            <input onChange={handleEmail} type="text" name='email' />
          </div>
          <div>
            <p>Télefono:</p>
            <div>
              <select onChange={handleLada} name="lada">
                  {lada.map( (data,key) => 
                    <option key={key} value={data.lada}>
                      {data.emsp}
                    </option>
                  )}
              </select>
              <input onChange={handleMobile} type="text" name='phone' />
            </div>
          </div>
          <div>
            <p>Contraseña:</p>        
            <div>
              <input onChange={handlePassword} type={showPassword ? 'text' : 'password' } name='password' />
              <figure onMouseDown={tooglePassword} onMouseUp={tooglePassword}>
                <img src="https://tractilus.s3.amazonaws.com/IconFeatherEyeblBlack.svg" alt="Ojito de contraseña" />
              </figure>
            </div>
          </div>
          <div>
            <p>Confirmar contraseña:</p>
            <div>
              <input onChange={handlePassword} type={showConfirmPassword ? 'text' : 'password' } name='c_password' />
              <figure onMouseDown={toogleConfirmPassword} onMouseUp={toogleConfirmPassword}>
                <img src="https://tractilus.s3.amazonaws.com/IconFeatherEyeblBlack.svg" alt="Ojito de contraseña" />
              </figure>
            </div>
          </div>  
          <button onClick={sendDataToServer}>Registrarse</button>
        </SignUpFormManual>
        <Forgot>
          <a href="/olvide-mi-contraseña">Olvide mi contraseña</a>
          <p>Tengo cuenta da <a href="/iniciar-sesion">clic aquí</a></p>
        </Forgot>
        <SocialNetworks>
          <div>
            <div></div>
            <p>O continue con su cuenta mediante</p>
            <div></div>
          </div>
          <button onClick={sendDataToGoogle}>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/google.png" alt="logo Gmail" />
            </figure>
            Iniciar sesión con Google
          </button>
          <button onClick={sendDataToFacebook}>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/facebook.png" alt="logo Facebook" />
            </figure>
            Iniciar sesión con Facebook
          </button>
        </SocialNetworks>
      </SignUpForm>
    </SignUpContainer>
  )
}