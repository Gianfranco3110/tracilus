import React from 'react'

import {
    FooterContainer,
    FooterLogo,
    Advices,
    SocialNetwork
} from './footer.styles'

export default function Footer () {

    return (
        <FooterContainer>
            <FooterLogo>
                <img src="https://tractilus.s3.amazonaws.com/logoFooter.svg" alt="Logo Tractilus" />
            </FooterLogo>
            <Advices>
                <a href="/aviso-de-privacidad">Aviso de privacidad</a>
                <a href="/terminos-y-condiciones">Términos y condiciones</a>
            </Advices>
            <SocialNetwork>
                <p>Redes sociales:</p>
                <div>
                    <figure>
                        <img src="https://tractilus.s3.amazonaws.com/Icon+awesome-facebook.svg" alt="icono facebook" />
                    </figure>
                    <figure>
                        <img src="https://tractilus.s3.amazonaws.com/Icon+simple-instagram.svg" alt="icono instagram" />
                    </figure>
                    <figure>
                        <img src="https://tractilus.s3.amazonaws.com/Icon+awesome-linkedin.svg" alt="icono linkedIn" />
                    </figure>
                </div>
            </SocialNetwork>
        </FooterContainer>    
    )
}