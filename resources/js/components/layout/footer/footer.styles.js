import styled from 'styled-components'

import {
    blueDarkTractilus, whiteTractilus
} from '../../../styles/colorStyles'

export const FooterContainer = styled.footer `
  display: flex;
  align-items: center;
  padding-top: 5%;
  padding-bottom: 5%;
  background-color: ${blueDarkTractilus};
 
  @media (max-width: 600px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-top: 20%;
    padding-bottom: 20%;
  }
`

export const FooterLogo = styled.footer `
  height: auto;
  margin-left: 5%;
  width: 30%;

  > img {
    width: 200px;
    height: 100%;
  }

  @media (max-width: 600px) {
    margin-left: -20%;
  }
`

export const Advices = styled.div `
  display: flex;
  flex-direction: column;
  width: 30%;

  > a {
    text-align: center;
    font-family: 'Montserrat';
    text-decoration: none;
    color: ${whiteTractilus};
  }

  @media (max-width: 600px) {
    width: 50%;
    margin: 10% 0 10% 0; 

    > a {
    }
  }
`

export const SocialNetwork = styled.div `
  width: 30%;

  > p {
    padding-left: 50%;
    color: ${whiteTractilus};
    font-size: 1.2vw;
    font-family: 'Montserrat';  
  }

  > div {
    display: flex;
    justify-content: space-between;
    margin-top: 2.5%;
    padding-left: 50%;
    width: 90%;

    > figure {
      width: 25px;
      height: 25px;
      cursor: pointer;
      transition: all 0.25s linear;

      &:hover {
        transform: translateY(-3px);
        box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
      }

      > img {
        width: 100%;
        height: 100%;
      }
    }
  }

  @media (max-width: 600px)  {
    width: 90%;

  > p {
    padding-left: 35%;
    color: ${whiteTractilus};
    font-size: 4vw;
    font-family: 'Montserrat';  
  }

  > div {
    display: flex;
    justify-content: space-between;
    margin-top: 2.5% auto;
    padding-left: 30%;
    width: 75%;
  
    > figure {
      width: 25px;
      height: 25px;
      cursor: pointer;
      transition: all 0.25s linear;

      &:hover {
        transform: translateY(-3px);
        box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
      }

      > img {
        width: 100%;
        height: 100%;
      }
    }
  }
}
`