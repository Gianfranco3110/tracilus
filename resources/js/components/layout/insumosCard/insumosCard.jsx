import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SupplieApi from '../../api/supplieRepository';
import FormEdit from '../../provider/addToCatalog/insumos/form/part2/partEdit2';

import {
  InsumosCard,
  InsumosCardInfo,
  CardPhoto,
  CardName,
  CardSpecifications,
  CardCost,
  Line,
  InsumosCardButtons
} from './insumosCard.styles'

class Supplie extends Component {
  constructor() {
    super();
    //Initialize the state in the constructor
    this.state = {
        products: [],
        mostrarForm: false
    }
  }

  componentDidMount() {
    /* fetch API in action */
    let user_id = localStorage.getItem("id");
    SupplieApi.getSupplieByUserId(user_id)
        .then(response => {
            return response.data;
        })
        .then(products => {
            this.setState({ products });
        }).catch(e => {
            console.error(e);
            console.log("Error getting products");
        });
  }

  deleteProduct = (product, e) => {
    SupplieApi.deleteSupplie(product)
        .then(r => {
              alert("Producto eliminado");
              location.reload();
        }).catch(e => {
              alert("No se pudo eliminar el producto, intentelo de nuevo");
        });
  }
  UpdateProduct = (product, e) => {
    SupplieApi.updateSupplie(product)
        .then(r => {
              alert("Producto actualizado");
              location.reload();
        }).catch(e => {
              alert("No se pudo actualizar el producto, intentelo de nuevo");
        });
  }

  renderProducts() {
    return this.state.products.map(product => {
        return (
          <div key={product.id}>
          {
            (!this.state.mostrarForm) ? (
          <InsumosCard key={product.id}>
            <InsumosCardInfo>
              <CardPhoto>
                <img src={`/uploads/images/supplies/${product.image1}`} alt="foto producto"/>
              </CardPhoto>
              <CardName>
                <h3>Insumo</h3>
                {product.typeSupplie == 'semilla' &&
                  <p>Semilla</p>
                }
                {product.typeSupplie == 'fertilizante' &&
                  <p>Fertilizante</p>
                }
                {product.typeSupplie == 'herbicida' &&
                  <p>Herbicida</p>
                }
                {product.typeSupplie == 'insecticida' &&
                  <p>Insecticida</p>
                }
                {product.typeSupplie == 'fungicida' &&
                  <p>Fungicida</p>
                }
                {product.otherSupplie != 'undefined' &&
                  <p>{product.otherSupplie}</p>
                }
              </CardName>
              <Line></Line>
              <CardSpecifications>
                <div>
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/insumos/InsumosUniversal.png" alt="icono marca"/>
                  </figure>
                  <div>
                    <h3>Nombre:</h3>
                    <p>{product.name}</p>
                  </div>
                </div>
                <div>
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/insumos/costal.png" alt="icono modelo"/>
                  </figure>
                  <div>
                    <h3>Presentación:</h3>
                    {product.typeContainer == 'costal' &&
                      <p>Costal</p>
                    }
                    {product.typeContainer == 'frasco' &&
                      <p>Frasco</p>
                    }
                    {product.typeContainer == 'granel' &&
                      <p>Granel</p>
                    }
                    {product.otherContainer != 'undefined' &&
                      <p>{product.otherContainer}</p>
                    }
                  </div>
                </div>
                <div>
                  <figure>
                    <img src="https://tractilus.s3.amazonaws.com/insumos/semillasIcono.png" alt="icono accionamiento"/>
                  </figure>
                  <div>
                    <h3>Cantidad existente:</h3>
                    <p>{product.inventory}</p>
                  </div>
                </div>
              </CardSpecifications>
              <Line></Line>
              <CardCost>
                <h3>Precio</h3>
                <p>${product.unitPrice}</p>
              </CardCost>
            </InsumosCardInfo>
            <InsumosCardButtons>
              <figure onClick={(e) => this.deleteProduct(product.id, e)}>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoBorrar.png" alt="boton de eliminar"/>
              </figure>
              <div></div>
              <figure onClick={() => this.setState({ mostrarForm: true })}>
                <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoEditar.png" alt="boton de editar"/>
              </figure>
            </InsumosCardButtons>
          </InsumosCard>
          ):null
        }
            {      
              (this.state.mostrarForm) ? (
                <FormEdit
                  producto ={product}
              />
              ):null
            } 
         </div>
        );
    })
  }

  render() {
    return (
        this.renderProducts()
    );
  }
}

export default Supplie; 