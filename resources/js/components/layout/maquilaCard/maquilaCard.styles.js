import styled from "styled-components";

import { maquilaPurple } from "../../../styles/colorStyles";

export const MaquilaCard = styled.div`
    display: flex;
    gap: 5%;
    margin-top: 2.5%;
    margin-bottom: 7.5%;

    @media (max-width: 600px) {
        flex-direction: column;
    }
`;

export const MaquilaCardInfo = styled.div`
    display: flex;
    justify-content: space-around;
    width: 90%;
    padding-top: 5%;
    padding-bottom: 5%;
    border-radius: 20px;
    box-shadow: 0px 5px 10px #00000029;

    @media (max-width: 600px) {
        flex-direction: column;
        width: 100%;
    }
`;

export const CardPhoto = styled.figure`
    width: 20%;
    height: 190px;
    margin-left: 2%;

    > img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    @media (max-width: 600px) {
        width: 90%;
        height: auto;
        margin: auto;

        > img {
        }
    }
`;

export const Line = styled.div`
    background-color: rgba(203, 203, 203, 1);
    width: 1px;
    height: 100%;
    margin-right: 5%;
    margin-left: 5%;

    @media (max-width: 600px) {
        width: 90%;
        height: 1px;
        margin-top: 10%;
        margin-bottom: 10%;
    }
`;

export const CardName = styled.div`
    display: flex;
    flex-direction: column;
    margin: auto;
    width: 20%;
    margin-left: 5%;

    > h3 {
        color: rgba(129, 129, 129, 1);
        font-family: "Montserrat";
        font-weight: 600;
        font-size: 1vw;
    }

    > p {
        color: ${maquilaPurple};
        font-family: "Montserrat";
        font-weight: 600;
        font-size: 1.5vw;
    }

    @media (max-width: 600px) {
        width: 90%;

        > h3 {
            font-size: 5vw;
        }

        > p {
            font-size: 5vw;
        }
    }
`;

export const CardSpecifications = styled.div`
    width: 22%;

    > div {
        display: flex;
        gap: 10%;

        > figure {
            width: 15px;
            height: 19px;

            > img {
                width: 100%;
                height: 100%;
            }
        }

        > div {
            width: 70%;
            margin-bottom: 5%;

            > h3 {
                color: rgba(129, 129, 129, 1);
                font-family: "Montserrat";
                font-weight: 600;
                font-size: 1vw;
            }

            > p {
                color: rgba(129, 129, 129, 1);
                font-family: "Montserrat";
                font-weight: 500;
                font-size: 1vw;
            }
        }
    }

    > div:nth-child(1) {
        > div {
            margin-left: 10%;
        }
    }

    > div:nth-child(2) {
        > figure {
            width: 32px;
            height: 30px;

            > img {
                width: 100%;
                height: 100%;
            }
        }
    }

    > div:nth-child(3) {
        > figure {
            width: 20px;
            height: 30px;

            > img {
                width: 100%;
                height: 100%;
            }
        }

        > div {
            margin-left: 8%;
        }
    }

    > div:nth-child(4) {
        > figure {
            width: 25px;
            height: 25px;

            > img {
                width: 100%;
                height: 100%;
            }
        }

        > div {
            margin-left: 5%;
        }
    }

    > div:nth-child(5) {
        > figure {
            width: 32px;
            height: 30px;

            > img {
                width: 100%;
                height: 100%;
            }
        }
    }

    > div:nth-child(6) {
        > figure {
            width: 25px;
            height: 25px;

            > img {
                width: 100%;
                height: 100%;
            }
        }

        > div {
            margin-left: 5%;
        }
    }

    @media (max-width: 600px) {
        width: 90%;
        margin: auto;

        > div {
            display: flex;
            gap: 10%;
            margin-bottom: 2.5%;

            > figure {
                width: 15px;
                height: 19px;

                > img {
                }
            }

            > div {
                width: 70%;
                margin-bottom: 5%;

                > h3 {
                    font-size: 4vw;
                }

                > p {
                    font-size: 3vw;
                }
            }
        }

        > div:nth-child(1) {
            > div {
                margin-left: 5%;
            }
        }

        > div:nth-child(2) {
            > figure {
                width: 30px;
                height: 30px;

                > img {
                }
            }
        }

        > div:nth-child(3) {
            > figure {
                width: 25px;
                height: 25px;

                > img {
                }
            }

            > div {
                margin-left: 0%;
            }
        }
    }
`;

export const CardCost = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 20%;

    > h3 {
        color: ${maquilaPurple};
        font-family: "Montserrat";
        font-weight: 600;
        font-size: 1.5vw;
    }

    > p {
        color: ${maquilaPurple};
        font-family: "Montserrat";
        font-weight: 600;
        font-size: 1.5vw;
    }

    @media (max-width: 600px) {
        width: 100%;

        > h3 {
            font-size: 6vw;
        }

        > p {
            font-size: 5vw;
        }
    }
`;

export const MaquilaCardButtons = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;

    > figure {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 80px;
        height: 80px;
        border-radius: 8px;
        background-color: ${maquilaPurple};
        box-shadow: 0px 5px 10px #00000029;
        cursor: pointer;

        > img {
            width: 60%;
            height: auto;
        }
    }

    > div {
        width: 90%;
        height: 1px;
        margin-top: 2.5%;
        margin-bottom: 2.5%;
        background-color: rgba(203, 203, 203, 1);
    }

    @media (max-width: 600px) {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-around;
        align-items: center;
        width: 90%;
        margin: 5% auto;

        > figure {
            > img {
            }
        }

        > div {
            width: 2px;
            height: 80px;
        }
    }
`;
