import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MaquilaApi from '../../api/maquilaRepository';

import FormEdit from '../../provider/addToCatalog/maquila/form/formEdit'
import {
  MaquilaCard,
  MaquilaCardInfo,
  CardPhoto,
  CardName,
  CardSpecifications,
  CardCost,
  Line,
  MaquilaCardButtons
} from './maquilaCard.styles'

class Maquila extends Component {
  constructor() {
    super();
    //Initialize the state in the constructor
    this.state = {
        products: [],
        mostrarForm: false
    }
  }

  componentDidMount() {
    /* fetch API in action */
    let user_id = localStorage.getItem("id");
    MaquilaApi.getMaquilaByUserId(user_id)
        .then(response => {
            return response.data;
        })
        .then(products => {
            this.setState({ products });
        }).catch(e => {
            console.error(e);
            console.log("Error getting products");
        });
  }

  deleteProduct = (product, e) => {
    MaquilaApi.deleteMaquila(product)
        .then(r => {
              alert("Producto eliminado");
              location.reload();
        }).catch(e => {
              alert("No se pudo eliminar el producto, intentelo de nuevo");
        });
  }
  updateProduct = (product, e) => {
    console.log('product',product);
    MaquilaApi.updateMaquila(product)
        .then(r => {
              alert("Producto actualizado");
              location.reload();
        }).catch(e => {
              alert("No se pudo actualizar el producto, intentelo de nuevo");
        });
  }

  renderProducts() {
    return this.state.products.map(product => {
        return (
          <div key={product.id}>
          {
            (!this.state.mostrarForm) ? (
              <MaquilaCard>
                <MaquilaCardInfo>
                  <CardPhoto>
                    <img src={`/uploads/images/maquilas/${product.image1}`} alt="foto producto"/>
                  </CardPhoto>
                  <CardName>
                    <h3>Maquila</h3>
                    <p>{product.activity}</p>
                  </CardName>
                  <Line></Line>
                  <CardSpecifications>
                    <div >
                      <figure >
                        <img src="https://tractilus.s3.amazonaws.com/maquila/marca.png" alt="icono marca" />
                      </figure>
                      <div >
                        <h3 >Marca:</h3>
                        <p >{product.brand}</p>
                      </div>
                    </div>
                    <div >
                      <figure >
                        <img src="https://tractilus.s3.amazonaws.com/maquila/tractor.png" alt="icono modelo" />
                      </figure>
                      <div >
                        <h3 >Modelo:</h3>
                        <p >{product.model}</p>
                      </div>
                    </div>
                    <div >
                      <figure >
                        <img src="https://tractilus.s3.amazonaws.com/maquila/operador.png" alt="icono accionamiento" />
                      </figure>
                      <div >
                        <h3 >Nombre del operador:</h3>
                        <p >{product.name}</p>
                      </div>
                    </div>
                    <div >
                      <figure >
                        <img src="https://tractilus.s3.amazonaws.com/maquila/tipoDeAcconamiento.png" alt="icono accionamiento" />
                      </figure>
                      <div >
                        <h3 >Tipo de accionamiento:</h3>
                            {product.type == 'fourWheels' &&
                              <p >Tractor 4 ruedas</p>
                            }
                            {product.type == 'twoWheels' &&
                              <p >Motocultor</p>
                            }
                            {product.type == 'potencia' &&
                              <p >Potencia</p>
                            }
                            {product.type == 'animal' &&
                              <p >Ancho de torque</p>
                            }
                      </div>
                    </div>
                  </CardSpecifications>
                  <Line ></Line>
                  <CardCost >
                    <h3 >Precio</h3>
                    <p >${product.cost}</p>
                  </CardCost>
                </MaquilaCardInfo>
                <MaquilaCardButtons >
                  <figure onClick={(e) => this.deleteProduct(product.id, e)}>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoBorrar.png" alt="boton de eliminar" />
                  </figure>
                  <div ></div>
                  <figure onClick={() => this.setState({ mostrarForm: true })}>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoEditar.png" alt="boton de editar" />
                  </figure>
                </MaquilaCardButtons>
              </MaquilaCard>
          ):null
        }
          {
            (this.state.mostrarForm) ? (
              <FormEdit
                producto ={product}
                />
            ):null
                }
        </div>
        );
    })
  }

  render() {
    return (
        this.renderProducts()
    );
  }

}

export default Maquila;