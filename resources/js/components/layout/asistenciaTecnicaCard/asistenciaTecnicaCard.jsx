import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TechnicalAssistanceApi from '../../api/technicalAssistanceRepository';

import FormEdit from '../../provider/addToCatalog/asistenciaTecnica/form/formEdit'

import {
  AsistenciaTecnicaCard,
  AsistenciaTecnicaCardInfo,
  CardPhoto,
  CardSpecifications,
  CardCost,
  Line,
  AsistenciaTecnicaCardButtons
} from './asistenciaTecnicaCard.styles'

/* Main Component */
class TechnicalAssistance extends Component {
  constructor() {
    super();
    //Initialize the state in the constructor
    this.state = {
        products: [],
        mostrarForm: false
    }
  }

  /*componentDidMount() is a lifecycle method
   * that gets called after the component is rendered
   */
  componentDidMount() {
    /* fetch API in action */
    let user_id = localStorage.getItem("id");
    TechnicalAssistanceApi.getTechnicalAssistanceByUserId(user_id)
        .then(response => {
            return response.data;
        })
        .then(products => {
            this.setState({ products });
        }).catch(e => {
            console.error(e);
            console.log("Error getting products");
        });
  }

  deleteProduct = (product, e) => {
    TechnicalAssistanceApi.deleteTechnicalAssistance(product)
        .then(r => {
              alert("Producto eliminado");
              location.reload();
        }).catch(e => {
              alert("No se pudo eliminar el producto, intentelo de nuevo");
        });
  }
  updateProduct = (product, e) => {
    TechnicalAssistanceApi.updateTechnicalAssistance(product)
        .then(r => {
              alert("Producto actualizado");
              location.reload();
        }).catch(e => {
              alert("No se pudo actualizar el producto, intentelo de nuevo");
        });
  }

  renderProducts() {
    return this.state.products.map(product => {
        return (
          <div key={product.id}>
          {
            (!this.state.mostrarForm) ? (
            <AsistenciaTecnicaCard >
              <AsistenciaTecnicaCardInfo>
                <CardPhoto>
                  <img src={`/uploads/images/technical_assistance/${product.image1}`} alt="foto asistente tecnico"/>
                </CardPhoto>
                <CardSpecifications>
                  <div>
                    <figure>
                      <img src="https://tractilus.s3.amazonaws.com/asistenciaTecnica/iconoMujer.svg" alt="icono marca"/>
                    </figure>
                    <div>
                      <h3>Nombre:</h3>
                      <p>{product.name}</p>
                    </div>
                  </div>
                  <div>
                    <figure>
                      <img src="https://tractilus.s3.amazonaws.com/asistenciaTecnica/asistencia.svg" alt="icono modelo"/>
                    </figure>
                    <div>
                      <h3>Grado de estudios:</h3>
                      <p>{product.degree}</p>
                    </div>
                  </div>
                  <div>
                    <figure>
                      <img src="https://tractilus.s3.amazonaws.com/asistenciaTecnica/asistencia.svg" alt="icono accionamiento"/>
                    </figure>
                    <div>
                      <h3>Especialidad:</h3>
                      <p>{product.speciality}</p>
                    </div>
                  </div>
                </CardSpecifications>
                <Line></Line>
                <CardSpecifications>
                  <div>
                    <figure>
                      <img src="https://tractilus.s3.amazonaws.com/asistenciaTecnica/organica.svg" alt="icono marca"/>
                    </figure>
                    <div>
                      <h3>Sistema agricola:</h3>
                          {product.convencional != 'undefined' &&
                            <p>Convencional</p>
                          }
                          {product.conservacion != 'undefined' &&
                            <p>Conservación</p>
                          }
                          {product.organica != 'undefined' &&
                            <p>Orgánica</p> 
                          }
                          {product.otra != 'undefined' &&
                            <p>{product.otra}</p>
                          }
                    </div>
                  </div>
                  <div>
                    <figure>
                      <img src="https://tractilus.s3.amazonaws.com/asistenciaTecnica/cosecha.svg" alt="icono modelo"/>
                    </figure>
                    <div>
                      <h3>Especialidades:</h3>
                      <p>{product.specialties}</p>
                    </div>
                  </div>
                  <div>
                    <figure>
                      <img src="https://tractilus.s3.amazonaws.com/asistenciaTecnica/frijol.svg" alt="icono accionamiento"/>
                    </figure>
                    <div>
                      <h3>Cultivo de especialidad:</h3>
                      <p>{product.typeCultivo}</p>
                    </div>
                  </div>
                </CardSpecifications>
                <Line></Line>
                <CardCost>
                  <h3>Precio</h3>
                  <p>${product.cost}</p>
                </CardCost>
              </AsistenciaTecnicaCardInfo>
              <AsistenciaTecnicaCardButtons>
                <figure onClick={(e) => this.deleteProduct(product.id, e)}>
                  <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoBorrar.png" alt="boton de eliminar"/>
                </figure>
                <div></div>
                <figure  onClick={() => this.setState({ mostrarForm: true })}>
                  <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoEditar.png" alt="boton de editar"/>
                </figure>
              </AsistenciaTecnicaCardButtons>
            </AsistenciaTecnicaCard>
          ):null
          }
            {
                    
              (this.state.mostrarForm) ? (
                <FormEdit
                  producto ={product}

              />
              ):null
          }
        </div>
        );
    })
  }

  render() {
    return (
        this.renderProducts()
    );
  }
}

export default TechnicalAssistance;