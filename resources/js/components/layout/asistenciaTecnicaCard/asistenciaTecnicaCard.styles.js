import styled from 'styled-components'

import { asistenciaTecnicaGray } from '../../../styles/colorStyles'

export const AsistenciaTecnicaCard = styled.div `
  display: flex;
  gap: 5%;
  margin-top: 2.5%;
  margin-bottom: 7.5%;
`

export const AsistenciaTecnicaCardInfo = styled.div `
  display: flex;
  justify-content: space-around;
  width: 90%;
  padding-top: 5%;
  padding-bottom: 5%;
  border-radius: 20px;
  box-shadow: 0px 5px 10px #00000029;
`

export const CardPhoto = styled.figure `
  width: 20%;
  margin-left: 2%;

  > img {
    width: 100%;
    height: 100%;
  }
`

export const Line = styled.div `
  background-color: rgba(203,203,203, 1);
  width: 1px;
  height: 100%;
  margin-right: 5%;
  margin-left: 5%;
`

export const CardSpecifications = styled.div `
  width: 22%;

  > div {
    display: flex;
    gap: 5%;

    > figure {
      width: 40px;
      height: 40px;
      margin-bottom: 5%;

      > img {
        width: 100%;
        height: 100%;
      }
    }

    > div {
      width: 70%;

      > h3 {
        color: rgba(129, 129, 129, 1);
        font-family: 'Montserrat';
        font-weight: 600;
        font-size: 1vw;
      }

      > p {
        color: rgba(129, 129, 129, 1);
        font-family: 'Montserrat';
        font-weight: 500;
        font-size: 1vw;
      }
    }
  }
`

export const CardCost = styled.div `
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 22%;

  > h3 {
    color: ${asistenciaTecnicaGray};
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.5vw;
  }

  > p {
    color: ${asistenciaTecnicaGray};
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.5vw;
  }
`


export const AsistenciaTecnicaCardButtons = styled.div `
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  
  > figure {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 80px;
    height: 80px;
    border-radius: 8px;
    background-color: ${asistenciaTecnicaGray};
    box-shadow: 0px 5px 10px #00000029;
    cursor: pointer;

    > img {
      width: 60%;
      height: auto;
    }
  }

  > div {
    width: 90%;
    height: 1px;
    margin-top: 2.5%;
    margin-bottom: 2.5%;
    background-color: rgba(203, 203, 203, 1);
  }
`