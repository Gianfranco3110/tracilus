import React, { useState } from 'react'

import {
  ProviderNavContainer,
} from './nav.styles'

import HamburguerMenu from '../hamburguerMenu/hamburguerMenu'

export default function ProviderNav () {
  const [ menu, setMenu ] = useState(false)

  const showMenu = () => setMenu(!menu)

  return (
    <>
    <HamburguerMenu showMenu={menu}/>
    <ProviderNavContainer>
      <div>
        <a href="/proveedor/inicio/agrega-productos/implementos/inventario">
          <figure>
            <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosImplementos.svg" alt="icono de implementos" />
          </figure>
          <p>Implementos</p>
        </a>
        <a href="/proveedor/inicio/agrega-productos/insumos/inventario">
          <figure>
            <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosInsumos.svg" alt="icono de insumos" />
          </figure>
          <p>Insumos</p>
        </a>
        <a href="/proveedor/inicio/agrega-productos/maquila/inventario">
          <figure>
            <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosMaquila.svg" alt="icono de maquila" />
          </figure>
          <p>Maquila</p>
        </a>
        <a href="/proveedor/inicio/agrega-productos/asistencia-tecnica/inventario">
          <figure>
            <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosAsistenciaTecnica.png" alt="icono de asistencia tecnica" />
          </figure>
          <p>Asistencia técnica</p>
        </a>
      </div>
      <div>
        <a href="/">Inicio</a>
        <div onClick={showMenu}>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
      </div>
    </ProviderNavContainer>
    </>
  )
}