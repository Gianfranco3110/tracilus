import styled from 'styled-components'

import { 
  asistenciaTecnicaGray, 
  blueDarkTractilus, 
  greyDarkTractilus, 
  implementoRed, 
  insumosBlue, 
  maquilaPurple, 
  orangeTractilus
} from '../../../../styles/colorStyles'

export const ProviderNavContainer = styled.div `
  display: flex;
  justify-content: space-between;
  width: 80%;
  margin: auto;
  padding-top: 2.5%;
  padding-bottom: 5%;
  border-top: 1px solid ${greyDarkTractilus};

  > div:nth-child(1) {
    display: flex;
    gap: 15%;
    align-items: center;
    margin-left: 2.5%;

    > a {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      text-decoration: none;
      font-family: 'Montserrat';
      font-size: 1.5vw;
      border-radius: 5px;
    
      > figure {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 45px;
        height: 45px;
        margin-bottom: 8%;
        box-shadow: 0px 3px 6px #00000029;
        border-radius: 8px;

        > img {
          width: 60%;
          height: auto;
        }
      }
    }

    > a:nth-child(1) {
      > p {
        color: ${implementoRed};
        font-size: 1vw;
      }
    }

    > a:nth-child(2) {
      > p {
        color: ${insumosBlue};
        font-size: 1vw;
      }
    }

    > a:nth-child(3) {

      > p {
        color: ${maquilaPurple};
        font-size: 1vw;
      }
    }

    > a:nth-child(4) {
      margin-top: 5%;

      > figure {
        > img {
          width: 40%;
        }
      }
   
      > p {
        color: ${asistenciaTecnicaGray};
        font-size: 1vw;
        text-align: center;
      }

    }
  }

  > div:nth-child(2) {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 15%;

    > a {
      font-family: 'Montserrat';
      font-weight: 600;
      text-decoration: none;
      color: ${blueDarkTractilus};
      transition: all 0.25s linear;

      &:hover {
        color: ${orangeTractilus};
      }
    }

    > div:nth-child(2) {
      display: flex;
      flex-direction: column;
      cursor: pointer;

      > div {
        margin-bottom: 16%;
        width: 30px;
        border: 1.3px solid rgba(80, 165, 140, 1);
        background-color: rgba(80, 165, 140, 1);
        border-radius: 5px;
      }
    }

    > div:nth-child(3) {
      display: none;
      width: 25px;
      height: 2px;
      position: relative;
      border-radius: 2px;

      > div:before,
        div:after {
        content: '';
        position: absolute; 
        width: 24px; 
        height: 3.5px;
        background-color: rgba(80, 165, 140, 1);
        border-radius: 2px;
      }

      > div:before {
        -webkit-transform: rotate(50deg);
        -moz-transform: rotate(50deg);
        transform: rotate(50deg);
        transform: rotate(50deg);
      }

      > div:after {
        -webkit-transform: rotate(-50deg);
        -moz-transform: rotate(-50deg);
        transform: rotate(-50deg);
        right: 2px;
      }
    }
  
    /* > figure {
      width: 30px;
      height: 30px;
      cursor: pointer;
  
      > img {
        width: 100%;
        height: 100%;
      }
    } */
  }

`
