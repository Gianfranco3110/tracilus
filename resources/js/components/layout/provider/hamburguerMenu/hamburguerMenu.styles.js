import styled from 'styled-components'

import { greyLigthTractilus } from '../../../../styles/colorStyles'

export const HamburguerMenuContainer = styled.div `
  position: absolute;
  display: ${props => props.showMenu ? 'flex' :'none'};
  flex-direction: column;
  top: 35%;
  right: 10%;
  padding: 2.5%;
  border-radius: 10px;
  background-color: ${greyLigthTractilus};
  box-shadow: 0px 3px 6px #00000029;

  > a {
    text-decoration: none;
    font-family: 'Montserrat';
    font-size: 1.3vw;
    color: black;
    margin-bottom: 20%;
    transition: all 0.25s linear;

    &:hover {
      transform: translateY(-5px);
    }
  }

  > div {
    margin-bottom: 10%;
    

    > h3 {
      font-weight: 500;
      font-family: 'Montserrat';
      font-size: 1.3vw;
      margin-bottom: 10%;
    }

    > div {
      display: ${props => props.showMyOrders ? 'flex' : 'none'};
      flex-direction: column;
      margin-left: 20%;

      > a {
        text-decoration: none;
        font-family: 'Montserrat';
        font-size: 1.3vw;
        color: black;
        margin-bottom: 10%;
        transition: all 0.25s linear;

        &:hover {
          transform: translateY(-5px);
        }
      }
    }
  }
  

`
