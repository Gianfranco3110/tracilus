import React, { useEffect, useState } from 'react'

import {
  HamburguerMenuContainer
} from './hamburguerMenu.styles'


export default function HamburguerMenu (props) {
  const [ showMenu, setShowMenu ] = useState(props.showMenu)
  const [ myOrders, setMyOrders ] = useState(false)

  useEffect( () => {
    setShowMenu(props.showMenu)
  },[props.showMenu])

  const showMyOrders = () => setMyOrders(true)
  const hideMyOrders = () => setMyOrders(false)

  return (
    <HamburguerMenuContainer showMenu={showMenu} showMyOrders={myOrders}>
      <a href="#">Mi perfil</a>
      <div onMouseOver={showMyOrders} onMouseLeave={hideMyOrders}>
        <h3>Mis pedidos</h3>
        <div>
          <a href="#">Activos</a>
          <a href="#">Historicos</a>
        </div>
      </div>
      <a href="#">Calculadora</a>
      <a href="#">Buzón de atención</a>
      <a href="#">Mis beneficios</a>
      <a href="#">Capacitación</a>
      <a href="#">Configuración</a>
    </HamburguerMenuContainer>
  )
}