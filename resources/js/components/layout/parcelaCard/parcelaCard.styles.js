import styled from 'styled-components'

import { blueDarkTractilus } from '../../../styles/colorStyles'

export const ParcelaCard = styled.div `
  display: flex;
  gap: 3%;
  margin-top: 2.5%;
  margin-bottom: 7.5%;


  @media (max-width: 600px) {
    flex-direction: column;
    margin-top: 10%;
    margin-bottom: 10%;
  }
`

export const ParcelaCardInfo = styled.div `
  display: flex;
  justify-content: space-around;
  width: 90%;
  padding-top: 3%;
  padding-bottom: 3%;
  border-radius: 20px;
  box-shadow: 0px 5px 10px #00000029;
 
  

  @media (max-width: 600px) {
    flex-direction: column;
    justify-content: space-around;
    width: 100%;
    padding-top: 5%;
    padding-bottom: 1%;
  }
`

export const CardPhoto = styled.figure `
  display: flex;
  margin:auto 0 auto 0;
  width: 13.5%;
  height: 120px;
  /* margin-left: 2%; */

  > img {
    width: 100%;
    height: 100%;
  }

  @media (max-width: 600px) {
    width: 80%;
    height: 150px;
    margin:auto;

    > img {
      
    }
  }
`

export const Line = styled.div `
  background-color: rgba(203,203,203, 1);
  width: 1px;
  height: 100%;
  /* margin-right: 5%;
  margin-left: 5%; */

  @media (max-width: 600px) {
    width: 80%;
    height: 1px;
    margin-right: 10%;
    margin-left: 10%;
  }
`

export const CardName = styled.div `
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 15%;
  /* margin: auto; */
  /* margin-left: 5%; */

  > h3 {
    color: rgba(129, 129, 129, 1);
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1vw;
  }

  > p {
    color: ${blueDarkTractilus};
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.5vw;
  }

  @media (max-width: 600px) {
    width: 30%;
    margin:10% 0 8% 10%;

    > h3 {
      font-size: 4vw;
    }

    > p {
      font-size: 5vw;
    }
  }
`

export const CardSpecifications = styled.div `
  width: 20%;

  > div {
    display: flex;
    justify-content: center;
    align-items:center; 
    gap: 8%;

    > figure {
      width: 12%;

      > img {
        object-fit: cover;
        width: 100%;
        height: 100%;
      }
    }

    > div {
      width: 70%;
      margin-top: 2%;
      margin-bottom: 2%;
     
      > h3 {
        color: rgba(129, 129, 129, 1);
        font-family: 'Montserrat';
        font-weight: 600;
        font-size: 1vw;
      }

      > p {
        color: rgba(129, 129, 129, 1);
        font-family: 'Montserrat';
        font-weight: 500;
        font-size: 1vw;
      }
    }
  }

  @media (max-width: 600px) {
    width: 80%;
    margin: 5% auto;

  > div {
    display: flex;
    justify-content: center;
    align-items:center; 
    gap:0%;

    > figure {
      width: 10%;

      > img {
        object-fit: cover;
        width: 100%;
        height: 100%;
      }
    }


    > div {
      width: 80%;
      padding-top: 8%;
      margin-bottom: 8%;
     
     
      > h3 {
        font-size: 3vw;
        padding-left : 15%;
      }

      > p {
        font-size: 3vw;
        padding-left : 15%;
      }
    }
  }
}
    
`

export const CardCost = styled.div `
  display: flex;
  width: 23%;
  /* padding-right: 5%; */
  gap: 8%;

  > figure {
    width: 10%;
    height: 22px;
    

    > img {
      width: 100%;
      height: 100%;
    }
  }
  

  > div {
    
    width: 90%;
  
    > h3 {
      color: rgb(129,129,129);
      font-family: 'Montserrat';
      font-weight: 600;
      font-size: 1vw;
    }

    > p {
      width: 75%;
      color: rgb(129,129,129);
      font-family: 'Montserrat';
      font-weight: 500;
      font-size: 1vw;
      line-height: 17px;
    }  
  }

  @media (max-width: 600px) {
    display: flex;
    width: 90%;
    margin:10% auto;
    padding-left: 8%;
    gap: 1%;

    > figure {
      width: 8%;
      height: 28px;

      > img {
        width: 100%;
        height: 100%;
      }
    }
    

    > div {
      width: 90%;
    
    
      > h3 {
        font-size: 2.5vw;
        padding-left: 15%;
      }

      > p {
        font-size: 2.5vw;
        padding-left: 15%;
        width: 90%;
      }  
    }
  }
`


export const ParcelaCardButtons = styled.div `
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  align-items: center;
  
  
  > figure {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 55px;
    height: 55px;
    border-radius: 8px;
    background-color: ${blueDarkTractilus};
    box-shadow: 0px 5px 10px #00000029;
    cursor: pointer;

    > img {
      width: 60%;
      height: auto;
    }
  }

  > div {
    width: 90%;
    height: 1px;
    margin-top: 2.5%;
    margin-bottom: 2.5%;
    background-color: rgba(203, 203, 203, 1);
  }

  @media (max-width: 600px) {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
    align-items: center;
    margin: 10% auto 0 auto;
    width: 70%;
  

    > figure { 
      width: 60px;
      height: 60px;
      border-radius: 8px;
      cursor: pointer;

      > img {
        
      }
    }

    > div {
      width: .5%;
      height: 60px;
      margin-top: 2.5%;
      margin-bottom: 2.5%;
    }
  }
`


