import React from 'react'

import {
  ParcelaCard,
  ParcelaCardInfo,
  CardPhoto,
  CardName,
  CardSpecifications,
  CardCost,
  Line,
  ParcelaCardButtons
} from './parcelaCard.styles'


export default function Parcela () {

  return (
    <ParcelaCard>
      <ParcelaCardInfo>
        <CardPhoto>
          <img src="https://tractilus.s3.amazonaws.com/terreno-juan-l-mallorquin-TEV8241569558652-750%402x.png" alt="foto producto" />
        </CardPhoto>
        <CardName>
          <h3>Parcela</h3>
          <p>Juan L. Mallorquin</p>
        </CardName>
        <Line></Line>
        <CardSpecifications>
          <div>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/MIsParcelas/Icon+awesome-tree%402x.png" alt="icono arbol" />
            </figure>
            <div>
              <h3>Parcela:</h3>
              <p>Propia</p>
            </div>
          </div>
          <div>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/MIsParcelas/Icon+ionic-ios-resize%402x.png" alt="icono flecha" />
            </figure>
            <div>
              <h3>Tamaño:</h3>
              <p>1200 ha</p>
            </div>
          </div>
          <div>
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/MIsParcelas/Icon+feather-clock%402x.png" alt="icono reloj" />
            </figure>
            <div>
              <h3>Uso:</h3>
              <p>Temporal</p>
            </div>
          </div>
        </CardSpecifications>
        <Line></Line>
        <CardCost>
          <figure>
            <img src="https://tractilus.s3.amazonaws.com/MIsParcelas/Icon+feather-map-pin%402x.png" alt="icono ubicacion" />
          </figure>
          <div>
            <h3>Localización</h3>
            <p>
              $R. Mal. Floriano Peixoto, 1500 sala 01 - Centro, 
              Foz do Iguaçu PR, 85851-020, Brazil
            </p>
          </div>
        </CardCost>
      </ParcelaCardInfo>
      <ParcelaCardButtons>
        <figure>
          <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoBorrar.png" alt="boton de eliminar" />
        </figure>
        <div></div>
        <figure>
          <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoEditar.png" alt="boton de editar" />
        </figure>
      </ParcelaCardButtons>
    </ParcelaCard>
  )
}