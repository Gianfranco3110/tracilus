import React, { Component , useState } from 'react';
import ReactDOM from 'react-dom';
import ImplementApi from '../../api/implementRepository';
// import { useNavigate } from 'react-router-dom'


import FormEdit from '../../provider/addToCatalog/insumos/form/part2/partEdit2'

import {
  ImplementoCard,
  ImplementoCardInfo,
  CardPhoto,
  CardName,
  CardSpecifications,
  CardCost,
  Line,
  ImplementoCardButtons
} from './implementoCard.styles'
 
/* Main Component */
class Implement extends Component {
  constructor() {
    super();
    //Initialize the state in the constructor
    this.state = {
        products: [],
        mostrarForm: false
    }
  }

  /*componentDidMount() is a lifecycle method
   * that gets called after the component is rendered
   */
  componentDidMount() {
    /* fetch API in action */
    let user_id = localStorage.getItem("id");
    ImplementApi.getImplementByUserId(user_id)
        .then(response => {
            return response.data;
        })
        .then(products => {
            this.setState({products});
        }).catch(e => {
            console.error(e);
            console.log("Error getting products");
        });
  }

  deleteProduct = (product, e) => {
    ImplementApi.deleteImplement(product)
        .then(r => {
              alert("Producto eliminado");
              location.reload();
        }).catch(e => {
              alert("No se pudo eliminar el producto, intentelo de nuevo");
        });
  }
  updateProduct = (product, e) => {
    console.log('product',product);
    ImplementApi.updateImplement(product)
        .then(r => {
              alert("Producto actualizado");
              location.reload();
        }).catch(e => {
              alert("No se pudo actualizar el producto, intentelo de nuevo");
        });
  }

  renderProducts() {
    return this.state.products.map(product => {
        return (
          <div key={product.id} >
            {
              (!this.state.mostrarForm) ? (
             <ImplementoCard  >
                <ImplementoCardInfo>
                  <CardPhoto>
                  {
                    console.log('product.image1',product.image1)
                  }
                    <img src={`/uploads/images/implements/${product.image1}`} alt="foto producto" />
                  </CardPhoto>
                  <CardName>
                    <h3 >{product.model}</h3>
                    <p >{product.activity}</p>
                  </CardName>
                  <Line></Line>
                  <CardSpecifications>
                    <div>
                      <figure>
                        <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosMarca.png" alt="icono marca" />
                      </figure>
                      <div>
                        <h3>Marca:</h3>
                        <p>{product.brand}</p>
                      </div>
                    </div>
                    <div>
                      <figure>
                        <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplemetosTractorRojo.png" alt="icono modelo" />
                      </figure>
                      <div>
                        <h3>Modelo:</h3>
                        <p>{product.model}</p>
                      </div>
                    </div>
                    <div>
                      <figure>
                        <img src="https://tractilus.s3.amazonaws.com/Implementos/ImplementosTipoDeAccionamiento.png" alt="icono accionamiento" />
                      </figure>
                      <div>
                        <h3>Tipo de accionamiento:</h3>
                        {product.type == 'fourWheels' &&
                          <p>Tractor 4 ruedas</p>
                        }
                        {product.type == 'twoWheels' &&
                          <p>Tractor 2 ruedas</p>
                        }
                        {product.type == 'animal' &&
                          <p>Animal</p>
                        }
                      </div>
                    </div>
                  </CardSpecifications>
                  <Line></Line>
                  <CardCost>
                    <h3>Precio</h3>
                    <p>${product.cost}</p>
                  </CardCost>
                </ImplementoCardInfo> 
                <ImplementoCardButtons>
                  <figure onClick={(e) => this.deleteProduct(product.id, e)}>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoBorrar.png" alt="boton de eliminar" />
                  </figure>
                  <div></div>
                  <figure onClick={() => this.setState({ mostrarForm: true })}>
                    <img src="https://tractilus.s3.amazonaws.com/Implementos/IconoEditar.png" alt="boton de editar" />
                  </figure>
                </ImplementoCardButtons>
              </ImplementoCard> 
              ):null
              }
              {
                  (this.state.mostrarForm) ? (
                    <FormEdit
                      producto ={product}

                  />
                  ):null
              }  
            </div> 
        );
    })
  }

  render() {
    /* Some css code has been removed for brevity */
    return (
        this.renderProducts()
    );
  }
}

export default Implement;