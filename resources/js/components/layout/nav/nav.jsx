import React from "react"

import {
    NavContainer  
} from './nav.styles'

export default function Nav () {
    return (
      <NavContainer>
        <nav>
          <a href="/">
            <figure>
              <img src="https://tractilus.s3.amazonaws.com/logoTractilus.svg" alt="logo de tractilus" />
            </figure>
          </a>
          <div>
            <a href="/iniciar-sesion">Iniciar sesión</a>
            <a href="/registrate">Registrate</a>
          </div>
        </nav>
      </NavContainer>
    )
}