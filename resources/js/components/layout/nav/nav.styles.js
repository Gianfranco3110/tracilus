import styled from 'styled-components'

import { blueDarkTractilus, greenLightTractilus, orangeTractilus, whiteTractilus } from '../../../styles/colorStyles'

export const NavContainer = styled.header `
  padding-top: 3%;
  padding-bottom: 3%;

  > nav {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 90%;

    > a {

      > figure {
        margin-left: 30%;
        width: 200px;
        height: auto;
  
        > img {
          width: 100%;
          height: 100%;
        }
      }
    }

    > div {
      display: flex;
      justify-content: space-around;
      align-items: center;
      width: 30%;

      > a {
        font-family: 'Montserrat';
        font-weight: 600;
        text-decoration: none;
      }

      > a:nth-child(1) {
        color: ${blueDarkTractilus};
        transition: all 0.25s linear;

        &:hover {
          color: ${orangeTractilus};
        }
      }

      > a:nth-child(2) {
        background-color: ${orangeTractilus};
        color: ${whiteTractilus};
        padding: 2% 12%;
        border-radius: 25px;
        transition: all 0.25s linear;

        &:hover {
          background-color: ${greenLightTractilus};
          transform: translateY(-3px);
          box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
        }
      }
    }
  }

  @media (max-width: 600px) {
    padding-top: 8%;
    padding-bottom: 8%;

    > nav {
      display: flex;
      flex-direction: column;
      align-items: center;
      width: 100%;

      > a {

        > figure {
          padding-bottom: 10%;
          width: 200px;
          height: auto;
          margin-left: 0;
    
          > img {
            width: 100%;
            height: 100%;
          }
        }
      }

      > div {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;

        > a {
          padding-bottom: 5%;
        }

        > a:nth-child(1) {
  

          &:hover {

          }
        }

        > a:nth-child(2) {
          padding: 3.5%;
          width: 80%;
          height: 45px;
          text-align: center;
          
          &:hover {

          }
        }
      }
    }
  }
`