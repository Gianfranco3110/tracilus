import axios from 'axios';  

import {
    tokenName,
    baseUrl,
} from '../../app/parameters'

const billRepository = () => {  
    //CRUD METHODS

    //Create
    const newBill = (bill) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)
                }  
            });  
     
            instance.post('/api/bill', bill)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                });  
        }); 
    }; 

    // Read All
    const getBill = () => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/bill')  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    // Read Resource Specified
    const getBillById = (idBill) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/bill/'+ idBill)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    //Update
    const updateBill = (bill) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.put('/api/bill/' + bill.id, bill)
                .then(r => {
                    resolve(r.data);
                }).catch(e => {  
                    reject(e.response);
                });
        }); 
    };  

    //Delete
    const deleteBill = (idBill) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.delete('/api/bill/'+ idBill)  
            .then(r => {   
                resolve(r.data);  
            }).catch(e => {  
                console.log(e);  
                reject(e.response);  
            });  
        }); 
    };

    return {  
        newBill,
        getBill,
        getBillById,
        updateBill,
        deleteBill
    }  
};  
  
export default billRepository();