import axios from "axios";

import { tokenName, baseUrl } from "../../app/parameters";

const implementRepository = () => {
    //CRUD METHODS

    //Create
    const newImplement = (implement) => {
        console.log('implement',implement)
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "multipart/form-data",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .post("/api/implement", implement)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    reject(e.response);
                });
        });
    };

    // Read All
    const getImplement = () => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/implement")
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    // Read Resource Specified
    const getImplementById = (idImplement) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/implement/" + idImplement)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    // Read Resource By User
    const getImplementByUserId = (idUser) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/show_by_user_implements/" + idUser)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    //Update
    const updateImplement = (implement) => {
        console.log("implement", implement);
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });
            instance
                .put("/api/implement/" + implement.id, implement)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    reject(e.response);
                });
        });
    };

    //Delete
    const deleteImplement = (idImplement) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .delete("/api/implement/" + idImplement)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    return {
        newImplement,
        getImplement,
        getImplementById,
        getImplementByUserId,
        updateImplement,
        deleteImplement,
    };
};

export default implementRepository();
