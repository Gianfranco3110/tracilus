import axios from 'axios';  

import {
    tokenName,
    baseUrl,
} from '../../app/parameters'

const parcelaRepository = () => {  
    //CRUD METHODS

    //Create
    const newParcela = (parcela) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)
                }  
            });  
     
            instance.post('/api/parcela', parcela)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                });  
        }); 
    }; 

    // Read All
    const getParcela = () => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/parcela')  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    // Read Resource Specified
    const getParcelaById = (idParcela) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/parcela/'+ idParcela)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    //Update
    const updateParcela = (parcela) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.put('/api/parcela/' + parcela.id, parcela)
                .then(r => {
                    resolve(r.data);
                }).catch(e => {  
                    reject(e.response);
                });
        }); 
    };  

    //Delete
    const deleteParcela = (idParcela) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.delete('/api/parcela/'+ idParcela)  
            .then(r => {   
                resolve(r.data);  
            }).catch(e => {  
                console.log(e);  
                reject(e.response);  
            });  
        }); 
    };

    return {  
        newParcela,
        getParcela,
        getParcelaById,
        updateParcela,
        deleteParcela
    }  
};  
  
export default parcelaRepository();