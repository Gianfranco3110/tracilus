import axios from 'axios';  

import {
    tokenName,
    baseUrl,
} from '../../app/parameters'

const locationRepository = () => {  
    //CRUD METHODS

    //Create
    const newLocation = (location) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)
                }  
            });  
     
            instance.post('/api/location', location)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                });  
        }); 
    }; 

    // Read All
    const getLocation = () => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/location')  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    // Read Resource Specified
    const getLocationById = (idLocation) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/location/'+ idLocation)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    //Update
    const updateLocation = (location) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.put('/api/location/' + location.id, location)
                .then(r => {
                    resolve(r.data);
                }).catch(e => {  
                    reject(e.response);
                });
        }); 
    };  

    //Delete
    const deleteLocation = (idLocation) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.delete('/api/location/'+ idLocation)  
            .then(r => {   
                resolve(r.data);  
            }).catch(e => {  
                console.log(e);  
                reject(e.response);  
            });  
        }); 
    };

    return {  
        newLocation,
        getLocation,
        getLocationById,
        updateLocation,
        deleteLocation
    }  
};  
  
export default locationRepository();