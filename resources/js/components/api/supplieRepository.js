import axios from "axios";

import { tokenName, baseUrl } from "../../app/parameters";

const supplieRepository = () => {
    //CRUD METHODS

    //Create
    const newSupplie = (supplie) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "multipart/form-data",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .post("/api/supplie", supplie)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    reject(e.response);
                });
        });
    };

    // Read All
    const getSupplie = () => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/supplie")
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    // Read Resource Specified
    const getSupplieById = (idSupplie) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/supplie/" + idSupplie)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    // Read Resource By User
    const getSupplieByUserId = (idUser) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/show_by_user_supplies/" + idUser)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    //Update
    const updateSupplie = (supplie) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .put("/api/supplie/" + supplie.id, supplie)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    reject(e.response);
                });
        });
    };

    //Delete
    const deleteSupplie = (idSupplie) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .delete("/api/supplie/" + idSupplie)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    return {
        newSupplie,
        getSupplie,
        getSupplieById,
        getSupplieByUserId,
        updateSupplie,
        deleteSupplie,
    };
};

export default supplieRepository();
