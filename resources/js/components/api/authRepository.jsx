import axios from 'axios';  

import {
    tokenName,
    baseUrl,
    login,
    register,
    registerSecond,
    logout,
} from '../../app/parameters'
  
const authRepository = () => {  
    
    const getLocalToken = () => {  
        return JSON.parse(localStorage.getItem(tokenName));  
    };  
    
    const logIn = user => {  
        return new Promise((resolve, reject) => {  
            const instance = axios.create({  
                baseURL: baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }  
            });  
 
            instance.post(login, user)  
                .then(r => {  
                    localStorage.setItem(tokenName, r.data.data.token);   
                    localStorage.setItem("id", r.data.data.id);
                    localStorage.setItem('user_type', r.data.data.user_type);   
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                }); 
        }); 
    };  

    const signUp = user => {  
        return new Promise((resolve, reject) => {  
            const instance = axios.create({  
                baseURL: baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }  
            });  
            instance.post(register, user)  
                .then(r => {  
                    localStorage.setItem(tokenName, r.data.data.token);  
                    localStorage.setItem("id", r.data.data.id);
                    resolve(r.data);  
                }).catch(e => { 
                    reject(e.response);  
                }); 
        }); 
    };  

    const signUpSecond = user => {  
        return new Promise((resolve, reject) => {  
            const instance = axios.create({  
                baseURL: baseUrl, 
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)
                }  
            });  
            instance.put(registerSecond, user)  
                .then(r => {  
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                }); 
        }); 
    };

    const logOut = () => {  
        return new Promise((resolve, reject) => {  
            const instance = axios.create({  
                baseURL: baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)
                }  
            });  

            instance.post(logout, {})  
                .then(r => {  
                    localStorage.removeItem(tokenName);  
                    localStorage.removeItem("id");  
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                }); 
        }); 
    };

    const verifiedPhone = user => {  
        return new Promise((resolve, reject) => {  
            const instance = axios.create({  
                baseURL: baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }  
            });  

            instance.post("/phone/verify", user)  
                .then(r => {  
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                }); 
        }); 
    };  
    
    return {  
        logIn, 
        signUp,
        signUpSecond,
        logOut,
        verifiedPhone
    }  
};  
  
export default authRepository();