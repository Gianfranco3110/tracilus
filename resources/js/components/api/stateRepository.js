import axios from 'axios';  

import {
    tokenName,
    baseUrl,
} from '../../app/parameters'

const stateRepository = () => {

    //CRUD METHODS

    //Create
    const newState = (state) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)
                }  
            });  
     
            instance.post('/api/state', state)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                });  
        }); 
    }; 

    // Read All
    const getState = () => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/state')  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    // Read Resource Specified
    const getStateById = (idState) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/state/'+ idState)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    // Read Resource By Country
    const getStateByCountryId = (idCountry) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/show_by_country/' + idCountry)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    //Update
    const updateState = (state) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.put('/api/state/' + state.id, state)
                .then(r => {
                    resolve(r.data);
                }).catch(e => {  
                    reject(e.response);
                });
        }); 
    };  

    //Delete
    const deleteState = (idState) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.delete('/api/state/'+ idState)  
            .then(r => {   
                resolve(r.data);  
            }).catch(e => {  
                console.log(e);  
                reject(e.response);  
            });  
        }); 
    };

    return {  
        newState,
        getState,
        getStateById,
        getStateByCountryId,
        updateState,
        deleteState
    }  
};  
  
export default stateRepository();