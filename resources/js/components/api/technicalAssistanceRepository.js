import axios from "axios";

import { tokenName, baseUrl } from "../../app/parameters";

const technicalAssistanceRepository = () => {
    //CRUD METHODS

    //Create
    const newTechnicalAssistance = (technical_assistance) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "multipart/form-data",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .post("/api/technical_assistance", technical_assistance)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    reject(e.response);
                });
        });
    };

    // Read All
    const getTechnicalAssistance = () => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/technical_assistance")
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    // Read Resource Specified
    const getTechnicalAssistanceById = (idTechnicalAssistance) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/technical_assistance/" + idTechnicalAssistance)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    // Read Resource By User
    const getTechnicalAssistanceByUserId = (idUser) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .get("/api/show_by_user_technical_assistance/" + idUser)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    //Update
    const updateTechnicalAssistance = (technical_assistance) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .put(
                    "/api/technical_assistance/" + technical_assistance.id,
                    technical_assistance
                )
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    reject(e.response);
                });
        });
    };

    //Delete
    const deleteTechnicalAssistance = (idTechnicalAssistance) => {
        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: baseUrl,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem(tokenName),
                },
            });

            instance
                .delete("/api/technical_assistance/" + idTechnicalAssistance)
                .then((r) => {
                    resolve(r.data);
                })
                .catch((e) => {
                    console.log(e);
                    reject(e.response);
                });
        });
    };

    return {
        newTechnicalAssistance,
        getTechnicalAssistance,
        getTechnicalAssistanceById,
        getTechnicalAssistanceByUserId,
        updateTechnicalAssistance,
        deleteTechnicalAssistance,
    };
};

export default technicalAssistanceRepository();
