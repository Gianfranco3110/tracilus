import axios from 'axios';  

import {
    tokenName,
    baseUrl,
} from '../../app/parameters'

const cityRepository = () => {  
    //CRUD METHODS

    //Create
    const newCity = (city) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)
                }  
            });  
     
            instance.post('/api/city', city)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    reject(e.response);  
                });  
        }); 
    }; 

    // Read All
    const getCity = () => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/city')  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    // Read Resource Specified
    const getCityById = (idCity) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/city/' + idState)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    // Read Resource By State
    const getCityByStateId = (idState) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName)  
                }  
            });  
     
            instance.get('/api/show_by_state/' + idState)  
                .then(r => {   
                    resolve(r.data);  
                }).catch(e => {  
                    console.log(e);  
                    reject(e.response);  
                });  
        }); 
    };

    //Update
    const updateCity = (city) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.put('/api/city/' + city.id, city)
                .then(r => {
                    resolve(r.data);
                }).catch(e => {  
                    reject(e.response);
                });
        }); 
    };  

    //Delete
    const deleteCity = (idCity) => {  
        return new Promise( (resolve, reject) => {  
            const instance = axios.create({  
                baseURL : baseUrl,  
                headers: {  
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem(tokenName) 
                }  
            });  
     
            instance.delete('/api/city/'+ idCity)  
            .then(r => {   
                resolve(r.data);  
            }).catch(e => {  
                console.log(e);  
                reject(e.response);  
            });  
        }); 
    };

    return {  
        newCity,
        getCity,
        getCityById,
        getCityByStateId,
        updateCity,
        deleteCity
    }  
};  
  
export default cityRepository();