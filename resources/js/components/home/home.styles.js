import styled from 'styled-components'

import { 
    blueDarkTractilus, 
    greenDarkTractilus, 
    greenLightTractilus, 
    greenTractilus, 
    greyDarkTractilus, 
    greyLigthTractilus, 
    greyTractilus, 
    orangeTractilus, 
    whiteTractilus 
} from '../../styles/colorStyles'

export const HomeContainer = styled.main `

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
  }
`

export const HomeText = styled.div `
  width: 100%;
  padding-left: 5%;
  padding-top: 5%;
  padding-bottom: 5%;
  background-image:url('https://tractilus.s3.amazonaws.com/bannerTractilus.png') ;
  background-size: 90%;
  background-position: 50%;
  background-repeat: no-repeat;
  
  > div:nth-child(1) {
    display: flex;
    align-items: center;
    width: 80%;
    margin-left: 5%;
    
    > div:nth-child(1) {
      width: 15%;
      height: 1px;
      border: 1px solid ${orangeTractilus};
      background-color: ${orangeTractilus};
      margin-right: 3%;
    }

    > h2 {
      font-family: 'Montserrat';
      font-weight: 400;
      font-size: 1.5vw;
      color: ${orangeTractilus};
    }
  }
  
  > h1 {
    color: ${blueDarkTractilus};
    font-weight: 300;
    font-size: 1.5vw;
    font-family: 'Montserrat';
    width: 32%;
    margin-top: 5%;
    margin-bottom: 5%;
    margin-left: 5%;
  }
  
  > p {
    color: ${blueDarkTractilus};
    font-weight: 600;
    font-size: 2vw;
    font-family: 'Montserrat';
    width: 28%;
    margin-bottom: 10%;
    margin-left: 5%;
  }
  
  > figure {
    width: 30px;
    height: 30px;
    cursor: pointer;
    margin-left: 5%;

    > a {
      > img {
        width: 100%;
        height: 100%;
      }
    }
  }
  
  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    padding-bottom: 100%;
    width: 100%;
    background-image:url('https://tractilus.s3.amazonaws.com/mobileTractilus.png') ;

    > div:nth-child(1) {
      margin-top: 10%;
      
      > div:nth-child(1) {
        width: 35%;
        height: 1px;
        margin-right: 5%;
      }

      > h2 {
        font-size: 3.5vw;
        width: 34%;
      }
    }
    
    > h1 {
      font-size: 5vw;
      width: 92%;
    }
    
    > p {
      font-size: 5vw;
      width: 73%;
    }
    
    > figure {
      width: 30px;
      height: 30px;

      > a {
        > img {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
`

export const ChooseUser = styled.section `
  background-color: rgba(0,0,255,0.3);
  padding-top: 5%;
  padding-bottom: 5%;
  background-image: url("https://tractilus.s3.amazonaws.com/fondo.png");
  background-size: cover;
  background-position-y: 70%;

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    background-color: rgba(0,0,255,0.3);
    padding-top: 5%;
    padding-bottom: 5%;
    background-image: url("https://tractilus.s3.amazonaws.com/fondo.png");
    background-size: cover;
    background-position-y: 70%;
  }
`

export const ChooseUserTitle = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 2%;
  
  > div:nth-child(1) {
    width: 8%;
    height: 1px;
    border: 1px solid ${whiteTractilus};
    background-color: ${whiteTractilus};
    margin-right: 2.5%;
  }

  > h2 {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.4vw;
    color: ${whiteTractilus};
  }

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    margin-top: 5%;
    margin-bottom: 5%;
    
    > div:nth-child(1) {
      width: 20%;
      
    }

    > h2 {
      width: 33%;
      font-size: 3.5vw;
     
    }
  }

`

export const ChooseUserText = styled.p `
  color: ${whiteTractilus};
  text-align: center;
  font-size: 2.2vw;
  font-family: 'Montserrat';
  font-weight: 600;
  width: 50%;
  margin: auto;

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    font-size: 6vw;
    width: 78%;
  }
`

export const ChooseUserCards = styled.div `
  display: flex;
  justify-content: space-around;
  width: 60%;
  margin: 5% auto 0 auto;

  > div:nth-child(1) {
    > div {
      background-color: ${greenTractilus};
    }
  }

  > div:nth-child(2) {
    > div {
      background-color: ${greenDarkTractilus};
    }
  }

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    display: flex;
    flex-direction: column;
    width: 90%;
    margin: 5% auto 0 auto;

    > div:nth-child(1) {
      > div {
      }
    }

    > div:nth-child(2) {
      > div {
      }
    }
  }
`

export const UserCard = styled.div `
  border-radius: 15px;
  width: 35%;

  > figure {
    width: 100%;
    height: 280px;
    
    > img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }

  > div {
    padding-top:10%;
    padding-bottom: 10%;
    border-bottom-right-radius: 15px;
    border-bottom-left-radius: 15px;

    > h2 {
      color: ${whiteTractilus};
      text-align: center;
      font-family: 'Montserrat';
      font-weight: 600;
      margin-bottom: 10%;
    }

    > a {
      display: block;
      text-align: center;
      text-decoration: none;
      color: ${whiteTractilus};
      font-size: 1.2vw;
      font-family: 'Montserrat';
      font-weight: 500;
      background-color: ${orangeTractilus};
      width: 50%;
      margin: auto auto 5% auto;
      padding: 2% 6%;
      border-radius: 20px;
      transition: all 0.25s linear;

      &:hover {
        background-color: ${greenLightTractilus};
        transform: translateY(-3px);
        box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
      }
    }
  }

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    width: 100%;
    margin-bottom: 15%;

    > figure {
    
      > img {
        border-top-right-radius: 15px;
        border-top-left-radius: 15px;
      }
    }

    > div {
      padding-top:10%;
      padding-bottom: 10%;
      border-bottom-right-radius: 15px;
      border-bottom-left-radius: 15px;

      > h2 {
      }

      > a {
        padding-top: 3%;
        height: 45px;
        font-size: 4vw;
        width: 90%;
      }
    }
  }
`

export const ChooseProducts = styled.section `
  padding-top: 5%;

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {

  }
`

export const ChooseProductsTitle = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 2%;
  
  > div:nth-child(1) {
    width: 8%;
    height: 1px;
    border: 1px solid ${greenLightTractilus};
    background-color: ${greenLightTractilus};
    margin-right: 2.5%;
  }

  > h2 {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.4vw;
    color: ${greenLightTractilus};
  }

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
  
    margin-bottom: 2%;
    
    > div:nth-child(1) {
      width: 20%;
    }

    > h2 {
      width: 25%;
      font-size: 3.5vw;
    }
  }
`

export const ChooseProductsText = styled.p `
  color: ${blueDarkTractilus};
  text-align: center;
  font-size: 2.2vw;
  font-family: 'Montserrat';
  font-weight: 600;
  width: 50%;
  margin: auto;

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    font-size: 6vw;
    width: 65%;
    margin: 5% auto 10% auto;
  }
`

export const ChooseProductsCards = styled.div `
    @media (min-width:600px) and (max-width: 1024px){
    }

    @media (max-width: 600px) {
    }
`

export const ProductsCards = styled.div `
  display: flex;
  justify-content: space-between; 
  width: 90%;
  margin: 3% auto;

  > figure:nth-child(1) {
    width: 20%;
    height: 320px;
    border-radius: 15px;

    > img {
      width: 100%;
      height: 100%;
      object-fit: contain;
      border-radius: 15px;
    }
  }

  > figure:nth-child(2) {
    width: 50%;
    height: 320px;

    > img {
      width: 100%;
      height: 100%;
      object-fit: contain;
    }
  }

  > div {
    width: 20%;

    > figure {
      
      > img {
        width: 100%;
        height: 100%;
        object-fit: contain;
      }
    }
  }

  @media (min-width:600px) and (max-width: 1024px) {
   
    > figure {
      
    }

    > figure:nth-child(1) {
      height: 230px;

      > img {
        
      }
    }

    > figure:nth-child(2) {
      height: 230px;

      > img {
        
      }
    }

    > div {

      > figure {
        
        > img {
          
        }
      }
    }
  }

  @media (max-width: 600px) {
    display: flex;
    justify-content: center; 
    width: 90%;
    margin: 3% auto;
    
    > figure {
      
    }

    > figure:nth-child(1) {
      width: 20%;
      height: 320px;
      display: none;

      > img {
        width: 100%;
        height: 100%;
      }
    }

    > figure:nth-child(2) {
      width: 60%;
      height: 200px;

      > img {
        width: 100%;
        height: 100%;     
      }
    }

    > div {
      width: 20%;
      display: none;

      > figure {
        
        > img {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
`

export const ProductsControls = styled.div `
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 90%;
  margin: auto;

  > div {
    display: flex;
    align-items: center;

    > p {
      font-family: 'Montserrat';
      font-size: 1.3vw;
      font-weight: bold;
      color: ${blueDarkTractilus};
    }

    > figure {
      width: 11.5px;
      height: 20px;
      cursor: pointer;

      > img {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
    }
  }

  > div:nth-child(1) {
      width: 20%;
      
    > p {
      width: 100%;
      margin-left: 5%;  
    }
  }

  > div:nth-child(2) {
      width: 9%;  

    > p {
      width: 100%;      
    }
  }

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {

    > div {

      > p {
        font-size: 4vw;     
      }

      > figure {
        width: 12px;
        height: 20px;
        cursor: pointer;

        > img {
          width: 100%;
          height: 100%;
          object-fit: cover;
        }
      }
    }

    > div:nth-child(1) {
      width: 20%;
      
      > p {
        width: 100%;
        margin-left: 5%;  
      }
    }

    > div:nth-child(2) {
        width: 26%;  

      > p {
        width: 90%;      
      }
    }
  }
`

export const ContactUs = styled.section `
  padding-top: 5%;
  padding-bottom: 7%;

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    padding-top: 12%;
    padding-bottom: 14%;
  }
`

export const ContactUsTitle = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 1%;
  
  > div:nth-child(1) {
    width: 8%;
    height: 1px;
    border: 1px solid ${greenLightTractilus};
    background-color: ${greenLightTractilus};
    margin-right: 2.5%;
  }

  > h2 {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.4vw;
    color: ${greenLightTractilus};
  }

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    margin-bottom: 5%;
    
    > div:nth-child(1) {
      width: 15%;
    }

    > h2 {
      font-size: 3.5vw;
    }
  }
`

export const ContactUsText = styled.p `
  color: ${blueDarkTractilus};
  text-align: center;
  font-size: 2.2vw;
  font-family: 'Montserrat';
  font-weight: 600;
  width: 50%;
  margin: auto auto 4% auto;

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    font-size: 6vw;
  }
`

export const ContactUsForm = styled.div `
  display: flex;
  flex-direction: column;
  width: 60%;
  margin: auto;

  > div {

    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${blueDarkTractilus};
      margin-bottom: 1%;
    }

    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greyDarkTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greyDarkTractilus};
      padding-left: 2%;
      outline: none;
    }

    > textarea {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greyDarkTractilus};
      width: 100%;
      margin-bottom: 2.5%;
      border-radius: 10px;
      border: 1px solid ${greyDarkTractilus};
      padding-top: 2%;
      padding-left: 2%;
      outline: none;
    }
  }

  > button {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1.2vw;
    color: ${whiteTractilus};
    background-color: ${orangeTractilus};
    border: none;
    width: 25%;
    margin: 5% auto auto auto;
    padding: 1% 6%;
    border-radius: 15px;
    cursor: pointer;
    transition: all 0.25s linear;

    &:hover {
      background-color: ${greenLightTractilus};
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }

  @media (min-width:600px) and (max-width: 1024px){
  }

  @media (max-width: 600px) {
    display: flex;
    flex-direction: column;
    width: 90%;
    margin:10% auto 0 auto;

    > div {

      > p {
        font-size: 3.5vw;
        margin-top: 2%;
        margin-bottom: 5%;
      }

      > input {
        height: 45px;
        font-size: 5vw;
      }

      > textarea {
        height: 200px;
        font-size: 5vw;
      }
    }

    > button {
      font-size: 3.5vw;
      width: 90%;
      height: 40px;
    }
  }
`