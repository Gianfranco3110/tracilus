import React, { useState } from 'react'

import {
    HomeContainer,
    HomeText,
    ChooseUser,
    ChooseUserTitle,
    ChooseUserText,
    ChooseUserCards,
    UserCard,
    ChooseProducts,
    ChooseProductsTitle,
    ChooseProductsText,
    ChooseProductsCards,
    ProductsCards,
    ProductsControls,
    ContactUs,
    ContactUsTitle,
    ContactUsText,
    ContactUsForm
} from './home.styles'

export default function Home () {

    const [ contact, setContact ] = useState({}) 

    const handleData = e => contact[e.target.name] = e.target.value

    const sendContactToServer = () => {
        if(Object.keys(contact).length === 3) {
          console.log('SEND DATA TO CONTACT')
        } else {
          console.log('SHOW MODAL ERROR LACK OF DATA')
        }
    }

    return (
        <HomeContainer>
            <HomeText>
                <div>
                    <div></div>
                    <h2>BIENVENIDO A TRACTILUS</h2>
                </div>
                <h1>
                Esta es la página donde se conectan
                productores y proveedores. Dentro de la 
                red podrá encontrar lo necesario para sus
                insumos y maquinaria.
                </h1>
                <p>
                Regístrese y comience 
                a conectar para crecer
                </p>
                <figure>
                    <a href="/#choose">
                        <img src="https://tractilus.s3.amazonaws.com/arrowDropdown.png" alt="flecha abajo" />
                    </a>
                </figure>
            </HomeText>
            <ChooseUser>
                <ChooseUserTitle>
                    <div></div>
                    <h2>ÚNASE A LA RED DE TRACTILUS</h2>
                </ChooseUserTitle>
                <ChooseUserText id='choose'>
                Dependiendo su especialidad, regístrese
                dando clic en los botones de abajo.
                </ChooseUserText>
                <ChooseUserCards>
                    <UserCard>
                        <figure>
                        <img src="https://tractilus.s3.amazonaws.com/productor.png" alt="productor de Tractilus" />
                        </figure>
                        <div>
                        <h2>Soy productor</h2>
                        <a href="/registrate/productor">Registrarse</a>
                        </div>
                    </UserCard>
                    <UserCard>
                        <figure>
                        <img src="https://tractilus.s3.amazonaws.com/proovedor.png" alt="productor de Tractilus" />
                        </figure>
                        <div>
                        <h2>Soy proveedor</h2>
                        <a href="/registrate/proveedor">Registrarse</a>
                        </div>
                    </UserCard>
                </ChooseUserCards>
            </ChooseUser>
            <ChooseProducts>
                <ChooseProductsTitle>
                    <div></div>
                    <h2>INSUMOS, MAQUINARIA Y SERVICIOS</h2>
                </ChooseProductsTitle>
                <ChooseProductsText>
                Podrá elegir entre los mejores proveedores.
                </ChooseProductsText>
                <ChooseProductsCards>
                    <ProductsCards>
                        <figure>
                        <img src="https://tractilus.s3.amazonaws.com/herramientas.png" alt="maquinaria" />
                        </figure>
                        <figure>
                        <img src="https://tractilus.s3.amazonaws.com/revolvedora.png" alt="maquinaria" />
                        </figure>
                        <div>
                        <figure>
                            <img src="https://tractilus.s3.amazonaws.com/rastril0.png" alt="maquinaria" />
                        </figure>
                        <figure>
                            <img src="https://tractilus.s3.amazonaws.com/bomba.png" alt="maquinaria" />
                        </figure>
                        </div>
                    </ProductsCards>
                    <ProductsControls>
                        <div>
                        <figure>
                            <img src="https://tractilus.s3.amazonaws.com/aroowLeft.svg" alt="flecha izquierda" />
                        </figure>
                        <p>Atrás</p>
                        </div>
                        <div>
                        <p>Siguiente</p>
                        <figure>
                            <img src="https://tractilus.s3.amazonaws.com/arrowRight.svg" alt="flecha izquierda" />
                        </figure>
                        </div>
                    </ProductsControls>
                </ChooseProductsCards>
            </ChooseProducts>
            <ContactUs>
                <ContactUsTitle>
                    <div></div>
                    <h2>CONTÁCTENOS</h2>
                </ContactUsTitle>
                <ContactUsText>
                ¿Dudas o comentarios?
                </ContactUsText>
                <ContactUsForm>
                    <div>
                        <p>Nombre completo:</p>
                        <input onChange={handleData} type="text" name='fullname' />
                    </div>
                    <div>
                        <p>Correo electrónico:</p>
                        <input onChange={handleData} type="email" name='email' />
                    </div>
                    <div>
                        <p>Comentario:</p>
                        <textarea onChange={handleData} name="comments" cols="30" rows="10" maxLength={250} />
                    </div>
                    <button onClick={sendContactToServer}>Enviar</button>
                </ContactUsForm>
            </ContactUs>
        </HomeContainer>
    )
}