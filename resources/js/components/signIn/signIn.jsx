import React, { useState } from 'react'
import AuthApi from '../api/authRepository';

import {
    SignInContainer,
    SignInTitle,
    SignInForm,
    SignInFormManual,
    Forgot,
    SocialNetworks
} from './signIn.styles'

export default function SignIn (props) {
    const [ user, setUser ] = useState({})

    const handleData = e => user[e.target.name] = e.target.value

    const sendDataToServer = () => {
        if(Object.keys(user).length === 2) {
            AuthApi.logIn(user)
                .then(r => {
                    let type = localStorage.getItem("user_type");
                    if(type == 3)
                        window.location = '/inicio/usuario'
                    else if(type == 4)
                        window.location = '/proveedor/inicio/agrega-productos'
                }).catch(e => {
                    console.log(e);
                    alert("No se pudo iniciar sesion, revise sus datos ingresados e intentelo de nuevo");
                });
        } else {
            console.log('SHOW MODAL ERROR LACK OF DATA LOGIN')
        }
    }

    const sendDataToGmail = () => {
        window.location = '/auth/google'
    }

    const sendDataToFacebook = () => {
        window.location = '/auth/facebook'
    }

    return (
        <SignInContainer>
            <SignInTitle>
                <h2>Iniciar sesión</h2>
                <p>
                Por favor, introduzca su correo 
                electrónico y contraseña.
                </p>
            </SignInTitle>
            <SignInForm>
                <SignInFormManual>
                    <div>
                        <p>Correo electrónico:</p>
                        <input onChange={handleData} type="text" name='email' />
                    </div>
                    <div>
                        <p>Contraseña:</p>
                        <div>
                        <input onChange={handleData} type="password" name='password' />
                        <figure>
                            <img src ="https://tractilus.s3.amazonaws.com/IconFeatherEyeblBlack.svg" alt="Ojito de contraseña" />
                        </figure>
                        </div>
                    </div>
                    <button onClick={sendDataToServer}>Iniciar sesión</button>
                </SignInFormManual>
                <Forgot>
                    <a href="/olvide-mi-contraseña">Olvidé mi contraseña</a>
                    <p>Si no tienes cuenta da <a href="/registrate">clic aquí</a></p>
                </Forgot>
                <SocialNetworks>
                    <div>
                        <div></div>
                        <p>O continue con su cuenta mediante</p>
                        <div></div>
                    </div>
                    <button onClick={sendDataToGmail}>
                        <figure>
                            <img src="https://tractilus.s3.amazonaws.com/google.png" alt="logo Gmail" />
                        </figure>
                        Iniciar sesión con Google
                    </button>
                    <button onClick={sendDataToFacebook}>
                        <figure>
                            <img src="https://tractilus.s3.amazonaws.com/facebook.png" alt="logo Facebook" />
                        </figure>
                        Iniciar sesión con Facebook
                    </button>
                </SocialNetworks>
            </SignInForm>
        </SignInContainer>
    )
}