import styled from 'styled-components'

import { blueDarkTractilus, greenLightTractilus, greyDarkTractilus, greyTractilus, orangeTractilus, whiteTractilus } from '../../styles/colorStyles'

export const SignInContainer = styled.section `
  padding-top: 5%;
  padding-bottom: 5%;
`

export const SignInTitle = styled.div `
  > h2 {
    color: ${greenLightTractilus};
    text-align: center;
    font-size: 2.5vw;
    font-family: 'Montserrat';
    font-weight: 600;
    margin-bottom: 1%;
  }

  > p {
    font-size: 1.2vw;
    font-family: 'Montserrat';
    color: ${blueDarkTractilus};
    text-align: center;
    margin-bottom: 2.5%;
  }

  @media (max-width: 600px) {

    > h2 {
      font-size: 9vw;
    }

    > p {
      font-size: 5vw;
      width: 90%;
      margin: auto auto 15% auto;
    }
  }
`

export const SignInForm = styled.div `

`

export const SignInFormManual = styled.div `
  display: flex;
  flex-direction: column;
  width: 35%;
  margin: auto;

  > div {

    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
      color: ${greyDarkTractilus};
      margin-bottom: 1%;
    }

    > input {
      font-size: 1.2vw;
      font-family: 'Montserrat';
      color: ${greyDarkTractilus};
      margin-bottom: 2.5%;
      width: 100%;
      height: 45px;
      border-radius: 10px;
      border: 1px solid ${greyDarkTractilus};
      padding-left: 2%;
      outline: none;
    }
    
    > div {
      display: flex;
      border-radius: 10px;
      border: 1px solid ${greyDarkTractilus};
      height: 45px;
      margin-bottom: 2.5%;
      padding-left: 2%;

      > input {
        font-size: 1.2vw;
        font-family: 'Montserrat';
        color: ${greyDarkTractilus};
        width: 90%;
        border: transparent;
        outline: none;
      }

      > figure {
        width: 4%;
        margin-left: 2%;
        padding-top: 2.5%;
        padding-bottom: 2.5%;
        cursor: pointer;

        > img {
          width: 100%;
          height: 100%;
        }
      }
    }
  }

  > button {
    font-family: 'Montserrat';
    font-weight: 600;
    font-size: 1vw;
    color: ${whiteTractilus};
    background-color: ${orangeTractilus};
    border: none;
    width: 35%;
    margin: 5% auto auto auto;
    padding: 2% 6%;
    border-radius: 20px;
    cursor: pointer;
    transition: all 0.25s linear;

    &:hover {
      background-color: ${greenLightTractilus};
      transform: translateY(-3px);
      box-shadow: 1px 4px 8px -2px rgba(0,0,0,0.57);
    }
  }

  @media(max-width: 600px) {
    width: 85%;

    > div {

      > p {
        font-size: 5vw;
        margin-bottom: 2.5%;
      }

      > input {
        font-size: 5vw;
      }

      > div {

        > input {
          font-size: 5vw;
        }

        > figure {
          width: 8%;
          margin-right: 6%;
          padding-top: 2.5%;
          padding-bottom: 2.5%;
          cursor: pointer;

          > img {
          }
        }
      }
    }

    > button {
      font-size: 5vw;
      width: 100%;
      padding: 4% 6%;
      border-radius: 25px;
    }
  }
`

export const Forgot = styled.div `
  > a {
    display: block;
    text-align: center;
    text-decoration: none;
    color: ${blueDarkTractilus};
    font-size: 1vw;
    font-family: 'Montserrat';
    font-weight: 500;
    width: 15%;
    margin: 2.5% auto auto auto;
  }

  > p {
    text-align: center;
    color: ${blueDarkTractilus};
    font-size: 1vw;
    font-family: 'Montserrat';
    font-weight: 500;

    > a {
      color: ${orangeTractilus};
      font-size: 1vw;
      font-family: 'Montserrat';
      font-weight: 600;
    }
  }

  @media(max-width: 600px) {
    > a {
      font-size: 5vw;
      width: 80%;
      margin: 5% auto auto auto;
    }

    > p {
      font-size: 5vw;

      > a {
        font-size: 5vw;
      }
    }
  }
`

export const SocialNetworks = styled.div `
  display: flex;
  flex-direction: column;

  > div:nth-child(1) {
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 40%;
    margin: 2.5% auto;

    > div {
      width: 15%;
      height: 1px;
      border: 1px solid ${blueDarkTractilus};
      background-color: ${blueDarkTractilus};
    }

    > p {
      font-family: 'Montserrat';
      font-size: 1.2vw;
    }
  }

  > button {
    display: flex;
    align-items: center;
    font-size: 1.2vw;
    font-family: 'Montserrat';
    color: ${greyDarkTractilus};
    margin-bottom: 2.5%;
    width: 35%;
    height: 40px;
    margin: 0.5% auto;
    padding-left: 5%;
    border-radius: 10px;
    border: 1px solid ${greyDarkTractilus};
    padding-left: 2%;
    background-color: transparent;
    cursor: pointer;

    > figure {
      width: 25px;
      height: 25px;
      margin-right: 20%;

      > img  {
        width: 100%;
        height: 100%;
      }
    }
  }

  @media(max-width: 600px) {
    > div:nth-child(1) {
      width: 85%;
      margin: 5% auto;


      > div {
        width: 10%;
      }

      > p {
        font-size: 3.5vw;
      }
    }

    > button {
      font-size: 4vw;
      width: 85%;
      height: 40px;
      margin: 2% auto;

      > figure {
        margin-right: 10%;

        > img  {
        }
      }
    }
  }
`